package com.minecraftonline.penguindungeons.ai;

import org.spongepowered.api.entity.living.Creature;

import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.player.EntityPlayer;
public class AttackIfReachable extends EntityAIAttackMelee {
   private final double reachSquared;

   public AttackIfReachable(EntityCreature creature) {
      this(creature, 0.0D);
   }

   public AttackIfReachable(EntityCreature creature, double reach) {
      super(creature, 0, false);
      this.reachSquared = reach * reach;
      this.setMutexBits(8);
   }

   /**
    * Returns whether the EntityAIBase should begin execution.
    */
   @Override
   public boolean shouldExecute() {
      EntityLivingBase entitylivingbase = this.attacker.getAttackTarget();
      if (entitylivingbase == null) {
         return false;
      } else if (!entitylivingbase.isEntityAlive()) {
         return false;
      } else {
         return this.getAttackReachSqr(entitylivingbase) >= this.attacker.getDistanceSq(entitylivingbase.posX, entitylivingbase.getEntityBoundingBox().minY, entitylivingbase.posZ);
      }
   }

   /**
    * Returns whether an in-progress EntityAIBase should continue executing
    */
   @Override
   public boolean shouldContinueExecuting() {
      EntityLivingBase entitylivingbase = this.attacker.getAttackTarget();
      if (entitylivingbase == null) {
         return false;
      } else if (!entitylivingbase.isEntityAlive()) {
         return false;
      } else if (this.attackTick <= 0 && this.getAttackReachSqr(entitylivingbase) <= this.attacker.getDistanceSq(entitylivingbase.posX, entitylivingbase.getEntityBoundingBox().minY, entitylivingbase.posZ)) {
          return false;
      } else {
         return !(entitylivingbase instanceof EntityPlayer) || !((EntityPlayer)entitylivingbase).isSpectator() && !((EntityPlayer)entitylivingbase).isCreative();
      }
   }

   /**
    * Execute a one shot task or start executing a continuous task
    */
   @Override
   public void startExecuting() {}

   /**
    * Reset the task's internal state. Called when this task is interrupted by another one
    */
   @Override
   public void resetTask() {
      EntityLivingBase entitylivingbase = this.attacker.getAttackTarget();
      if (entitylivingbase instanceof EntityPlayer && (((EntityPlayer)entitylivingbase).isSpectator() || ((EntityPlayer)entitylivingbase).isCreative())) {
         this.attacker.setAttackTarget((EntityLivingBase)null);
      }
   }

   /**
    * Keep ticking a continuous task that has already been started
    */
   @Override
   public void updateTask() {
      EntityLivingBase entitylivingbase = this.attacker.getAttackTarget();
      this.attacker.getLookHelper().setLookPositionWithEntity(entitylivingbase, 30.0F, 30.0F);
      double d0 = this.attacker.getDistanceSq(entitylivingbase.posX, entitylivingbase.getEntityBoundingBox().minY, entitylivingbase.posZ);

      this.attackTick = Math.max(this.attackTick - 1, 0);
      this.checkAndPerformAttack(entitylivingbase, d0);
   }

   public static class AttackIfReachableTask extends DelegatingToMCAI<Creature> {
       public AttackIfReachableTask(Creature creature) {
           super(PenguinDungeonAITaskTypes.ATTACK_IF_REACHABLE, new AttackIfReachable((EntityCreature) creature));
       }

       public AttackIfReachableTask(Creature creature, double reach) {
           super(PenguinDungeonAITaskTypes.ATTACK_IF_REACHABLE, new AttackIfReachable((EntityCreature) creature, reach));
       }
   }

   @Override
   protected double getAttackReachSqr(EntityLivingBase attackTarget) {
       if (reachSquared != 0.0D) {
           return reachSquared;
       } else {
           return (double)(this.attacker.width * 2.0F * this.attacker.width * 2.0F + attackTarget.width);
       }
   }
}
