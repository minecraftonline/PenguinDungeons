package com.minecraftonline.penguindungeons.ai;

import org.spongepowered.api.entity.living.Creature;

import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.player.EntityPlayer;
public class AttackPlayerIfReachable extends EntityAIAttackMelee {
   private final double reach;
   private final double reachSquared;
   private EntityPlayer currentTarget = null;

   public AttackPlayerIfReachable(EntityCreature creature, double reach) {
      super(creature, 0, false);
      this.reach = reach;
      this.reachSquared = reach * reach;
      this.setMutexBits(8);
   }

   /**
    * Returns whether the EntityAIBase should begin execution.
    */
   @Override
   public boolean shouldExecute() {
      // does not have to be the current target, just any player within range
      currentTarget = this.attacker.world.getClosestPlayerToEntity(attacker, reach);
      if (currentTarget == null) {
         return false;
      } else if (!currentTarget.isEntityAlive()) {
         return false;
      } else {
         return !currentTarget.isSpectator() && !currentTarget.isCreative();
      }
   }

   /**
    * Returns whether an in-progress EntityAIBase should continue executing
    */
   @Override
   public boolean shouldContinueExecuting() {
      if (currentTarget == null) {
         return false;
      } else if (!currentTarget.isEntityAlive()) {
         return false;
      } else if (this.attackTick <= 0 && this.getAttackReachSqr(currentTarget) <= this.attacker.getDistanceSq(currentTarget.posX, currentTarget.getEntityBoundingBox().minY, currentTarget.posZ)) {
          return false;
      } else {
         return !currentTarget.isSpectator() && !currentTarget.isCreative();
      }
   }

   /**
    * Execute a one shot task or start executing a continuous task
    */
   @Override
   public void startExecuting() {}

   /**
    * Reset the task's internal state. Called when this task is interrupted by another one
    */
   @Override
   public void resetTask() {
      currentTarget = null;
   }

   /**
    * Keep ticking a continuous task that has already been started
    */
   @Override
   public void updateTask() {
      this.attacker.getLookHelper().setLookPositionWithEntity(currentTarget, 30.0F, 30.0F);
      double d0 = this.attacker.getDistanceSq(currentTarget.posX, currentTarget.getEntityBoundingBox().minY, currentTarget.posZ);

      this.attackTick = Math.max(this.attackTick - 1, 0);
      this.checkAndPerformAttack(currentTarget, d0);
   }

   public static class AttackPlayerIfReachableTask extends DelegatingToMCAI<Creature> {
       public AttackPlayerIfReachableTask(Creature creature, double reach) {
           super(PenguinDungeonAITaskTypes.ATTACK_IF_REACHABLE, new AttackPlayerIfReachable((EntityCreature) creature, reach));
       }
   }

   @Override
   protected double getAttackReachSqr(EntityLivingBase attackTarget) {
       return reachSquared;
   }
}
