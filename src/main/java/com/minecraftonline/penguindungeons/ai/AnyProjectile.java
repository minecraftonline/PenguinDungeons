package com.minecraftonline.penguindungeons.ai;

import java.util.Optional;
import java.util.UUID;

import org.spongepowered.api.entity.Entity;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityFireball;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.world.WorldServer;

public class AnyProjectile
{
    Entity projectile;

    public AnyProjectile(Entity projectile)
    {
        this.projectile = projectile;
    }

    public EntityLivingBase getShooter()
    {
        if (projectile instanceof EntityThrowable)
        {
            return ((EntityThrowable) projectile).getThrower();
        }
        else if (projectile instanceof EntityFireball)
        {
            return ((EntityFireball) projectile).shootingEntity;
        }
        else if (projectile instanceof EntityArrow)
        {
            net.minecraft.entity.Entity shooter = ((EntityArrow) projectile).shootingEntity;
            if (shooter instanceof EntityLivingBase)
            {
                return (EntityLivingBase) shooter;
            }
        }
        Optional<UUID> creator = projectile.getCreator();
        if (creator.isPresent())
        {
            try
            {
                net.minecraft.entity.Entity entity = ((WorldServer)projectile.getWorld()).getEntityFromUuid(creator.get());
                if (entity instanceof EntityLivingBase)
                {
                    return (EntityLivingBase)entity;
                }
            }
            catch (Throwable any)
            {
                return null;
            }
        }
        return null;
    }

    public Entity getEntity()
    {
        return projectile;
    }

    public net.minecraft.entity.Entity getMinecraftEntity()
    {
        return (net.minecraft.entity.Entity) projectile;
    }
}
