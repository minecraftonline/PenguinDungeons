package com.minecraftonline.penguindungeons.loot;

import com.google.gson.JsonParseException;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.mixin.LootTableManagerAccessor;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.LootTableList;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.spongepowered.api.asset.Asset;
import org.spongepowered.api.plugin.PluginContainer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class LootTableManager {

    private static final ResourceLocation EMPTY_LOOT_TABLE = LootTableList.EMPTY;
    private final Map<ResourceKey, ResourceLocation> lootTableMap = new HashMap<>();
    private final Path overridesDir;
    private final Path defaultsDir;

    public LootTableManager(Path defaultsDir, Path overridesDir) {
        this.defaultsDir = defaultsDir;
        this.overridesDir = overridesDir;
    }

    public ResourceLocation getLootTable(ResourceKey id) {
        ResourceLocation lootTable = this.lootTableMap.get(id);
        if (lootTable == null) {
            PenguinDungeons.getLogger().warn("Loot table was null for {} ({})", id);
            return EMPTY_LOOT_TABLE;
        }
        return lootTable;
    }

    public LootTable loadLootTable(ResourceLocation resource) {
        String path = resource.getPath() + ".json";
        Path overridePath = overridesDir.resolve(path);
        final Path resourcePath;
        if (Files.exists(overridePath)) {
            PenguinDungeons.getLogger().info("Loading customised loot table from {}", overridePath);
            resourcePath = overridePath;
        }
        else {
            resourcePath = defaultsDir.resolve(path);
        }
        final String s;
        try {
            s = FileUtils.readFileToString(resourcePath.toFile(), StandardCharsets.UTF_8);
        } catch (FileNotFoundException e) {
            PenguinDungeons.getLogger().warn("No loot table found for custom entity resource {}, path: {}", resource, resourcePath);
            return LootTable.EMPTY_LOOT_TABLE;
        } catch (IOException e) {
            PenguinDungeons.getLogger().error("Failed to load PenguinDungeons loot table" + resource + " at " + resourcePath + " while reading file.", e);
            return LootTable.EMPTY_LOOT_TABLE;
        }

        try {
            return JsonUtils.gsonDeserialize(LootTableManagerAccessor.getGsonInstance(), s, LootTable.class);
        } catch (JsonParseException e) {
            PenguinDungeons.getLogger().error("Failed to load PenguinDungeons loot table" + resource + " at " + resourcePath + " while parsing json.", e);
            return LootTable.EMPTY_LOOT_TABLE;
        }

    }

    private static final String LOOT_TABLE_SOURCE_DIR = "loottable";

    public void copyDefaultLootTables() {
        final PluginContainer pluginContainer = PenguinDungeons.getInstance().getContainer();

        for (PDEntityType pdEntityType : CustomEntityTypes.getAll()) {
            final String path = LOOT_TABLE_SOURCE_DIR + "/" + pdEntityType.getId().formattedWith("/") + ".json";

            final Optional<Asset> asset = pluginContainer.getAsset(path);
            if (asset.isPresent()) {
                final Path curPath = makePathFor(pdEntityType);

                try {
                    Files.createDirectories(curPath.getParent());

                    if (!Files.exists(curPath)) {
                        asset.get().copyToFile(curPath);
                    }
                    else if (lootTableNeedsUpdating(asset.get(), curPath)) {
                        PenguinDungeons.getLogger().info("Updating loot table to new default " + curPath);
                        asset.get().copyToFile(curPath, true);
                    }
                } catch (IOException e) {
                    PenguinDungeons.getLogger().error("Error copying loot table of " + pdEntityType.getId() + " to directory: " + curPath +
                            "\n Not going to try to copy any others.", e);
                    break;
                }
            }
        }

        int registeredTables = 0;
        // Register minecraft loot tables with minecraft.
        for (PDEntityType pdEntityType : CustomEntityTypes.getAll()) {
            Path path = makePathFor(pdEntityType);
            if (Files.exists(path)) {
                registerLootTable(pdEntityType.getId(), pdEntityType.getId().formattedWith("/"));
                registeredTables++;
            }
        }
        PenguinDungeons.getLogger().info("Registered " + registeredTables + " custom loot tables for custom entities.");
    }

    private Path makePathFor(final PDEntityType pdEntityType) {
        final ResourceKey key = pdEntityType.getId();
        return this.defaultsDir.resolve(key.namespace()).resolve(key.value() + ".json");
    }

    private void registerLootTable(ResourceKey key, String id) {
        final ResourceLocation resource = new ResourceLocation(ResourceKey.PENGUIN_DUNGEONS_NAMESPACE, id);
        LootTableList.register(resource);
        lootTableMap.put(key, resource);
    }

    private boolean lootTableNeedsUpdating(Asset asset, Path path) {
        try {
            final byte[] md5OfAsset = DigestUtils.md5(asset.getUrl().openStream());
            final byte[] md5OfPath = DigestUtils.md5(Files.newInputStream(path));
            return !Arrays.equals(md5OfAsset, md5OfPath);
        } catch (IOException e) {
            PenguinDungeons.getLogger().error("Failed to compare md5 hashes of loot tables");
        }
        try {
            return !asset.readString().equals(new String(Files.readAllBytes(path)));
        } catch (IOException e) {
            PenguinDungeons.getLogger().error("Failed to compare contents of loot tables!");
        }
        return false;
    }
}
