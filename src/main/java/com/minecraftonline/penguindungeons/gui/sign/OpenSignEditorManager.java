package com.minecraftonline.penguindungeons.gui.sign;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.play.server.SPacketSignEditorOpen;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.common.bridge.api.text.TextBridge;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class OpenSignEditorManager {

    private static final int MAX_HEIGHT = 255;
    private static final int MID_POINT = MAX_HEIGHT / 2;

    private final Map<Player, SignGui> guiMap = new HashMap<>();
    private final Map<Player, BlockPos> fakeBlockMap = new HashMap<>();

    public void openSignGui(Player player, SignGui signGui) {
        Location<World> loc = player.getLocation();
        int x = loc.getBlockX();
        // Make the sign as far away as possible from the player without risking it not being loaded at all.
        int y = loc.getBlockY() > MID_POINT ? 0 : MAX_HEIGHT;
        int z = loc.getBlockZ();

        BlockPos blockPos = new BlockPos(x, y, z);
        this.fakeBlockMap.put(player, blockPos);
        player.sendBlockChange(x, y, z, BlockTypes.STANDING_SIGN.getDefaultState());
        this.guiMap.put(player, signGui);
        TileEntitySign sign = new TileEntitySign();
        sign.setPos(blockPos);
        SignData signData = signGui.getSignData();
        setMCData(sign.signText, signData);
        NetHandlerPlayServer conn = ((EntityPlayerMP) player).connection;
        conn.sendPacket(sign.getUpdatePacket());
        conn.sendPacket(new SPacketSignEditorOpen(blockPos));
    }

    private static void setMCData(ITextComponent[] component, SignData signData) {
        for (int i = 0; i < 4; i++) {
            component[i] = ((TextBridge) signData.get(i).get()).bridge$toComponent();
        }
    }

    private void cleanupBlockPos(Player player) {
        final BlockPos pos = this.fakeBlockMap.remove(player);
        if (pos != null) {
            player.resetBlockChange(pos.getX(), pos.getY(), pos.getZ());
        }
    }

    @Nullable
    public SignGui getOpenSign(final Player player) {
        return guiMap.get(player);
    }

    @Nullable
    public SignGui consumeOpenSign(final Player player) {
        this.cleanupBlockPos(player);
        return this.guiMap.remove(player);
    }

    @Listener
    public void onDisconnect(ClientConnectionEvent.Disconnect event) {
        this.guiMap.remove(event.getTargetEntity());
        this.fakeBlockMap.remove(event.getTargetEntity());
    }
}
