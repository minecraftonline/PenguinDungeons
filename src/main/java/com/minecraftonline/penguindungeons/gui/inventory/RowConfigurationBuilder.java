package com.minecraftonline.penguindungeons.gui.inventory;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class RowConfigurationBuilder implements RowConfiguration.Builder {

    private final Map<Integer, RowConfigurationButton> map = new HashMap<>();

    @Override
    public RowConfiguration.Builder withButton(int index, ItemStack icon, Consumer<Player> run) {
        this.map.put(index, RowConfigurationButton.of(icon, run));
        return this;
    }

    @Override
    public RowConfiguration.Builder withConditionalButton(int index, ItemStack icon, Consumer<Player> run, Supplier<Boolean> shouldApply) {
        this.map.put(index, RowConfigurationButton.of(icon, run, shouldApply));
        return this;
    }

    @Override
    public RowConfiguration.Builder withConditionalButton(int index, ItemStack icon, Consumer<Player> run, Supplier<Boolean> shouldApply, ItemStack notAppliedItem) {
        this.map.put(index, RowConfigurationButton.of(icon, run, shouldApply, notAppliedItem));
        return this;
    }

    @Override
    public RowConfiguration build() {
        return new RowConfiguration(new HashMap<>(this.map));
    }
}
