package com.minecraftonline.penguindungeons.gui.inventory;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.data.container_loot.ContainerLootData;
import com.minecraftonline.penguindungeons.data.container_loot.ContainerLootableData;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.InventoryArchetypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.item.inventory.property.InventoryTitle;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.item.inventory.transaction.SlotTransaction;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class PDContainerLootEdit {
    private final Location<World> blockContainer;
    private final Inventory inventory;
    private final Set<Player> players = new HashSet<>(); // Players who have this inventory open.

    private static final int WIDTH = 9;
    private static final int HEIGHT = 1;

    // data queries on items that could provide loot tables
    private static final DataQuery containerLootQuery = DataQuery.of("UnsafeData", "BlockEntityTag", "LootTable");
    private static final DataQuery entityLootQuery = DataQuery.of("UnsafeData", "EntityTag", "DeathLootTable");

    private Optional<ResourceKey> key = Optional.empty();

    public void showInventoryTo(Player player) {
        player.openInventory(this.inventory);
        this.players.add(player);
    }

    public void onDisconnect(Player player) {
        this.players.remove(player);
    }

    public PDContainerLootEdit(Location<World> blockContainer, Text title) {
        this.blockContainer = blockContainer;
        this.inventory = Inventory.builder()
                .of(InventoryArchetypes.MENU_ROW)
                .property(InventoryTitle.of(title))
                .property(InventoryDimension.of(WIDTH, HEIGHT))
                .listener(ClickInventoryEvent.class, event -> {
                    if (!event.getSlot().isPresent()) {
                        return;
                    }
                    Optional<Player> optPlayer = event.getCause().first(Player.class);
                    if (!optPlayer.isPresent()) {
                        return;
                    }
                    if (event.getTransactions().isEmpty()) return;
                    if (event instanceof ClickInventoryEvent.Middle) return; // allow middle clicks to copy items

                    boolean changed = false;

                    if (event instanceof ClickInventoryEvent.Shift) {
                        int slotIndex = event.getSlot().get().getInventoryProperty(SlotIndex.class).map(SlotIndex::getValue).get();
                        for (SlotTransaction transaction : event.getTransactions()) {
                            if (slotIndex >= WIDTH) {
                                // moving item from outside menu to inside
                                if (!transaction.getOriginal().isEmpty()) {
                                    transaction.setValid(false);
                                    Optional<ResourceKey> newKey = fromItem(transaction.getOriginal());
                                    if (newKey.isPresent() && (!this.key.isPresent() || !newKey.get().equals(this.key.get()))) {
                                        setLootTable(newKey.get());
                                        changed = true;
                                    }
                                }
                            } else {
                                // removing loot table from inside menu
                                if (!transaction.getOriginal().isEmpty()) {
                                    removeLootData();
                                    changed = true;
                                }
                            }
                        }
                        // any other event/item
                        event.setCancelled(!changed);
                    } else if (event instanceof ClickInventoryEvent.Primary) {
                        for (SlotTransaction transaction : event.getTransactions()) {
                            int slotIndex = transaction.getSlot().getInventoryProperty(SlotIndex.class).map(SlotIndex::getValue).get();
                            if (slotIndex < WIDTH) {
                                Optional<ResourceKey> newKey = fromItem(transaction.getFinal());
                                if (newKey.isPresent() && (!this.key.isPresent() || !newKey.get().equals(this.key.get()))) {
                                    setLootTable(newKey.get());
                                    event.getCursorTransaction().setValid(false);
                                    event.setCancelled(false);
                                    changed = true;
                                } else if (transaction.getFinal().isEmpty() && !transaction.getOriginal().isEmpty()) {
                                    removeLootData();
                                    event.getCursorTransaction().setValid(false);
                                    event.setCancelled(false);
                                    changed = true;
                                } else {
                                    event.getCursorTransaction().setValid(false);
                                    event.setCancelled(true);
                                }
                            } else {
                                event.setCancelled(false); // To do with the users inventory.
                                return;
                            }
                        }
                    } else {
                        for (SlotTransaction slotTransaction : event.getTransactions()) {
                            int slotIndex = slotTransaction.getSlot().getInventoryProperty(SlotIndex.class).map(SlotIndex::getValue).get();
                            if (slotIndex < WIDTH) {
                                event.setCancelled(true);
                                return;
                            }
                        }
                    }
                    if (changed) {
                        // update inventory after marking all the transactions as invalid
                        updateInventory();
                    }
                })
                .build(PenguinDungeons.getInstance());
        this.key = this.blockContainer.get(PenguinDungeonKeys.CONTAINER_LOOT);
        updateInventory();
    }

    private void removeLootData() {
        // remove the loot key/time from this container
        this.key = Optional.empty();
        blockContainer.remove(ContainerLootData.class);
        blockContainer.remove(ContainerLootableData.class);
        this.inventory.clear();
    }

    private void setLootTable(ResourceKey newKey) {
        blockContainer.offer(new ContainerLootData(newKey));
        // no need to add a lootable time here, that is only added after container is looted
        this.key = Optional.of(newKey);
    }

    private void updateInventory() {
        this.inventory.clear();
        if (!this.key.isPresent()) return;

        List<Text> lore = Collections.singletonList(Text.of(TextColors.WHITE, "Loot Table: ", TextColors.GRAY, this.key.get().asString()));
        ItemStack item;

        Optional<PDEntityType> pdEntityTypeOptional = PenguinDungeons.getInstance().getPDEntityRegistry().find(this.key.get());
        if (pdEntityTypeOptional.isPresent()) {
            // use PD spawn egg
            item = pdEntityTypeOptional.get().getSpawnEgg();
        } else if (this.key.get().value().startsWith("entities/")) {
            String entityId = this.key.get().value().split("/")[1];
            // display as spawn egg for mob
            item = ItemStack.builder().itemType(ItemTypes.SPAWN_EGG).build();
            Optional<EntityType> entityType = Sponge.getRegistry().getType(EntityType.class, entityId);
            if (entityType.isPresent()) item.offer(Keys.SPAWNABLE_ENTITY_TYPE, entityType.get());
        } else {
            // just display as a chest
            item = ItemStack.builder().itemType(ItemTypes.CHEST).build();
            item.setRawData(item.toContainer().set(containerLootQuery, this.key.get().asString()));
        }
        // override lore to just say loot table
        item.offer(Keys.ITEM_LORE, lore);

        this.inventory.set(item);
    }

    private static Optional<ResourceKey> fromItem(ItemStackSnapshot snapshot) {
        if (snapshot.isEmpty()) return Optional.empty();
        // allow loot tables from PD entities
        Optional<ResourceKey> resourceKey = snapshot.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (resourceKey.isPresent()) {
            return resourceKey;
        }
        Optional<String> lootTableKey;
        // try chest item nbt loot table
        lootTableKey = snapshot.toContainer().getString(containerLootQuery);
        // then entity spawn egg item nbt death loot table
        if (!lootTableKey.isPresent()) lootTableKey = snapshot.toContainer().getString(entityLootQuery);

        if (lootTableKey.isPresent()) {
            return lootTableKey.flatMap(ResourceKey::tryResolve);
        }

        if (snapshot.getType() == ItemTypes.SPAWN_EGG) {
            Optional<EntityType> entityType = snapshot.get(Keys.SPAWNABLE_ENTITY_TYPE);
            if (entityType.isPresent()) {
                // entity loot table
                String entityLootTable = entityType.get().getId().replace(":", ":entities/");
                if (entityType.get() == EntityTypes.SHEEP) entityLootTable += "/white";
                return ResourceKey.tryResolve(entityLootTable);
            }
        }

        return Optional.empty();
    }

    protected void closeAllOpenInventories() {
        this.players.forEach(Player::closeInventory);
    }
}
