package com.minecraftonline.penguindungeons.gui.inventory;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.InventoryArchetypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.item.inventory.property.InventoryTitle;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.item.inventory.property.SlotPos;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class ScrollableInventoryList {

    private static final int WIDTH = 9;
    private static final int HEIGHT = 6;
    private static final int PAGE_CAPACITY = WIDTH * (HEIGHT - 1);
    private static final int OPTIONS_ROW = HEIGHT - 1;
    private int index = 0;
    private Inventory inventory;
    private RowConfiguration rowConfiguration;
    private final Text title;
    private final List<ItemStack> items;
    private final ClickListener onClick;

    public interface ClickListener {
        void onClick(Player player, ItemStack itemStack, int index, boolean shift);
    }

    public ScrollableInventoryList(Text title, List<ItemStack> items, ClickListener onClick) {
        this.title = title;
        this.items = new ArrayList<>(items);
        this.onClick = onClick;
    }

    public ScrollableInventoryList(ScrollableInventoryListInfo info) {
        this(info.title, info.items, info.onClick);
    }

    private void initInventory() {
        final InventoryDimension dimension = InventoryDimension.of(WIDTH, HEIGHT);

        this.rowConfiguration = makeRowConfiguration().build();

        final Inventory.Builder invBuilder = Inventory.builder()
                .of(InventoryArchetypes.MENU_GRID)
                .property(InventoryTitle.of(this.title))
                .property(dimension)
                .listener(ClickInventoryEvent.class, event -> {
                    final Optional<Integer> slotIndex = event.getSlot().flatMap(slot -> slot.getInventoryProperty(SlotIndex.class)).map(SlotIndex::getValue);
                    if (!slotIndex.isPresent()) {
                        event.setCancelled(true);
                        return;
                    }
                    if (slotIndex.get() >= PAGE_CAPACITY) {
                        return;
                    }
                    event.setCancelled(true);
                    int itemIndex = this.index * PAGE_CAPACITY + slotIndex.get();
                    if (itemIndex >= this.items.size()) {
                        return;
                    }
                    event.getCause().first(Player.class).ifPresent(player -> {
                        if (event instanceof ClickInventoryEvent.Shift) {
                            this.onClick.onClick(player, this.items.get(itemIndex), itemIndex, true);
                        }
                        else if (event instanceof ClickInventoryEvent.Primary) {
                            this.onClick.onClick(player, this.items.get(itemIndex), itemIndex, false);
                        }
                    });
                });

        this.rowConfiguration.applyListeners(invBuilder, dimension, OPTIONS_ROW);
        this.inventory = invBuilder.build(PenguinDungeons.getInstance());
    }

    public Inventory create() {
        initInventory();
        setPage(0);
        return this.inventory;
    }

    private boolean hasPage(int index) {
        return index == 0 || (0 <= index && index < getPages());
    }

    private int getPages() {
        return ((items.size() + PAGE_CAPACITY - 1) / PAGE_CAPACITY); // Ceiling divide apparently
        // We're adding PAGE_CAPACITY - 1 so not quite a full extra when we divide,
        // but it means if theres any more remainder, it increases the output by 1.
    }

    private void setPage(int index) {

        if (!hasPage(index)) {
            throw new IllegalStateException("Cannot create page " + index + " we don't have that many pages!");
        }

        this.inventory.clear();

        this.index = index;

        final Iterator<ItemStack> iter = items.listIterator(PAGE_CAPACITY * index);
        for (int y = 0; y < (HEIGHT - 1); y++) {
            if (!iter.hasNext()) {
                break;
            }
            for (int x = 0; x < WIDTH; x++) {
                if (!iter.hasNext()) {
                    break;
                }
                inventory.query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(x, y))).set(iter.next());
            }
        }
        this.rowConfiguration.applyItems(this.inventory, OPTIONS_ROW);
    }

    private static final ItemStack PREVIOUS_PAGE_BUTTON = ItemStack.builder()
            .itemType(ItemTypes.ARROW)
            .add(Keys.DISPLAY_NAME, Text.of(TextColors.WHITE, "Previous Page"))
            .build();

    private static final ItemStack NO_PREVIOUS_PAGE = ItemStack.builder()
            .itemType(ItemTypes.BARRIER)
            .add(Keys.DISPLAY_NAME, Text.of(TextColors.RED, "No Previous Page"))
            .build();

    private static final ItemStack NEXT_PAGE_BUTTON = ItemStack.builder()
            .itemType(ItemTypes.ARROW)
            .add(Keys.DISPLAY_NAME, Text.of(TextColors.WHITE, "Next Page"))
            .build();

    private static final ItemStack NO_NEXT_PAGE = ItemStack.builder()
            .itemType(ItemTypes.BARRIER)
            .add(Keys.DISPLAY_NAME, Text.of(TextColors.RED, "No Next Page"))
            .build();

    protected RowConfiguration.Builder makeRowConfiguration() {
        return RowConfiguration.builder()
                .withConditionalButton(0, PREVIOUS_PAGE_BUTTON, player -> setPage(this.index - 1), () -> hasPage(this.index - 1), NO_PREVIOUS_PAGE)
                .withConditionalButton(WIDTH - 1, NEXT_PAGE_BUTTON, player -> setPage(this.index + 1), () -> hasPage(this.index + 1), NO_NEXT_PAGE);
    }

}
