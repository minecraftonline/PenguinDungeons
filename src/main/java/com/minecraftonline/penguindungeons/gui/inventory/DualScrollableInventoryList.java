package com.minecraftonline.penguindungeons.gui.inventory;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.scheduler.Task;

import java.util.function.Supplier;

public class DualScrollableInventoryList {

    private static final int MIDDLE_SLOT = 4;

    private final ScrollableInventoryList list1;
    private ScrollableInventoryList list2;

    public DualScrollableInventoryList(
            ScrollableInventoryListInfo list1Info,
            ScrollableInventoryListInfo list2Info,
            ItemStack selectList1Icon,
            ItemStack selectList2Icon
    ) {
        this.list1 = new SubScrollableInventoryList(list1Info, selectList1Icon, () -> this.list2);
        this.list2 = new SubScrollableInventoryList(list2Info, selectList2Icon, () -> this.list1);
    }

    public Inventory create() {
        return this.list1.create();
    }

    public static class SubScrollableInventoryList extends ScrollableInventoryList {

        private final ItemStack switchIcon;
        private final Supplier<? extends ScrollableInventoryList> other;

        public SubScrollableInventoryList(ScrollableInventoryListInfo info, ItemStack switchIcon, Supplier<? extends ScrollableInventoryList> other) {
            super(info);
            this.switchIcon = switchIcon;
            this.other = other;
        }

        @Override
        protected RowConfiguration.Builder makeRowConfiguration() {
            return super.makeRowConfiguration()
                    .withButton(MIDDLE_SLOT, switchIcon, player -> Task.builder()
                            .delayTicks(1)
                            .execute(() -> player.openInventory(other.get().create()))
                            .submit(PenguinDungeons.getInstance()));
        }
    }

}
