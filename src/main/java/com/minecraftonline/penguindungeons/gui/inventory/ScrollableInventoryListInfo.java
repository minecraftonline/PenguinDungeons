package com.minecraftonline.penguindungeons.gui.inventory;

import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

import java.util.List;

public class ScrollableInventoryListInfo {
    public final Text title;
    public final List<ItemStack> items;
    public final ScrollableInventoryList.ClickListener onClick;

    public ScrollableInventoryListInfo(Text title, List<ItemStack> items, ScrollableInventoryList.ClickListener onClick) {
        this.title = title;
        this.items = items;
        this.onClick = onClick;
    }
}
