package com.minecraftonline.penguindungeons.util.weightedtable;

import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.dungeon.WeightedSpawnOption;

import javax.annotation.Nullable;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class WeightedSpawnOptionTableReference implements WeightedTableReference<PDEntityType> {

    private final WeightedSpawnOption weightedSpawnOption;

    public WeightedSpawnOptionTableReference(WeightedSpawnOption weightedSpawnOption) {
        this.weightedSpawnOption = weightedSpawnOption;
    }

    @Nullable
    @Override
    public Double getWeight(PDEntityType obj) throws ReferenceNoLongerValid {
        final Integer weight = this.weightedSpawnOption.getWeight(obj);
        return weight == null ? null : weight.doubleValue();
    }

    @Override
    public void put(PDEntityType obj, double weight) throws ReferenceNoLongerValid, KeyNotValidException {
        this.weightedSpawnOption.set(obj, (int) weight);
    }

    @Override
    public boolean has(PDEntityType obj) throws ReferenceNoLongerValid {
        return this.weightedSpawnOption.has(obj);
    }

    @Override
    public void remove(PDEntityType obj) throws ReferenceNoLongerValid {
        this.weightedSpawnOption.remove(obj);
    }

    @Override
    public Collection<Map.Entry<PDEntityType, Double>> entries() throws ReferenceNoLongerValid {
        return this.weightedSpawnOption.all().entrySet().stream()
                .map(entry -> new AbstractMap.SimpleImmutableEntry<>(entry.getKey(), (double) entry.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public double totalWeight() throws ReferenceNoLongerValid {
        return this.weightedSpawnOption.totalWeight();
    }
}
