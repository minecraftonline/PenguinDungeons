package com.minecraftonline.penguindungeons.util;

import com.google.common.reflect.TypeToken;
import com.minecraftonline.penguindungeons.customentity.NBTEntityType;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import com.minecraftonline.penguindungeons.dungeon.WeightedSpawnOption;
import com.minecraftonline.penguindungeons.trigger.SummonTrigger;
import com.minecraftonline.penguindungeons.trigger.Trigger;
import org.spongepowered.api.data.value.mutable.Value;

@SuppressWarnings("UnstableApiUsage")
public class PDTypeTokens {

    private PDTypeTokens() { throw new AssertionError("You should not be insantiating this class!"); }

    public static final TypeToken<PDEntityType> PD_ENTITY_TYPE_TOKEN = new TypeToken<PDEntityType>() {};

    public static final TypeToken<Dungeon> DUNGEON_TYPE_TOKEN = new TypeToken<Dungeon>() {};

    public static final TypeToken<WeightedSpawnOption> WEIGHTED_SPAWN_OPTION_TOKEN = new TypeToken<WeightedSpawnOption>() {};

    public static final TypeToken<Trigger> TRIGGER_TOKEN = new TypeToken<Trigger>() {};

    public static final TypeToken<SummonTrigger> SUMMON_TRIGGER_TOKEN = new TypeToken<SummonTrigger>() {};

    public static final TypeToken<Value<ResourceKey>> RESOURCE_KEY_VALUE_TOKEN = new TypeToken<Value<ResourceKey>>() {};

    public static final TypeToken<NBTEntityType> NBT_ENTITY_TYPE_TOKEN = new TypeToken<NBTEntityType>() {};
}
