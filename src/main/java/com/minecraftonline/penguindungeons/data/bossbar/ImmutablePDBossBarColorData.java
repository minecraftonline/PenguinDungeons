package com.minecraftonline.penguindungeons.data.bossbar;


import org.spongepowered.api.boss.BossBarColor;
import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableSingleCatalogData;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class ImmutablePDBossBarColorData extends AbstractImmutableSingleCatalogData<BossBarColor, ImmutablePDBossBarColorData, PDBossBarColorData>
{

	public ImmutablePDBossBarColorData(BossBarColor value)
	{
		super(PenguinDungeonKeys.PD_BOSS_BAR_COLOR, value, BossBarColors.RED);
	}
	
    @Override
    public PDBossBarColorData asMutable()
    {
        return new PDBossBarColorData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }

}
