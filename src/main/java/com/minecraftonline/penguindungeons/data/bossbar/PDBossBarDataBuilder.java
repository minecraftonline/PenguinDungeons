package com.minecraftonline.penguindungeons.data.bossbar;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;

import java.util.Optional;

public class PDBossBarDataBuilder extends AbstractDataBuilder<PDBossBarData> implements DataManipulatorBuilder<PDBossBarData, ImmutablePDBossBarData>
{
    public PDBossBarDataBuilder()
    {
        super(PDBossBarData.class, 1);
    }

    @Override
    public PDBossBarData create()
    {
        return new PDBossBarData(false);
    }

    @Override
    public Optional<PDBossBarData> createFrom(DataHolder dataHolder)
    {
        return this.create().fill(dataHolder);
    }

    @Override
    protected Optional<PDBossBarData> buildContent(DataView container) throws InvalidDataException
    {
        if (container.contains(PenguinDungeonKeys.PD_BOSS_BAR.getQuery()))
        {
            return Optional.of(new PDBossBarData(container.getBoolean(PenguinDungeonKeys.PD_BOSS_BAR.getQuery()).get()));
        }

        return Optional.empty();
    }
}

