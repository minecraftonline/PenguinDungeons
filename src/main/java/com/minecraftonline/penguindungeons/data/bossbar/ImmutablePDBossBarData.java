package com.minecraftonline.penguindungeons.data.bossbar;

import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableBooleanData;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class ImmutablePDBossBarData extends AbstractImmutableBooleanData<ImmutablePDBossBarData, PDBossBarData>
{

	public ImmutablePDBossBarData(boolean value)
	{
		super(PenguinDungeonKeys.PD_BOSS_BAR, value, false);
	}
	
    @Override
    public PDBossBarData asMutable()
    {
        return new PDBossBarData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }

}
