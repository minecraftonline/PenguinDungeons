package com.minecraftonline.penguindungeons.data;

import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeDataBuilder;
import com.google.common.reflect.TypeToken;
import com.minecraftonline.penguindungeons.data.bossbar.ImmutablePDBossBarColorData;
import com.minecraftonline.penguindungeons.data.bossbar.ImmutablePDBossBarData;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarColorData;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarColorDataBuilder;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarData;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarDataBuilder;
import com.minecraftonline.penguindungeons.data.container_loot.ContainerLootData;
import com.minecraftonline.penguindungeons.data.container_loot.ContainerLootDataBuilder;
import com.minecraftonline.penguindungeons.data.container_loot.ImmutableContainerLootData;
import com.minecraftonline.penguindungeons.data.container_loot.ContainerLootableData;
import com.minecraftonline.penguindungeons.data.container_loot.ContainerLootableDataBuilder;
import com.minecraftonline.penguindungeons.data.container_loot.ImmutableContainerLootableData;
import com.minecraftonline.penguindungeons.data.customentity.ImmutablePDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.ImmutableOwnerLootData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootDataBuilder;
import com.minecraftonline.penguindungeons.data.spawning.ImmutablePDSpawnData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnDataBuilder;
import com.minecraftonline.penguindungeons.data.wand.CustomWandData;
import com.minecraftonline.penguindungeons.data.wand.CustomWandDataBuilder;
import com.minecraftonline.penguindungeons.data.wand.ImmutableCustomWandData;
import com.minecraftonline.penguindungeons.data.wand.dungeon.CustomWandDungeonData;
import com.minecraftonline.penguindungeons.data.wand.dungeon.CustomWandDungeonDataBuilder;
import com.minecraftonline.penguindungeons.data.wand.dungeon.ImmutableCustomWandDungeonData;
import com.minecraftonline.penguindungeons.util.PDTypeTokens;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import static org.spongepowered.api.data.DataQuery.of;

import java.time.Instant;

import org.spongepowered.api.boss.BossBarColor;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataRegistration;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.value.mutable.Value;
import org.spongepowered.api.util.TypeTokens;

public class PenguinDungeonKeys {

    public static final TypeToken<Value<BossBarColor>> BOSS_BAR_COLOR_VALUE_TOKEN = new TypeToken<Value<BossBarColor>>() {private static final long serialVersionUID = -1;};

    public static Key<Value<ResourceKey>> PD_ENTITY_TYPE = Key.builder()
            .type(PDTypeTokens.RESOURCE_KEY_VALUE_TOKEN)
            .id("pd_entity_type")
            .name("PenguinDungeons Entity Type")
            .query(DataQuery.of("PDEntityType"))
            .build();

    public static Key<Value<Boolean>> IS_PD_WAND = Key.builder()
            .type(TypeTokens.BOOLEAN_VALUE_TOKEN)
            .id("is_pd_wand")
            .name("Is Penguin Dungeons Wand")
            .query(DataQuery.of("IsPDWand"))
            .build();

    public static Key<Value<String>> PD_WAND_DUNGEON = Key.builder()
            .type(TypeTokens.STRING_VALUE_TOKEN)
            .id("pd_wand_dungeon")
            .name("Penguin Dungeons Wands Dungeon")
            .query(DataQuery.of("PDWandDungeon"))
            .build();

    public static Key<Value<Boolean>> OWNER_LOOT = Key.builder()
            .type(TypeTokens.BOOLEAN_VALUE_TOKEN)
            .id("ownerloot")
            .name("Owner Loot")
            .query(of("OwnerLoot"))
            .build();

    public static Key<Value<ResourceKey>> CONTAINER_LOOT = Key.builder()
            .type(PDTypeTokens.RESOURCE_KEY_VALUE_TOKEN)
            .id("containerloot")
            .name("Container Loot")
            .query(of("ContainerLoot"))
            .build();

    public static Key<Value<Long>> CONTAINER_LOOTABLE = Key.builder()
            .type(TypeTokens.LONG_VALUE_TOKEN)
            .id("containerlootable")
            .name("Container Lootable Time")
            .query(of("ContainerLootable"))
            .build();

    public static Key<Value<Boolean>> PD_SPAWN = Key.builder()
            .type(TypeTokens.BOOLEAN_VALUE_TOKEN)
            .id("pd_spawn")
            .name("Penguin Dungeons Entity Spawning")
            .query(of("PDSpawn"))
            .build();

    public static Key<Value<Boolean>> PD_BOSS_BAR = Key.builder()
            .type(TypeTokens.BOOLEAN_VALUE_TOKEN)
            .id("pd_boss_bar")
            .name("Penguin Dungeons Boss Bar")
            .query(of("PDBossBar"))
            .build();

    public static Key<Value<BossBarColor>> PD_BOSS_BAR_COLOR = Key.builder()
            .type(BOSS_BAR_COLOR_VALUE_TOKEN)
            .id("pd_boss_bar_color")
            .name("Penguin Dungeons Boss Bar Color")
            .query(of("PDBossBarColor"))
            .build();

    public static void register() {
        DataRegistration.builder()
                .dataClass(PDEntityTypeData.class)
                .immutableClass(ImmutablePDEntityTypeData.class)
                .builder(new PDEntityTypeDataBuilder())
                .id("pd_entity_type")
                .name("PenguinDungeons Entity Type")
                .build();

        DataRegistration.builder()
                .dataClass(CustomWandData.class)
                .immutableClass(ImmutableCustomWandData.class)
                .builder(new CustomWandDataBuilder())
                .id("is_pd_wand")
                .name("Is Penguin Dungeons Wand")
                .build();

        DataRegistration.builder()
                .dataClass(CustomWandDungeonData.class)
                .immutableClass(ImmutableCustomWandDungeonData.class)
                .builder(new CustomWandDungeonDataBuilder())
                .id("pd_wand_dungeon")
                .name("Penguin Dungeons Wands Dungeon")
                .build();

        DataRegistration.builder()
                .dataClass(OwnerLootData.class)
                .immutableClass(ImmutableOwnerLootData.class)
                .builder(new OwnerLootDataBuilder())
                .id("owner_loot")
                .name("Owner Loot")
                .build();

        DataRegistration.builder()
                .dataClass(ContainerLootData.class)
                .immutableClass(ImmutableContainerLootData.class)
                .builder(new ContainerLootDataBuilder())
                .id("container_loot")
                .name("Container Loot")
                .build();

        DataRegistration.builder()
                .dataClass(ContainerLootableData.class)
                .immutableClass(ImmutableContainerLootableData.class)
                .builder(new ContainerLootableDataBuilder())
                .id("container_lootable")
                .name("Container Lootable Time")
                .build();

        DataRegistration.builder()
                .dataClass(PDSpawnData.class)
                .immutableClass(ImmutablePDSpawnData.class)
                .builder(new PDSpawnDataBuilder())
                .id("pd_spawn")
                .name("Penguin Dungeons Entity Spawning")
                .build();

        DataRegistration.builder()
                .dataClass(PDBossBarData.class)
                .immutableClass(ImmutablePDBossBarData.class)
                .builder(new PDBossBarDataBuilder())
                .id("pd_boss_bar")
                .name("Penguin Dungeons Boss Bar")
                .build();

        DataRegistration.builder()
                .dataClass(PDBossBarColorData.class)
                .immutableClass(ImmutablePDBossBarColorData.class)
                .builder(new PDBossBarColorDataBuilder())
                .id("pd_boss_bar_color")
                .name("Penguin Dungeons Boss Bar Color")
                .build();
    }
}
