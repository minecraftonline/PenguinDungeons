package com.minecraftonline.penguindungeons.data.loot;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;

import java.util.Optional;

public class OwnerLootDataBuilder extends AbstractDataBuilder<OwnerLootData> implements DataManipulatorBuilder<OwnerLootData, ImmutableOwnerLootData>
{
    public OwnerLootDataBuilder()
    {
        super(OwnerLootData.class, 1);
    }

    @Override
    public OwnerLootData create()
    {
        return new OwnerLootData(false);
    }

    @Override
    public Optional<OwnerLootData> createFrom(DataHolder dataHolder)
    {
        return this.create().fill(dataHolder);
    }

    @Override
    protected Optional<OwnerLootData> buildContent(DataView container) throws InvalidDataException
    {
        if (container.contains(PenguinDungeonKeys.OWNER_LOOT.getQuery()))
        {
            return Optional.of(new OwnerLootData(container.getBoolean(PenguinDungeonKeys.OWNER_LOOT.getQuery()).get()));
        }

        return Optional.empty();
    }
}

