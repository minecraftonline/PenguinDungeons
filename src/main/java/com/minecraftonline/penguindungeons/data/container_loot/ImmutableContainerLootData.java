package com.minecraftonline.penguindungeons.data.container_loot;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableSingleData;
import org.spongepowered.api.data.value.immutable.ImmutableValue;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class ImmutableContainerLootData extends AbstractImmutableSingleData<ResourceKey, ImmutableContainerLootData, ContainerLootData>
{
	public ImmutableContainerLootData(ResourceKey value)
	{
		super(PenguinDungeonKeys.CONTAINER_LOOT, value);
	}

	@Override
    protected ImmutableValue<ResourceKey> getValueGetter() {
        return Sponge.getRegistry().getValueFactory()
                .createValue(PenguinDungeonKeys.CONTAINER_LOOT, getValue())
                .asImmutable();
    }

    @Override
    public ContainerLootData asMutable()
    {
        return new ContainerLootData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }

}
