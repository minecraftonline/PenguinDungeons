package com.minecraftonline.penguindungeons.data.container_loot;

import java.util.Optional;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractSingleData;
import org.spongepowered.api.data.merge.MergeFunction;
import org.spongepowered.api.data.value.mutable.Value;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class ContainerLootData extends AbstractSingleData<ResourceKey, ContainerLootData, ImmutableContainerLootData>
{

	public ContainerLootData(ResourceKey value)
	{
        super(PenguinDungeonKeys.CONTAINER_LOOT, value);
    }   

    @Override
    protected Value<ResourceKey> getValueGetter()
    {
        return Sponge.getRegistry().getValueFactory()
                .createValue(PenguinDungeonKeys.CONTAINER_LOOT, getValue());
    }

    @Override
    protected DataContainer fillContainer(DataContainer dataContainer)
    {
        return dataContainer.set(this.usedKey.getQuery(), this.getValue().asString());
    }

    @Override
    public Optional<ContainerLootData> fill(DataHolder dataHolder, MergeFunction overlap)
    {
        dataHolder.get(ContainerLootData.class).ifPresent((data) -> {
            ContainerLootData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<ContainerLootData> from(DataContainer container)
    {
        Optional<String> type = container.getString(PenguinDungeonKeys.CONTAINER_LOOT.getQuery());
        return type.flatMap(ResourceKey::tryResolve).map(ContainerLootData::new);
    }

    @Override
    public ContainerLootData copy()
    {
        return new ContainerLootData(this.getValue());
    }

    @Override
    public ImmutableContainerLootData asImmutable()
    {
        return new ImmutableContainerLootData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }
}
