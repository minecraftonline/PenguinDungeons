package com.minecraftonline.penguindungeons.data.container_loot;

import java.util.Optional;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractSingleData;
import org.spongepowered.api.data.merge.MergeFunction;
import org.spongepowered.api.data.value.mutable.Value;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class ContainerLootableData extends AbstractSingleData<Long, ContainerLootableData, ImmutableContainerLootableData>
{

	public ContainerLootableData(Long value)
	{
        super(PenguinDungeonKeys.CONTAINER_LOOTABLE, value);
    }   

    @Override
    protected Value<Long> getValueGetter()
    {
        return Sponge.getRegistry().getValueFactory()
                .createValue(PenguinDungeonKeys.CONTAINER_LOOTABLE, getValue());
    }

    @Override
    public Optional<ContainerLootableData> fill(DataHolder dataHolder, MergeFunction overlap)
    {
        dataHolder.get(ContainerLootableData.class).ifPresent((data) -> {
            ContainerLootableData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<ContainerLootableData> from(DataContainer container)
    {
        Optional<Long> time = container.getLong(PenguinDungeonKeys.CONTAINER_LOOTABLE.getQuery());
        return time.map(ContainerLootableData::new);
    }

    @Override
    public ContainerLootableData copy()
    {
        return new ContainerLootableData(this.getValue());
    }

    @Override
    public ImmutableContainerLootableData asImmutable()
    {
        return new ImmutableContainerLootableData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }
}
