package com.minecraftonline.penguindungeons.data.container_loot;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;

import java.util.Optional;

public class ContainerLootableDataBuilder extends AbstractDataBuilder<ContainerLootableData> implements DataManipulatorBuilder<ContainerLootableData, ImmutableContainerLootableData>
{
    public ContainerLootableDataBuilder()
    {
        super(ContainerLootableData.class, 1);
    }

    @Override
    public ContainerLootableData create()
    {
        return new ContainerLootableData((long) 0);
    }

    @Override
    public Optional<ContainerLootableData> createFrom(DataHolder dataHolder)
    {
        return this.create().fill(dataHolder);
    }

    @Override
    protected Optional<ContainerLootableData> buildContent(DataView container) throws InvalidDataException
    {
        if (container.contains(PenguinDungeonKeys.CONTAINER_LOOTABLE.getQuery()))
        {
            return Optional.of(new ContainerLootableData(
                container.getLong(PenguinDungeonKeys.CONTAINER_LOOTABLE.getQuery()).get()));
        }
        return Optional.empty();
    }
}

