package com.minecraftonline.penguindungeons.data.wand;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;

import java.util.Optional;

public class CustomWandDataBuilder extends AbstractDataBuilder<CustomWandData> implements DataManipulatorBuilder<CustomWandData, ImmutableCustomWandData> {

    public CustomWandDataBuilder() {
        super(CustomWandData.class, 1);
    }

    @Override
    public CustomWandData create() {
        return new CustomWandData();
    }

    @Override
    public Optional<CustomWandData> createFrom(DataHolder dataHolder) {
        return this.create().fill(dataHolder);
    }

    @Override
    protected Optional<CustomWandData> buildContent(DataView container) throws InvalidDataException {
        if (container.contains(PenguinDungeonKeys.IS_PD_WAND.getQuery())) {
            return Optional.of(new CustomWandData(container.getBoolean(PenguinDungeonKeys.IS_PD_WAND.getQuery()).get()));
        }

        return Optional.empty();
    }
}
