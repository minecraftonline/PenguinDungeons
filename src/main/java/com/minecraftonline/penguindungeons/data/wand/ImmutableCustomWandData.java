package com.minecraftonline.penguindungeons.data.wand;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableBooleanData;

public class ImmutableCustomWandData extends AbstractImmutableBooleanData<ImmutableCustomWandData, CustomWandData> {

    public ImmutableCustomWandData() {
        this(false);
    }

    public ImmutableCustomWandData(boolean value) {
        super(PenguinDungeonKeys.IS_PD_WAND, value, false);
    }

    @Override
    public CustomWandData asMutable() {
        return new CustomWandData(this.getValue());
    }

    @Override
    public int getContentVersion() {
        return 1;
    }
}
