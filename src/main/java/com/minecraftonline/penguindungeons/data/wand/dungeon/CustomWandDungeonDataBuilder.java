package com.minecraftonline.penguindungeons.data.wand.dungeon;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;

import java.util.Optional;

public class CustomWandDungeonDataBuilder extends AbstractDataBuilder<CustomWandDungeonData> implements DataManipulatorBuilder<CustomWandDungeonData, ImmutableCustomWandDungeonData> {
    public CustomWandDungeonDataBuilder() {
        super(CustomWandDungeonData.class, 1);
    }

    @Override
    public CustomWandDungeonData create() {
        return new CustomWandDungeonData("");
    }

    @Override
    public Optional<CustomWandDungeonData> createFrom(DataHolder dataHolder) {
        return create().fill(dataHolder);
    }

    @Override
    protected Optional<CustomWandDungeonData> buildContent(DataView container) throws InvalidDataException {
        if (container.contains(PenguinDungeonKeys.PD_WAND_DUNGEON.getQuery())) {
            return Optional.of(new CustomWandDungeonData(container.getString(PenguinDungeonKeys.PD_WAND_DUNGEON.getQuery()).get()));
        }

        return Optional.empty();
    }
}
