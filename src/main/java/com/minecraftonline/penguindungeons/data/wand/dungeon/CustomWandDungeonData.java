package com.minecraftonline.penguindungeons.data.wand.dungeon;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractSingleData;
import org.spongepowered.api.data.merge.MergeFunction;
import org.spongepowered.api.data.value.mutable.Value;

import java.util.Optional;

public class CustomWandDungeonData extends AbstractSingleData<String, CustomWandDungeonData, ImmutableCustomWandDungeonData> {

    public CustomWandDungeonData(String value) {
        super(PenguinDungeonKeys.PD_WAND_DUNGEON, value);
    }

    public Value<String> dungeon() {
        return Sponge.getRegistry().getValueFactory()
                .createValue(PenguinDungeonKeys.PD_WAND_DUNGEON, getValue());
    }

    @Override
    protected Value<?> getValueGetter() {
        return this.dungeon();
    }

    @Override
    public Optional<CustomWandDungeonData> fill(DataHolder dataHolder, MergeFunction overlap) {
        dataHolder.get(CustomWandDungeonData.class).ifPresent(data -> {
            CustomWandDungeonData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<CustomWandDungeonData> from(DataContainer container) {
        Optional<String> type = container.getString(PenguinDungeonKeys.PD_WAND_DUNGEON.getQuery());
        return type.map(CustomWandDungeonData::new);
    }

    @Override
    public CustomWandDungeonData copy() {
        return new CustomWandDungeonData(this.getValue());
    }

    @Override
    public ImmutableCustomWandDungeonData asImmutable() {
        return new ImmutableCustomWandDungeonData(this.getValue());
    }

    @Override
    public int getContentVersion() {
        return 1;
    }
}
