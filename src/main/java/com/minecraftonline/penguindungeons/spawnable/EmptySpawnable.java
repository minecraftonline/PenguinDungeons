package com.minecraftonline.penguindungeons.spawnable;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.World;

public class EmptySpawnable implements Spawnable {

    public EmptySpawnable() {}

    @Override
    public Entity spawn(World world) {
        return null;
    }
}
