package com.minecraftonline.penguindungeons.spawnable;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.World;

import java.util.function.Consumer;


public interface Spawnable {

    Entity spawn(World world);

    static Spawnable of(Entity entity) {
        return new SingleSpawnable(entity);
    }

    static Spawnable of(Entity entity, Consumer<Entity> onSpawn) {
        return new SingleSpawnable(entity, onSpawn);
    }

    static Spawnable empty() {
        return new EmptySpawnable();
    }
}
