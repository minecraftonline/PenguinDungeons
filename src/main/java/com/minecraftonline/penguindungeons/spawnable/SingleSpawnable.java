package com.minecraftonline.penguindungeons.spawnable;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.World;

import java.util.function.Consumer;

public class SingleSpawnable implements Spawnable {

    private final Entity entity;
    private final Consumer<Entity> onSpawn;

    public SingleSpawnable(Entity entity) {
        this(entity, null);
    }

    public SingleSpawnable(Entity entity, Consumer<Entity> onSpawn) {
        this.entity = entity;
        this.onSpawn = onSpawn;
    }

    @Override
    public Entity spawn(World world) {
        world.spawnEntity(entity);
        if (onSpawn != null) {
            onSpawn.accept(entity);
        }
        return entity;
    }
}
