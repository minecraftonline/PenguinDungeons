package com.minecraftonline.penguindungeons.customentity.human;

import java.util.UUID;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;
import org.spongepowered.common.entity.living.human.EntityHuman;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public abstract class HumanType extends AbstractCustomEntity {

    public HumanType(ResourceKey key) {
        super(key);
    }

    public abstract Text getEntityName();

    public abstract Text getDisplayName();

    @Override
    public EntityType getType() {
        return EntityTypes.HUMAN;
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeHuman(world, pos), this::onLoad);
    }

    protected Human makeHuman(World world, Vector3d blockPos) {
        Human human = (Human) world.createEntity(EntityTypes.HUMAN, blockPos);
        // pretty sure name visibility doesn't do anything here
        // as player names are always visible unless set otherwise by scoreboard teams
        human.offer(Keys.CUSTOM_NAME_VISIBLE, true);
        human.offer(Keys.DISPLAY_NAME, getEntityName());
        human.offer(Keys.PERSISTS, !shouldDespawn());
        human.offer(new PDEntityTypeData(getId()));
        human.offer(new PDSpawnData(true));

        ((EntityHuman) human).setCanPickUpLoot(shouldPickUpLoot());

        applyEquipment(human);
        applyLootTable(human);

        return human;
    }

    public abstract boolean shouldDespawn();

    public abstract boolean shouldPickUpLoot();

    protected void applySkinFromUUID(Human human, UUID uuid) {
        human.offer(Keys.SKIN_UNIQUE_ID, uuid);
    }

    protected void applySkinFromTexture(Human human, final String texture, final String signature) {
        human.offer(Keys.SKIN_SIGNATURE, signature);
        human.offer(Keys.SKIN_TEXTURE, texture);
    }
}
