package com.minecraftonline.penguindungeons.customentity.human;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.builtin.SwimmingAITask;
import org.spongepowered.api.entity.ai.task.builtin.WatchClosestAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStack.Builder;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.AttackPlayerIfReachable;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.HasEffects;
import com.minecraftonline.penguindungeons.customentity.HasProjectileData;
import com.minecraftonline.penguindungeons.customentity.JumpToTarget;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.customentity.SummonMinion;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarColorData;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumParticleTypes;

public class HerobrineBoss extends Herobrine {

    protected double BOSS_HEALTH = 300.0F;
    private static final double NO_DROP_CHANCE = 0.0F;
    private static final double LOW_DROP_CHANCE = 0.1F;

    public HerobrineBoss(ResourceKey key) {
        super(key);
    }

    @Override
    protected Human makeHuman(World world, Vector3d blockPos) {
        Human human = super.makeHuman(world, blockPos);

        human.offer(Keys.MAX_HEALTH, BOSS_HEALTH);
        human.offer(Keys.HEALTH, BOSS_HEALTH);
        human.offer(new PDBossBarData(true));
        human.offer(new PDBossBarColorData(BossBarColors.RED));
        human.offer(new OwnerLootData(true));

        // equivalent to netherite armour
        ((EntityLivingBase) human).getEntityAttribute(SharedMonsterAttributes.ARMOR).applyModifier(new AttributeModifier("Herobrine Armor", 20, 0));
        ((EntityLivingBase) human).getEntityAttribute(SharedMonsterAttributes.ARMOR_TOUGHNESS).applyModifier(new AttributeModifier("Herobrine Armor", 12, 0));
        ((EntityLivingBase) human).getEntityAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).applyModifier(new AttributeModifier("Herobrine Armor", 100, 0));

        return human;
    }

    @Override
    protected void addAITasks(Human human) {
        Agent agent = (Agent) human;

        SwimmingAITask swimTask = SwimmingAITask.builder()
                .swimChance(1.0F)
                .build(agent);

        WatchClosestAITask watchTask = WatchClosestAITask.builder()
                .watch(Player.class)
                .maxDistance(50.0F)
                .build((Creature) agent);

        AITask<?> rangedAttackTask = new StraightProjectileAttack.RangedAttack(human, Optional.empty(), Optional.of(SoundEvents.ENTITY_BLAZE_SHOOT),
                AIUtil.ProjectileCreator.forSmallFireball(
                        AIUtil.ProjectileForImpact.forMultipleImpacts(
                            AIUtil.ProjectileForImpact.forImpactDamage(5.0F),
                            AIUtil.ProjectileForImpact.forImpactSetEffects(new HasEffects()
                            { public List<PotionEffect> getEffects() { return Collections.singletonList(new PotionEffect(MobEffects.BLINDNESS, TICKS_PER_SECOND*2)); }}, false),
                            AIUtil.ProjectileForImpact.forImpactAreaOfEffect(0.5F, TICKS_PER_SECOND*3, getProjectileData())
                        )
                ), 1.0D, 25, 75, 50.0F);

        AITask<?> attackTask = new AttackPlayerIfReachable.AttackPlayerIfReachableTask(human, 4.5D);

        AITask<?> minionTask = new SummonMinion.SummonMinionTask(human, CustomEntityTypes.ANGRY_ZOMBIE_PIGMAN,
                SoundEvents.ENTITY_ZOMBIE_PIG_ANGRY, 60, 180, (float) BOSS_HEALTH / 2);

        AITask<?> jumpTask = new JumpToTarget.JumpAtTarget((Creature) human, 1.0f, 10.0D, 25.0D, 0.1f, SoundEvents.ENTITY_MAGMACUBE_JUMP);

        Optional<Goal<Agent>> normalGoals = agent.getGoal(GoalTypes.NORMAL);
        if (normalGoals.isPresent()) {
            normalGoals.get().addTask(0, swimTask)
                             .addTask(2, attackTask)
                             .addTask(3, rangedAttackTask)
                             .addTask(4, jumpTask)
                             .addTask(7, minionTask)
                             .addTask(8, watchTask);
        }

        Optional<Goal<Agent>> targetGoals = agent.getGoal(GoalTypes.TARGET);
        if (targetGoals.isPresent()) {
            targetGoals.get().addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers(human, true, true))
                             .addTask(2, (AITask<?>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) human, EntityPlayer.class, true));
        }
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A powerful Herobrine which shoots fireballs that create wither area of effects"));
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        return super.makeEquipmentLoadout()
                .mainHand(makeSword(), NO_DROP_CHANCE)
                .boots(makeHead(), LOW_DROP_CHANCE);
    }

    private ItemStack makeSword() {
        Builder sword = ItemStack.builder().itemType(ItemTypes.GOLDEN_SWORD);
        List<Enchantment> enchants = new ArrayList<Enchantment>();
        enchants.add(Enchantment.of(EnchantmentTypes.SHARPNESS, 7));
        enchants.add(Enchantment.of(EnchantmentTypes.KNOCKBACK, 4));
        enchants.add(Enchantment.of(EnchantmentTypes.FIRE_ASPECT, 3));
        enchants.add(Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1));
        sword.add(Keys.ITEM_ENCHANTMENTS, enchants);
        return sword.build();
    }

    private ItemStack makeHead() {
        return PlayerSkinHelper.makeHead(SKIN_TEXTURE, SKIN_SIGNATURE, UUID, getDisplayName().toPlain());
    }

    private static HasProjectileData getProjectileData() {
        return new HasProjectileData()
        {
            public List<PotionEffect> getEffects() {
                return Collections.singletonList(new PotionEffect(MobEffects.WITHER, TICKS_PER_SECOND*3));
            }

            @Override
            public EnumParticleTypes getParticle() {
                return EnumParticleTypes.BLOCK_DUST;
            }

            @Override
            public int getParticleParam1() {
                return Block.getIdFromBlock(Blocks.OBSIDIAN);
            }

            @Override
            public int getParticleParam2() {
                return 0;
            }
        };
    }
}
