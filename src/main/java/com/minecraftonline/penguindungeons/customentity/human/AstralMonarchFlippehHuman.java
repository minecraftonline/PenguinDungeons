package com.minecraftonline.penguindungeons.customentity.human;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.builtin.SwimmingAITask;
import org.spongepowered.api.entity.ai.task.builtin.WatchClosestAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStack.Builder;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.AttackPlayerIfReachable;
import com.minecraftonline.penguindungeons.customentity.AvoidTargetOnLowHealth;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.HasEffects;
import com.minecraftonline.penguindungeons.customentity.ProjectileData;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarColorData;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarData;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.mixin.EntityMixinAccessor;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumParticleTypes;

public class AstralMonarchFlippehHuman extends HumanType {

    private static final double BOSS_HEALTH = 250.0F;
    private static final double MAX_BOSS_HEALTH = 1000.0F;
    private static final double NO_DROP_CHANCE = 0.0F;
    private static final double FULL_DROP_CHANCE = 2.0F; // chance of 2 means that the item isn't randomly damaged when dropped

    private static final String SKIN_TEXTURE   = "ewogICJ0aW1lc3RhbXAiIDogMTcwNTk4NDI1NzUyOCwKICAicHJvZmlsZUlkIiA6ICI4MTBjNmRiYmY5OTc0ZDg2OWJkNDRlNjkxZDRiMWU2ZiIsCiAgInByb2ZpbGVOYW1lIiA6ICJNb25hcmNoRmxpcHBlaCIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9mNWIwM2YxYjhiNTVmZjg3MWNjZjE5OWFjMGZhODQyNGJhMWY5MmEwYjA4YjI5MTZhMDI2N2MzODgyMmMyMDM0IgogICAgfQogIH0KfQ==";
    private static final String SKIN_SIGNATURE = "ev3CH8Du+cbdomX1jFMudsW4K8VOShYRN/2dkEQX7E5fLIeRpe+eF0AAiE0GlfnjAKdUuCDIqWFApZiIIYIcIodPUX725Kr240mkK5ozELKB2D2iIAHSf3wmUZyTCtm2m2AdjzYn6modIQRBx5V5dfHII9jqyxLI8qaE7eE2sUclrPzFW6Zxk4ElQeYv93hUHwsc6b2DKGQ4QDkbBNHsUFY9OKXL4ntlI+gVOI/6+7Oz7yK8ouYndPL7ngvK3AKsWsg2C48vPTgTnsoDS874dZ4gADNXVDES7VqTWtKTGbVBsfiKWO264yhCMzFb4KefKmTDZwuiuhtMdPBVmRoUFYIl5YOhmdllu0tpEnJlqZSE6w3Cg/7TcLPM5fvXbBgjDM/INS51djGR137oD2Hz4ORxUmOvDiwoGuhMPaaZ5xruJriYh12pbwAVWY54TmkUX2DhG2cJsEOKkHRr3Wm+bXQHYc90yeyOcJofvhcS2/AYuF59euH2F2VkcIrC/qtZK8mPU/KHgqX9jjgkEAA4Yx0S/gR08o2RS3kVHI6fH5CxAjVr0qRJ3patrTQSMNSlRFEs4sMPNBzQaioysS4maJN9MhyxFvqkZlFqELr3R1LvjSIO6Vv8tU1MlRlOrxkDDIqPF/nYNjie/HwO6MIPyRXVFS6XnIlSdC+wcCUViG4=";

    private static final String UUID = "47892b12-4acc-5abb-acbf-cc2d96733e97"; // astral_monarch_flippeh

    public AstralMonarchFlippehHuman(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ENDERMAN;
    }

    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Human)) {
            throw new IllegalArgumentException("Expected a human to be given to AstralMonarchFlippehHuman to load, but got: " + entity);
        }
        Human human = (Human) entity;
        addAITasks(human);
        ((EntityMixinAccessor) human).setIsImmuneToFire(true);
    }

    @Override
    protected Human makeHuman(World world, Vector3d blockPos) {
        Human human = super.makeHuman(world, blockPos);

        human.offer(new PDEntityTypeData(getId()));
        human.offer(new OwnerLootData(ownerLoot()));
        human.offer(new PDSpawnData(true));
        human.offer(new PDBossBarData(true));
        human.offer(new PDBossBarColorData(BossBarColors.YELLOW));

        // boss health, starts off below max
        human.offer(Keys.MAX_HEALTH, MAX_BOSS_HEALTH);
        human.offer(Keys.HEALTH, BOSS_HEALTH);

        applySkinFromTexture(human, SKIN_TEXTURE, SKIN_SIGNATURE);

        // equivalent to netherite armour
        ((EntityLivingBase) human).getEntityAttribute(SharedMonsterAttributes.ARMOR).applyModifier(new AttributeModifier("Astral Armor", 20, 0));
        ((EntityLivingBase) human).getEntityAttribute(SharedMonsterAttributes.ARMOR_TOUGHNESS).applyModifier(new AttributeModifier("Astral Armor", 12, 0));
        ((EntityLivingBase) human).getEntityAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).applyModifier(new AttributeModifier("Astral Armor", 100, 0));

        Sponge.getServer().getServerScoreboard().ifPresent(scoreboard ->
            scoreboard.getTeam("FlippehFamily").ifPresent(team -> team.addMember(human.getTeamRepresentation())));

        return human;
    }

    private void addAITasks(Human human) {
        Agent agent = (Agent) human;

        SwimmingAITask swimTask = SwimmingAITask.builder()
                .swimChance(0.9F)
                .build(agent);

        WatchClosestAITask watchTask = WatchClosestAITask.builder()
                .watch(Player.class)
                .maxDistance(30.0F)
                .build((Creature) agent);

        AITask<?> rangedAttackTask = new StraightProjectileAttack.RangedAttack(human,
                Optional.of(Text.of(TextColors.YELLOW, "Astral Cube")), Optional.empty(),
                AIUtil.ProjectileCreator.forSkull(AIUtil.ProjectileForImpact.forImpactWithSound(
                        AIUtil.ProjectileForImpact.forMultipleImpacts(
                            AIUtil.ProjectileForImpact.forImpactAreaOfEffect(2.0F, TICKS_PER_SECOND*20,
                                    new ProjectileData((net.minecraft.potion.PotionEffect)
                                    org.spongepowered.api.effect.potion.PotionEffect.of(PotionEffectTypes.INSTANT_DAMAGE, 0, 1),
                                    EnumParticleTypes.REDSTONE, 0, 0)),
                            AIUtil.ProjectileForImpact.forImpactDamage(6.0F),
                            AIUtil.ProjectileForImpact.forImpactSetEffects(new HasEffects()
                            { public List<PotionEffect> getEffects() { return Collections.singletonList(new PotionEffect(MobEffects.GLOWING, TICKS_PER_SECOND*8)); }}, false),
                            AIUtil.ProjectileForImpact.forImpactTeleportRandomly()),
                        SoundEvents.BLOCK_END_PORTAL_FRAME_FILL)), 0.7D, 25, 80, 30.0F);

        AITask<?> attackTask = new AttackPlayerIfReachable.AttackPlayerIfReachableTask(human, 4.0D);

        AITask<?> runAwayTask = new AvoidTargetOnLowHealth.AvoidTargetOnLowHealthTask(human, 40.0F, 1.3D, 1.4D, BOSS_HEALTH / 4);

        Optional<Goal<Agent>> normalGoals = agent.getGoal(GoalTypes.NORMAL);
        if (normalGoals.isPresent()) {
            normalGoals.get().addTask(0, swimTask)
                             .addTask(1, runAwayTask)
                             .addTask(2, attackTask)
                             .addTask(3, rangedAttackTask)
                             .addTask(8, watchTask);
        }

        Optional<Goal<Agent>> targetGoals = agent.getGoal(GoalTypes.TARGET);
        if (targetGoals.isPresent()) {
            targetGoals.get().addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers(human, true, true))
                             .addTask(2, (AITask<?>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) human, EntityPlayer.class, true));
        }
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.YELLOW, "Astral_Monarch_Flippeh");
    }

    @Override
    public Text getEntityName() {
        // player usernames can not be over 16 characters (including formatting)
        // the extra "_Flippeh" suffix will be added via scoreboard team
        return Text.of(TextColors.YELLOW, "Astral_Monarch");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Astral Monarch Flippeh human version, immune to potions/fire and shoots astral cubes"));
    }

    @Override
    public boolean shouldDespawn() {
        return false;
    }

    @Override
    public boolean shouldPickUpLoot() {
        return false;
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        return super.makeEquipmentLoadout()
                .mainHand(makeSword(), NO_DROP_CHANCE)
                .chest(makeElytra(), FULL_DROP_CHANCE)
                .boots(makeHead(), FULL_DROP_CHANCE);
    }

    private ItemStack makeElytra() {
        Builder elytra = ItemStack.builder().itemType(ItemTypes.ELYTRA);
        elytra.add(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Astral Dragon Wings"));
        return elytra.build();
    }

    private ItemStack makeSword() {
        Builder sword = ItemStack.builder().itemType(ItemTypes.GOLDEN_SWORD);
        List<Enchantment> enchants = new ArrayList<Enchantment>();
        enchants.add(Enchantment.of(EnchantmentTypes.SHARPNESS, 5));
        enchants.add(Enchantment.of(EnchantmentTypes.KNOCKBACK, 4));
        enchants.add(Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1));
        sword.add(Keys.ITEM_ENCHANTMENTS, enchants);
        return sword.build();
    }

    private ItemStack makeHead() {
        return PlayerSkinHelper.makeHead(SKIN_TEXTURE, SKIN_SIGNATURE, UUID, getDisplayName().toPlain());
    }
}
