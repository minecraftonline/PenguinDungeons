package com.minecraftonline.penguindungeons.customentity.human;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.meta.PatternLayer;
import org.spongepowered.api.data.type.BannerPatternShapes;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.builtin.LookIdleAITask;
import org.spongepowered.api.entity.ai.task.builtin.SwimmingAITask;
import org.spongepowered.api.entity.ai.task.builtin.WatchClosestAITask;
import org.spongepowered.api.entity.ai.task.builtin.creature.AttackLivingAITask;
import org.spongepowered.api.entity.ai.task.builtin.creature.WanderAITask;
import org.spongepowered.api.entity.ai.task.builtin.creature.target.FindNearestAttackableTargetAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStack.Builder;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class StevePolice extends HumanType {

    private static final double NO_DROP_CHANCE = 0.0F;
    private static final double FULL_DROP_CHANCE = 2.0F; // chance of 2 means that the item isn't randomly damaged when dropped
    private static final double WARRANT_DROP_CHANCE = 0.085F;
    private static final double HEAD_DROP_CHANCE = 0.01F;

    private static final DataQuery ITEM_OWNER_PATH = DataQuery.of("UnsafeData", "OwnerUUID");

    // these include some player profile information from the original skin users, in base64
    // which seems to be needed to get these to work for human skins, but is not needed for player heads
    private static final String STEVE_SKIN_TEXTURE       = "ewogICJ0aW1lc3RhbXAiIDogMTY5ODM3NzY4OTc0OSwKICAicHJvZmlsZUlkIiA6ICI5OWEzNzcyNDI4MzI0YWEyODU2M2U2ZTY3MmExYjBjNSIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyU3RldmUxIiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzU4NzliODAwNjQwNGFiYmNiZGQ0ZTc0MDI0NDVlNzM0ZTAxYjdhNTkyNzkzYjk0OGRlMDc2ZTk1NTYyNzhkYmYiCiAgICB9CiAgfQp9";
    private static final String STEVE_SKIN_SIGNATURE     = "Q/iz4Z+EPhbk7ggPh/6BP5VgJo1CJ3WX0OfIkYClA0QMJUXIYaMfUXpq3do9NxIutsx7jm08ELdTn3yKOV2cZDOmMQ0uWQs09Kyw046O1DmKNkPjYnMRynSJe8FIuO2UWZ5fcMSEQ3lymblnxFRqRTuzTHj/wEJYGYmhyVRenHE/aseFy+dwTAkXvNjuYBdcRSBh/1tzeqTwSPiZOqAIg61TGJ2W10lwRvCMCcL544QmMkzBdzEIocj44MK9mURI1NXKVlkSDC/lZD9eX/fMw79PasmlY4ghHC1Jvz9PWtJAwRCSDBl3dBiZr3bHJzM0vghKhSfY4pKl7DTEQD+8ClEKOOowPdpVpWqDnF9F1/UlOGD4nm7q/5DoNIJDK74N2ozQjLwTSckIotDCisSbZj6CiibhIoL0cJ2WoJgef/PHX91DlzqhK/u0EQRNcsIW+3xERaWehDqjZD6v25ErBoiZxuxx7BI2n8TBe8ZxLuA9nhakCAh2d+uy4kygxvABpJoUj9isBUOlyY5UfRWf+EUD+QACjeiN0o3fzP4p00C9HQJmAKI6Zd2tcHA5J4HsXWXJuq7MsCA7xjhh+YueIUtUl7OoxuEF/0QiBdZaPCGKxYEiq6dWO+jRgWfYY3Fgitbf6weWX8oQ0dV9KPA8lEfVA60+k/6+poMboGshUR8=";
    private static final String ALEX_SKIN_TEXTURE        = "ewogICJ0aW1lc3RhbXAiIDogMTcwMDM0ODExODg1NiwKICAicHJvZmlsZUlkIiA6ICI0YWI4ZjgwZjNjODE0MGExOGI5ZDI1MmYxYTJiNGYzOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyQWxleDEiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDI3ZWJkNDhmYjY0M2ZlOWM2ODAwMjQxMWE5ZWI0ZDhhOWFiYjQyMDhhYTZhOWZmZThiMTQxMDZmMjRhM2QxNCIsCiAgICAgICJtZXRhZGF0YSIgOiB7CiAgICAgICAgIm1vZGVsIiA6ICJzbGltIgogICAgICB9CiAgICB9CiAgfQp9";
    private static final String ALEX_SKIN_SIGNATURE      = "MuYsswQanQlSbra7XzJqASWQplNhvLZgNtjsExzxUlPkOKxzrSBaaWaocyTi9kzwTIFH+re0DwtYT/gItjD6I9JznnSSI7uhEAn3Zsfepq+wvI1FY0GuovSTCMl2b7Y7QlutWCArCfEKX4PztUXgney0SaPbmBtXIX5b6cc+/ce8MzAsaDb1bFfO7Lz25lTAbP02gKEfdZHimyz2X9oiufOI+qoLxy2wlCj1ktWAw9jOtpAbDNvtcl0TGNJBIs4o4tw2JmH/mSpHhhwgKO/YDI5/Hn0+ko8y636sRreDQQqJv9tnaJWr+ExXUsRNeACOX1cY+YFYcy1OTZeXaPQFpUaXXk66vNkRIXxykOJ/8A4fbEm2pJ+68zXyvtBnhBQF1zZGnWVmjXxRXWJJY/yVMq7V3jvCOjbHv/rNv26jv7aJCMYaiUp3l1qCI8zh2Tvu1ChyWEvlSiv1ZBDXcvE4zbj1Yi+7MZmExatkyg37RlCVQHEoCJaosiEnOizmaBMWOyoTyggaRyxWwitzgcovmgJhDIx2PKosotspnaRyaEXjbfctriUgbslIioWknk6miiUVZM0dwbBW1VnXZNvmr7yMLmcRYcyoC1bEF4jBT9xbaVyfXgXWWKrtyGOirPQTFd9sManQ5YiKDJL0q9dw1xKOupvPYSKRQu5L+QMFO84=";
    private static final String ZOMBIE_SKIN_TEXTURE      = "ewogICJ0aW1lc3RhbXAiIDogMTY5ODM3ODM4MjczOSwKICAicHJvZmlsZUlkIiA6ICI0YWI4ZjgwZjNjODE0MGExOGI5ZDI1MmYxYTJiNGYzOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyQWxleDEiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTYwMjNlYjhkYjM4MjY4YjdiNmJjNGYyMTNmNjk4NzY2MDkyZmIzZDljNjY2ZGQ1NjVhNTA5MDRkZTQ4ODQ2ZSIKICAgIH0KICB9Cn0=";
    private static final String ZOMBIE_SKIN_SIGNATURE    = "LM71Ewr5O5HUnQc0x457d+xrbpNESsgTC9lNyCAr/zh+yYbIE3xPFZuTXpWUZTeEM6KHFLWcGkrlQNJQasX6PiVhzfIpRfyskhysqrX2plvq0h3kTPR8tfcdKt+WIsww11uzbXC1kKh8yhfZBZLx5mxsqMs8ztp6OGRnpVGIZF9DtULmYS2oUfzIJ3RsQTuhap1mr0C7r4uQKLyHtYI2NIeCtq70CI0y0r5AxeqpO1RwX7hzCiaED/SEEQlu7CG6NIeS4gQoibApwnhdwAI9hADRggqeV4kZC0sa1duXfuSNfWHHhRX+I54M1Apc8+JMqG84zI+OyzlXSniQXLK9Nq1TPeTHaBul6UkGafzN+CLh6LT15YQ87+i/TUvL/kFBmVLIoxmA32HBGkLZORajBiLPPC6uDFlSMYajxQINvltempC825+T0AgR5gQ6dStBzl3Rfxr2Z6tNSHU8eQVTGd5WQLRahB/Ng5Y9YPB3LxV+saFQqaLWBpAKC6lkXE0IcG3SWjqRTMJ0o7/XksSjr2HA7NyyjZBhIXNSNYdYev2Pnb/wbOr+DDix03RqsE8glS0Y7assGkmJAvszPHt4c60swltDhRyXME/tZC4weFho3kxSXhUaEJe8LLsAB/dOXM2SAg7fpgtHEtZdDDe7jIcHypiwri+1qlhjQXMzq90=";
    private static final String PIGZOMBIE_SKIN_TEXTURE   = "ewogICJ0aW1lc3RhbXAiIDogMTcwMDI2Nzc2MzkyOSwKICAicHJvZmlsZUlkIiA6ICI0YWI4ZjgwZjNjODE0MGExOGI5ZDI1MmYxYTJiNGYzOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyQWxleDEiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTQwYTBlYWFhYmYzZWM1ZjhjYzVhMThhZjJiNzg2YjdlZTYyN2YyZWYyNDQ5ODM1MWNmODg3NDFkMWNmMGRmYyIKICAgIH0KICB9Cn0=";
    private static final String PIGZOMBIE_SKIN_SIGNATURE = "uKRYewG9kS9yclUPkY4l1DXuaTRO00YN8Z0g9auBRevBRmFavhpAX+uCu/0s5WpNYNvMS9/DtvM3QeRQnny0UqYqmjMUhA76cJss6LF72OmUxgQEqmbHU+2IWLAshH99sgAOiCfPGVghGx50AAgxG2NgCh6krxY2LeXEr9o55Wh3RyO3p1G3MkiMh4UWX2DtEmDUrUQ0pd1UtqpaAYbwPnChu7ol6k8hAkx6jvmu5KBXNFfXSWnbyoFsfA7QI9jdrhW6iP33w29BB+IsiROgkqZl3FpAYl6kIO2B3Ume1fNa0YZeWvG7h4VTS9up+xCcGg0iyruxQm3iz81v4Aj1/1UEFZhOClNH3QmXAK9y+Z77SxkqMjKzEuQHkOUnwrZpq+OJnFzuV/jywF+y6iU+GKf79hcB3cXYe+d/+mUIv3q2QIBVP5i7U8jU9M+cgGw+6QGVfQcN06cSrVpGobuibc5YDam/OXtAPWEZ3Dhx8qrXMLSDidDjZwjSFtysTBecw++3M463GNBDiU7qvTF8PTuU2Iz5ooWYsUyUyFehnlq928Yyq9sDyMi2/ejxE1iwLoJ7Q6TzfgglpSW3NQ4HosmdY+6YSeyPnmlvcZet8I5C8e9yPOsT3Jkh34/xHf2m3fX22OdJRrTcjr1aEbzWbcJFxv4NqFSMElrgBB3EHJA=";
    private static final String PIG_SKIN_TEXTURE         = "ewogICJ0aW1lc3RhbXAiIDogMTcwMDI2NzQ3Njc0MSwKICAicHJvZmlsZUlkIiA6ICI0YWI4ZjgwZjNjODE0MGExOGI5ZDI1MmYxYTJiNGYzOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyQWxleDEiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTE5NTMyMGQ1ZDM1MmFiNzdmNzY5NDczMTIzZWJjY2VhZjliZjcxNjgzNDhjN2UxMjY3YTA1YjJkZjVkMWM3NCIKICAgIH0KICB9Cn0=";
    private static final String PIG_SKIN_SIGNATURE       = "jCpjc7Ch/N8fpPB8TBGHztQYyIGvN7hW6CqpjNjdwzNXz1FWZbXdZfgnBBBNyhJRIDer9JgyB+Mmfq1G8F/8AX15F4/od7LKlHYwgxq5gH8b/PBQ+iwRom1xgZdz49jwr7BmTnXf7VVH7HVA37GL8w5JLXSR5ofKtXcC/fnRxOPgZmEfI0SRVLhGymxGSwvkn9/lJ4PQLVlINajLp6Qw3flET5jv6XsD8Ob/WDHHKKDKFX5YwpXjUVLVBpIToCcZ25vVIYZsNeD9f2Fq702eSmR9l2W+JlXtUToqU+4JLjUdF04NhAjr46I7hM6+a+ZlyIJ3d3j0C6DHnHrjPtALBux5AJTwEgU2Zf4uMvcMeClKJ0oQtNT6xgRn6b1NBsPnCaxgsIObw2QisoK9B/L7gw4ltYW+iNTNS2hIaSqXj8qFpOmtxvYcmLEJrLTpQaRiI3IDsCax6w7EMI6DooOM9sqGshn1EVFTqb46rcjOmBAt7eni/LQROYu07O2Lifpxn69QwRSYC8KP7w5aG+RvDKyDC2FlQhU+xnWZTRoFQrsOWDwban5Gx3ni7EnXygBSoOiVQ7s6Ry+PM26j+77ADfaJ7eTkiBh1DDh8RvuDfeC0B2Os9U0i0tamk3DnE0ZfIPU7L2iw6EXgYs+ASLRnQ2I4Um1Kg8EsyBPTWSSLpl4=";
    private static final String SPACE_SKIN_TEXTURE       = "ewogICJ0aW1lc3RhbXAiIDogMTcwMDI2NDQ1NjkxNSwKICAicHJvZmlsZUlkIiA6ICI0YWI4ZjgwZjNjODE0MGExOGI5ZDI1MmYxYTJiNGYzOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyQWxleDEiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmVlNDY3ODAyNWM3NjM4ZTJkYmJkNTk2YWU4YTY3NTQxZTgyNGRmOTI3Mjk3OTUxYWMwY2FhYTA0ZmEwYWIxZSIKICAgIH0KICB9Cn0=";
    private static final String SPACE_SKIN_SIGNATURE     = "nq/gdRfsSkyEsDD7V2mNeBCj3dwWzvnC85G+ik8ffCPe75iyBM0JX63YVkG987vIIHMgMG8MhJkwaSYnuIH7T+Bax5RUtK8+nnDwCDit0m4nl+lmop1mVp8/0fMXlMzXfg/HEpTX0kIowmF1PvcNVxF0v8LVyxVWKli2Cb2N608SKlI7yYEOe4fA0IQ25mGJlfG1zG4idkzXsxkPXrn+EjNRFDBPy+qjO/PNIqseVOlAxyNeOYYkvk2msTI9xd5i0z7g5RbOmbLLCEgLlX1zBwpLaaTnvzEMldUDk53ENCq6WFIo8r2lfrjP/5wxmfTVWVajcbuADo2Xss5J4VCrwrloIqlMnIa3ajGshvGjR7f+eVl4/HwD3MlwWKlcfGDb2xdv7p4ro0Ag0BNmqnDeup8Vee9pTvAKuTS+oIvkuJHzRdA6NAVUdw7TLDXzJeTFyfQUyqcvRrhY6wDpWdYUTC9vuHWOLaVJKqXxtMi30GeDXAvrvIp38EUIw5HMFzzsd/07znhcYx5O/nNx8k5ZgEq7DVHBD1ImaFiYB12LfuwcLaTZvAAFqqMKalUYbTZyEwUiM4+zN6wSJYpRkEHzc1ky2DPdWnPxX3dCn0UeD+H/8kW7wPt8aNA5+ZQaWKxWHD6qz9vNun1bfgaZbvCwQ7qE0A6DVA3bFNO65wo8daI=";
    private static final String RIOT_SKIN_TEXTURE        = "ewogICJ0aW1lc3RhbXAiIDogMTcwMDE5ODg5NTIxNSwKICAicHJvZmlsZUlkIiA6ICI0YWI4ZjgwZjNjODE0MGExOGI5ZDI1MmYxYTJiNGYzOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyQWxleDEiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODRlOWUzMWQwNTY2MmMwOTQ3OWI2OWNiNjdkNDkzMmQ5OTZhZjE1MWZjOWRmMTY0MDRhYjdkM2EzMWQ4MTdjIgogICAgfQogIH0KfQ==";
    private static final String RIOT_SKIN_SIGNATURE      = "W0vgQDB/YZQ5wMOFSD9Pb2pzWviqxzJq62N/pC0coWQlXgyaE5rx0N7sEUIAXPD2En6V/mgBXXMc3+LMMGx+pNz2qHRBclrfVrqfcvW1xHJ4DpXqWZOedPkNu0wm/UbTizJi4rP9I0IwBbpRm8s3eoPfwzMmB2fcCHPVBArQK69DD7/T4Gf2dbIQyvI5lWAzG2NGUHj9jTRIX3UFAKQh7ufaCGV8P7dweT1wfWPSjBFGcO72mMEs9V0aK5bQL5nQbhW68Ec5/AQiUuAXE4vT0DKP9CJVC5RLTh+jH18twd99rdiQHgHgxEamyt7UWKRxyJL27jaPR1nbAHP1KKrQnZpbwUuwyrqLFYpe4Vr39jTcIZAu145F1EnjP+5OuHWC6fCF9prqiSPBk1BAI7F4FCg1n30HzQRzmyJcoJ1eK79b5d/5G1Nz6UbmbrTNZdenle+AkIHl/buUiyon5+9VQ+p2z4Ir1R3iTCthpvgB5PYpWA0i1DhejBwvEygqujWFh9gSfSFy/OLzvHuv0lE8cNNU/S2NOaa53jOwQgL6ujOPosmjCdasBmaKCl3v54RZg1gqKQ3OQFxrmyEiTWnWHAMwVWw4BMoVdnIx49lGtpByi0Nr9mopINQRu3vP3dHfV9Cnjq3h3TodOr7kTMjDZ8FWI+0CjcTDdaWXfubEvD4=";
    private static final String RIOT_RIOT_SKIN_TEXTURE   = "ewogICJ0aW1lc3RhbXAiIDogMTcwMDIwMDMxNDM5OCwKICAicHJvZmlsZUlkIiA6ICI0YWI4ZjgwZjNjODE0MGExOGI5ZDI1MmYxYTJiNGYzOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyQWxleDEiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTM5OTkyMmZlNmMxMzZkNjI4NGRhOTc4OWQ2NjQ5ZmY0ZTBiNjRhNzQ0MTdjOTcyY2FkYjAyNTJmMTQzMWVjYyIKICAgIH0KICB9Cn0=";
    private static final String RIOT_RIOT_SKIN_SIGNATURE = "vf30aJFPvftokMVv4/En1tB/QX8mazE5L20o4NcAuNeLqJhhzWfiTN21PZryQ19vRpqAbhqC1Gw9H8A7VBHg4q5BRXJsSxtJiodE1PmED3S/2EfeMOqTNdkYa6oQ79aTdDKrCbdBNgAhXTxgv+trkXQh//IA36BQ++oUlYYVKBxKNIdaqBSwg3qJwdAGRSNOu5DRfdfE0vbYHt5JgoMoI7e1l7A/4V5UOPYLg8h1pYqrGYwScf74+AdytkRBJfEsckqOiP9G4tghotclk8u2pEVH6UF4CR3p28cASY80Jys4q7GkS51dVPMMzs1fun9o9frHpRCvlrFeXdhUQdOEiGX9nkaOmXUlmWRR/wIeP2O3tROImOHlIBQzmeR6RU1xR0DttXnr2VVLNe7H73+nyoSkBscZzXzRHHI0FnZb1bHK/JyzragrtAmpykKigqjUJlJLuMEVIeyqptnFAjF/3e+knwjHKQIHT+8ETihxZuQlY/XaYUjmmf5UhmIxoTISnQ/IJt1YXDKZTM0oHxZqS9zDYgOYyDX9vHMG7x3XrpgQNCvgQJSoaYAiql4PREwTulkftYed5ypyTdZ6DScxj80xnNiDhBng+YCvdT8X5h324RVcF6sBquXMurBQCGH/DarxWjy8JDqa97Uy/1J6JZxXgb6gmk/BSAjjUvPueUo=";
    private static final String ANNA_SKIN_TEXTURE        = "ewogICJ0aW1lc3RhbXAiIDogMTcwMDg5NzUzMjg4MCwKICAicHJvZmlsZUlkIiA6ICIyNTdiM2EzNWJhN2I0MDhiOWVmNWMwNjU2MjAwNzM4NSIsCiAgInByb2ZpbGVOYW1lIiA6ICJBbm5hXzI4IiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzVhZmYzY2Q2ODgzNWQ3ZWQ4MmIyNzk0ZDMxYzVhNGIxNmIyN2UxZTQ4Nzc5MDUyODJlNzIyMDAxYmE0N2RiYWMiLAogICAgICAibWV0YWRhdGEiIDogewogICAgICAgICJtb2RlbCIgOiAic2xpbSIKICAgICAgfQogICAgfQogIH0KfQ==";
    private static final String ANNA_SKIN_SIGNATURE      = "AOXHEAGPwoQF3ztncW3flNO/gui/cW1aDxbrFSpdtmYuNq0+JakQON6gBl7K9DzcYOOAKaN8vmJy688ew5W5BDXOfYZ997BjykAxZ7k73HvqTORgadGtJF2l+YAqpUqE7vVfkZeFpLvvNl6BrxsxH1rm0zb0tYQMfQg4aUQYNu3xkzuKisV9drvxvUVYcLw4dECOuOjKkH10Jlr5oXjrSmP73998LAWJGboMae+7aRK7/nxaQuexIm9caSiij9R4HA/CpYZm8bAlUbppUauvi32vPl6NoTvvBGqOHctY5XjBsJlcBLLlppdKKrPG9JvIRXi5zXhSEYQXw/CpuXRq90YDcQa1njZ0z4qk+8H0R/GUdfiR7kYKgFd2R/wMU0onTSqHdIk9t/XdgdGvu4boFnEmwTAQ3tdgozI7N+SpvwbD4OZnMUIwLFJBJqqzqhpYFIlRibvABmglUT8VVnp/AC+LhVM+NUx+2TGvQKRn/Bxcul1/nm8/mBAKM6mG/EO+SiQszfaNjoev6YdoreZqdBHHeYD7qYzyXvTcDxwzJZFCqGyoYBUEQ7A03eFj9nL56K1EiZsDsB6N6Q2JydxBPFQ5u7A/6UK/GG0hHwaF6hXVvFlNvSyqu5gjjTRu6DjeR5WeJZDV/WW0jzBkpPUUk6daHYa4itM0yzVMNoZjMx0=";
    private static final String ANNA_PINK_SKIN_TEXTURE   =  "ewogICJ0aW1lc3RhbXAiIDogMTcwMzcxMTQyNTkwMiwKICAicHJvZmlsZUlkIiA6ICIyNTdiM2EzNWJhN2I0MDhiOWVmNWMwNjU2MjAwNzM4NSIsCiAgInByb2ZpbGVOYW1lIiA6ICJBbm5hXzI4IiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2YxZWMyMGRlMzdmYjE2OWMzOWM2NTU3Y2RlOGE0YjZhZGY5YmNjYzMwZmIyZmI2ZjIxMDJiMWE4YzRkOGYwMWYiLAogICAgICAibWV0YWRhdGEiIDogewogICAgICAgICJtb2RlbCIgOiAic2xpbSIKICAgICAgfQogICAgfQogIH0KfQ==";
    private static final String ANNA_PINK_SKIN_SIGNATURE = "b/9nsuGecRByBu1e9HhaQavTyeSJRNI5l3MUrLsbx4DHqaf6cqInHojCYGuDuDaEgZcr8c8aM5c6CNdm26/uOgSWoUWlYUaK/0P/1McW1eWym6s2aUNETL5Do+WOYAe8VOFD4PFoQ5QnUDUWxSfyC2rghE03Ch3ovBnYLtAKbuH30LF0GnLjmoKkekrW/Iha+8h5PozO7tchR5GSr8x1MM5qy4CljsE801mCQD85hf+sjoB+8t7nv6Y2h36At79pm5JZd/VE4gGYKJPWfoOHBH2sFKik5cAsRJHPy3G8uMtmtDplYyhMGDobJA0l9037tm7SoABrWtPWzSoy3Gxq2zdB6m61U1QlrCdb6mcBmiZ0GrfS9TP/fjdE9AvDpkh+0ra5MPRU8ciiReJuqs+XflNhASCzTQuJub8lc1NMLv0LI2KpzruP+U0W52tPEPONSGLE1hZx8EK0DjVJN7LrecCwGTwqKgbbDRveBXtS+o5CWYNWIHpoxJvFAgwTBBZMJd7PNVoedZ7JYSWf+YDCpZea1b3PySi/sVkNzar4Ni00bI9TcbNNP9ZDvWSK7n6IBm8/NrKeFAJoJBhdBL4yaswd3OpxLdgx8hault8syItaoSUNj2VqnjRb8K6Ed2U0tKR+aQvCrUk3940d0H3JIUlkgZPhCkz5s+WU4c8vFmI=";
    private static final String BEE_SKIN_TEXTURE         = "ewogICJ0aW1lc3RhbXAiIDogMTcwMDI2NTUxMDYyOCwKICAicHJvZmlsZUlkIiA6ICI0YWI4ZjgwZjNjODE0MGExOGI5ZDI1MmYxYTJiNGYzOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyQWxleDEiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWVkODQ4ODIyOGUzNDI4NGRjZjE1N2JlOWU5NjY2MmE1MmYzMTMzODVlZGQ1NTk1ZjRiZTEyMGEwODM0NTNlYiIKICAgIH0KICB9Cn0=";
    private static final String BEE_SKIN_SIGNATURE       = "rvw4UVvFhTkfYHzm4BF+bk+Ad1czUHVO/5IxKVnPhhimUYbP2tdozyH++yANvISZgjfjn2QROoCh1Rc7DOepsEJ4ll9NSbk3ua2NVJCWVeZnnWRak+weoDmd8hJcs2jZlFaepu+Aapek8a6aEfnEbCkGsAdR5vrgEl36maaPNAJmjnMRC7VY4drpc5oNlQBuIz3+8LVXTdRHnpnhLtGytXq4GFa57kp4RoU0gtpDYZ/jZIWrYUbpeDn0+UYyhb5xJKxwoMB9v5dpiEEZMF4F+SuuFsaEs4/Ht56PaGgblM0JJsuMAUGF+fBQBsVntK6PB13BiNAlHZJZN70fxNr/U9ncVBt52lHlaz/vVrKeEKOMtkqQ7ibIs9UBdTfQIPkVIOEslOiRSzVQHkzrm3q+HMX+U0vqXqdJjA2wdrA5IYkfVR4u+GhzaMzLmVbFskkUv9VgMHa7CGs4j/3ehLEVLeAR8pqulGjoo3/df8JxUrrnyNdhT15JYx/NMT8m5qXTg1EMcPO2VpJPGtn+MX18E4nkAhB4DEGDqeg4kAln3S+aoXAEvQq6DjWs7HjS8oZ1QJBj/zSxX+nL8vJ6czqvzl1NJWFwxPdOUoHXeCRKOTLhtQyVGTf3PM+V+pC64RJf8k802KSzzbPmUYrlk+9XTRmwGMBzmLi6aDKfy7440zI=";
    private static final String CLASSIC_SKIN_TEXTURE     = "ewogICJ0aW1lc3RhbXAiIDogMTcwMDM0MzQyNjA2NCwKICAicHJvZmlsZUlkIiA6ICI0YWI4ZjgwZjNjODE0MGExOGI5ZDI1MmYxYTJiNGYzOCIsCiAgInByb2ZpbGVOYW1lIiA6ICJPZmZpY2VyQWxleDEiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjZlZWFjY2U2ZTY3NWJiYmZiMWJlNDFmNmU3ZjExMGFlMmM4YjY1Zjk1YmE4MjVmNGU4ZGViZmMzMjU5NDE2YyIKICAgIH0KICB9Cn0=";
    private static final String CLASSIC_SKIN_SIGNATURE   = "nEknNEAGmHjSu/2NHnbhfNj2TfBT2X5Xc4tIRxgZ4l4sI0Ho7ETnwSecvbYsVDJwYsiwwrQQHQp5kHzU0YoxLy2shWlpA4dYDGH7FRLOjxiE4vPERYn0s2jIUKLWhLMhuJrhnEzcBk65u+wdkjqrKvgwVMC0ysSxNunVoz3nr76HHqaDvHahcGIQp3/ZtHRt4e4ihIaZNVPUQEhXym7TztE8PP8ZuAWJIbV+l32UBM52fJGz3uNM/XsJfiejQhJ3WUbx1Qwl/l4I+TvjYRgX5iFIseKbw5vGOYRPYv40T8VthEw6lk2ROtUcUgV+E+H5P0A+hEN1aSGx6f1i7342ugkio9vN4W/ioIw4le60CyQqa9vfghGxRav8KEeGwNkufKAQ7vABXmGSjrgNx7Zv/mvAP9LL5JJaPxG6vvqEFabkKRRXyqLXNhWzYNHlh5cl1xc79rspDRebpaTpe7x86K4NOWCgMdZVOQNTM2a1M12gH0u9xv93TsXy/qM9Y+9co3IVaeNfUs4V1DtEClIqyOMAqjzdz4d5KOppo4irBy1la9l4wsdt6N4Tvjt0jhbZo+PsZZW8fpjsPRxc9L+yVV75fANk3jvPoIm0gn9MDEGT5c4yEMt40JEoPzv3iMbjxZkLss5ehjjgy4K6Bl8toqd4tV7DQzWl+5s+AZ/zJoQ=";
    private static final String CHRISTMAS_SKIN_TEXTURE   = "ewogICJ0aW1lc3RhbXAiIDogMTcwMTQ3MTg2MTQ1NSwKICAicHJvZmlsZUlkIiA6ICIyNDRjNGZiMGZiYjI0NTIzOTRkOWI3ZDFmYjkyOTU4MCIsCiAgInByb2ZpbGVOYW1lIiA6ICJHaG9taWsiLAogICJzaWduYXR1cmVSZXF1aXJlZCIgOiB0cnVlLAogICJ0ZXh0dXJlcyIgOiB7CiAgICAiU0tJTiIgOiB7CiAgICAgICJ1cmwiIDogImh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODhhYTVmNTU0Yzg0NGMxZDNjOTZlMTlmYzgzZDBiM2E1N2I3NGFmZDEwMjQwMjU0NWQ5MzQ2OTgyYjNmYWRkOSIKICAgIH0KICB9Cn0=";
    private static final String CHRISTMAS_SKIN_SIGNATURE = "WxIZOxD8p8ss6wvFfoKS8F2fsKfuhPwnx/PdisUBZbXkfz/Q0gNJ6ZUi01I1pjM+SmUDs2D5fjtkFe93Nd4Lt2GNlpfmzPjELkylVz3mGZLZ/NSZE+PwqvUQtalAo4HRw448Or3PPNoOl7astGsWw1H5BhChCVpBpPH4h4VvuhB4JH+N2HtY1Ni5tOFbgNhoqxY2PUtDCVd7v5zaqvgS4bzKNWMAhqvdrhtu6cJxvSQOml7QEAD8uQpKYxBa7nNVpMk0+0TL065/yEiqRnFWlE5BGraNEL7Q3+354dn/bLbO/pVeaE757fTmQChebw/B1noECT1T/SfxAl62APK468nBxWZcUFeAt9F0cveWw68ZA9HjHE9a2dzJHsAirKbkAiH3iTcBGvtABTH4dDt2+mAx3BS0aIKLsGIK2qzzAJo/YgjFWvDWjRRMEs2Wv+q6h0Wraj9W1X9aTpTXLqAvbq4WFEAMJTmiP9+mW9x0GoXA6FRrJkpqICGrajaX3/UDg3UPbdLxp720jfUI2dL1phZI8Q6tfntTpwKhkgw4edyJlF+CWQIJwdCtogI9GirxOINsIeQCjh1R5/KLeYYHIuUdM5UivsxE9ZOqZ2E+7a9miaN4/ol56TaaUL0j/mLk205jvGI0em5pSTqK8gdbV9IJ6w4+KX0eP1SormdB144=";

    private static final String STEVE_UUID     = "f69004fd-74fe-5461-becd-c48457980e1d"; // officer_steve
    private static final String ALEX_UUID      = "15671d64-d2d6-5736-9490-0448c79d4ec9"; // officer_alex
    private static final String ZOMBIE_UUID    = "4d23ad71-67ff-51dc-be35-515b62cd088d"; // officer_zombie
    private static final String PIGZOMBIE_UUID = "f83c0b9a-65f8-5cf9-b73b-f7d299a7a71b"; // officer_zombie_pigman
    private static final String PIG_UUID       = "244d27cb-d202-5c90-a3c1-99b64757b68b"; // officer_pig
    private static final String SPACE_UUID     = "c62f6410-7a97-5ebb-9d9c-7a51916a09ef"; // officer_space_steve
    private static final String RIOT_UUID      = "5695a5bc-6129-5533-82de-8eac24c1ce0d"; // officer_riot
    private static final String RIOT_RIOT_UUID = "b181e4de-edd5-58b6-83db-7a5758dd6885"; // officer_riot_riot
    private static final String ANNA_UUID      = "b5aed973-daec-583c-96e8-2568a05089fe"; // officer_anna
    private static final String ANNA_PINK_UUID = "a0e38c81-c033-5f98-aa4d-7722d86825f3"; // officer_anna_pink
    private static final String BEE_UUID       = "f5059a43-02ef-5b8b-a0b1-68fef4c3588e"; // officer_bee
    private static final String CLASSIC_UUID   = "6b983cee-0f00-5e4c-bcbb-269821ff8f90"; // officer_steve_classic
    private static final String CHRISTMAS_UUID = "792ab1b3-9022-51b6-aeda-cee9a1f425b4"; // officer_christmas_steve

    // Evan-owned "MinecraftOnline" account has Officer Steve skin, change if that account ever changes skin
    // private static final UUID PLAYER_UUID = UUID.fromString("678d06ff-24dd-4a27-bdde-9c2e9d02b81d");

    private static final Calendar calendar = Calendar.getInstance();
    // zombie police during Octoboer
    private static final boolean zombie = calendar.get(Calendar.MONTH) == Calendar.OCTOBER;
    // christmas police during December
    private static final boolean christmas = calendar.get(Calendar.MONTH) == Calendar.DECEMBER && calendar.get(Calendar.DAY_OF_MONTH) <= 26;

    public enum PoliceType {
        STEVE, ALEX, ZOMBIE, ZOMBIE_PIGMAN, PIG, SPACE, RIOT, RIOT_RIOT, ANNA, ANNA_PINK, BEE, CLASSIC, CHRISTMAS, RANDOM // random has to always be last
    }

    public StevePolice(ResourceKey key) {
        super(key);
    }

    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Human)) {
            throw new IllegalArgumentException("Expected a human to be given to StevePolice to load, but got: " + entity);
        }
        Human officer = (Human) entity;
        // warrant item held in offhand contains the target
        Optional<UUID> target = Optional.empty();
        Optional<ItemStack> warrant = officer.getItemInHand(HandTypes.OFF_HAND);
        if (warrant.isPresent()) {
            Optional<String> targetUUID = warrant.get().toContainer().getString(ITEM_OWNER_PATH);
            if (targetUUID.isPresent()) {
                try {
                    target = Optional.of(UUID.fromString(targetUUID.get()));
                } catch (IllegalArgumentException e) {
                    // invalid UUID
                }
            }
        }
        addAITasks(officer, target);
    }

    private PoliceType getDefaultPoliceType() {
        if (zombie) return PoliceType.ZOMBIE;
        else if (christmas) return PoliceType.CHRISTMAS;
        return new Random().nextBoolean() ? PoliceType.STEVE : PoliceType.ALEX;
    }

    @Override
    protected Human makeHuman(World world, Vector3d blockPos) {
        Human officer = super.makeHuman(world, blockPos);
        PoliceType type = getDefaultPoliceType();
        // override name and other attributes for specific kind of officer
        officer.offer(Keys.DISPLAY_NAME, getEntityName(type));
        addSkin(officer, type);

        return officer;
    }

    public Human makeHumanWithTarget(World world, Vector3d blockPos, Living target) {
        PoliceType type = getDefaultPoliceType();
        return makeHumanWithTarget(world, blockPos, target, type);
    }

    public Human makeHumanWithTarget(World world, Vector3d blockPos, Living target, PoliceType type) {
        if (type == PoliceType.RANDOM) {
            type = PoliceType.values()[new Random().nextInt(PoliceType.values().length - 1)];
        }
        Human officer = (Human) world.createEntity(getType(), blockPos);
        officer.offer(Keys.CUSTOM_NAME_VISIBLE, true);
        officer.offer(Keys.DISPLAY_NAME, getEntityName(type));
        officer.offer(Keys.PERSISTS, !shouldDespawn());
        officer.offer(new PDEntityTypeData(getId()));
        officer.offer(new PDSpawnData(true));
        addSkin(officer, type);

        makeTargettedEquipmentLoadout(type, target).build().apply(officer);
        applyLootTable(officer);

        return officer;
    }

    private void addSkin(Human officer, PoliceType type) {
        switch (type) {
        case STEVE:
            applySkinFromTexture(officer, STEVE_SKIN_TEXTURE, STEVE_SKIN_SIGNATURE);
            break;
        case ALEX:
            applySkinFromTexture(officer, ALEX_SKIN_TEXTURE, ALEX_SKIN_SIGNATURE);
            break;
        case ZOMBIE:
            applySkinFromTexture(officer, ZOMBIE_SKIN_TEXTURE, ZOMBIE_SKIN_SIGNATURE);
            break;
        case ZOMBIE_PIGMAN:
            applySkinFromTexture(officer, PIGZOMBIE_SKIN_TEXTURE, PIGZOMBIE_SKIN_SIGNATURE);
            break;
        case PIG:
            applySkinFromTexture(officer, PIG_SKIN_TEXTURE, PIG_SKIN_SIGNATURE);
            break;
        case SPACE:
            applySkinFromTexture(officer, SPACE_SKIN_TEXTURE, SPACE_SKIN_SIGNATURE);
            break;
        case RIOT:
            applySkinFromTexture(officer, RIOT_SKIN_TEXTURE, RIOT_SKIN_SIGNATURE);
            break;
        case RIOT_RIOT:
            applySkinFromTexture(officer, RIOT_RIOT_SKIN_TEXTURE, RIOT_RIOT_SKIN_SIGNATURE);
            break;
        case ANNA:
            applySkinFromTexture(officer, ANNA_SKIN_TEXTURE, ANNA_SKIN_SIGNATURE);
            break;
        case ANNA_PINK:
            applySkinFromTexture(officer, ANNA_PINK_SKIN_TEXTURE, ANNA_PINK_SKIN_SIGNATURE);
            break;
        case BEE:
            applySkinFromTexture(officer, BEE_SKIN_TEXTURE, BEE_SKIN_SIGNATURE);
            break;
        case CLASSIC:
            applySkinFromTexture(officer, CLASSIC_SKIN_TEXTURE, CLASSIC_SKIN_SIGNATURE);
            break;
        case CHRISTMAS:
            applySkinFromTexture(officer, CHRISTMAS_SKIN_TEXTURE, CHRISTMAS_SKIN_SIGNATURE);
            break;
        case RANDOM:
            // this shouldn't happen here, as random is replaced with one of the other options
            break;
        }
    }

    private void addAITasks(Human officer, Optional<UUID> targetUUID) {
        Agent agent = (Agent) officer;

        SwimmingAITask swimTask = SwimmingAITask.builder()
                .swimChance(0.9F)
                .build(agent);

        WanderAITask wanderTask = WanderAITask.builder()
                .speed(0.8D)
                .executionChance(5)
                .build((Creature) agent);

        WatchClosestAITask watchTask = WatchClosestAITask.builder()
                .watch(Player.class)
                .maxDistance(8.0F)
                .build((Creature) agent);

        LookIdleAITask idleTask = LookIdleAITask.builder()
                .build((Creature) agent);

        AttackLivingAITask attackTask = AttackLivingAITask.builder()
                .longMemory()
                .speed(1.0D)
                .build((Creature) agent);

        Optional<Goal<Agent>> normalGoals = agent.getGoal(GoalTypes.NORMAL);
        if (normalGoals.isPresent()) {
            normalGoals.get().addTask(0, swimTask)
                             .addTask(2, attackTask)
                             .addTask(7, wanderTask)
                             .addTask(8, watchTask)
                             .addTask(8, idleTask);
        }

        if (targetUUID.isPresent()) {
            FindNearestAttackableTargetAITask targetTask = FindNearestAttackableTargetAITask.builder()
                    .chance(1)
                    .target(Player.class)
                    .filter(living -> living.getUniqueId().equals(targetUUID.get()))
                    .build((Creature) agent);

            Optional<Goal<Agent>> targetGoal = agent.getGoal(GoalTypes.TARGET);
            targetGoal.ifPresent(agentGoal -> agentGoal.addTask(0, targetTask));
        }
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.RESET, "Police Officer");
    }

    @Override
    public Text getEntityName() {
        return Text.of("Police Officer");
    }

    public Text getEntityName(PoliceType type) {
        switch (type) {
        case STEVE:
        case SPACE:
        case CLASSIC:
        case CHRISTMAS:
            return Text.of("Officer Steve");
        case ALEX:
            return Text.of("Officer Alex");
        case ZOMBIE:
            return Text.of("Officer Zombie");
        case ZOMBIE_PIGMAN:
        case PIG:
            return Text.of("Officer Pig");
        case RIOT:
        case RIOT_RIOT:
            return Text.of("Officer Riot");
        case ANNA:
        case ANNA_PINK:
            return Text.of("Officer Anna");
        case BEE:
            return Text.of("Officer Bee");
        case RANDOM:
            // this shouldn't happen here, as random is replaced with one of the other options
            break;
        }
        return Text.of("Police Officer");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Spawns a police officer"));
    }

    @Override
    public boolean shouldDespawn() {
        return true;
    }

    @Override
    public boolean shouldPickUpLoot() {
        return false;
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        return super.makeEquipmentLoadout()
                .mainHand(makeSword(), NO_DROP_CHANCE)
                .offHand(makeWarrant(Optional.empty()), WARRANT_DROP_CHANCE);
    }

    protected EquipmentLoadout.Builder makeTargettedEquipmentLoadout(PoliceType type, Living target) {
        return super.makeEquipmentLoadout()
                .mainHand(makeSword(), FULL_DROP_CHANCE) // won't drop because of curse of vanishing
                // head item does not display, just is an item drop
                // Currently Disabled until a cooldown can be implemented
                //.chest(makeHead(type), HEAD_DROP_CHANCE)
                .offHand(makeWarrant(Optional.of(target)), WARRANT_DROP_CHANCE);
    }

    private ItemStack makeSword() {
        Builder sword = ItemStack.builder().itemType(ItemTypes.WOODEN_SWORD);
        List<Enchantment> enchants = new ArrayList<Enchantment>();
        enchants.add(Enchantment.of(EnchantmentTypes.SHARPNESS, 1));
        enchants.add(Enchantment.of(EnchantmentTypes.KNOCKBACK, 2));
        enchants.add(Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1));
        sword.add(Keys.ITEM_ENCHANTMENTS, enchants);
        return sword.build();
    }

    @SuppressWarnings("unused")
    private ItemStack makeRiotShield() {
        Builder shield = ItemStack.builder().itemType(ItemTypes.SHIELD);
        shield.add(Keys.ITEM_ENCHANTMENTS, Collections.singletonList(
                Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1)));
        List<PatternLayer> patterns = new ArrayList<PatternLayer>();
        patterns.add(PatternLayer.of(BannerPatternShapes.BASE, DyeColors.WHITE));
        patterns.add(PatternLayer.of(BannerPatternShapes.GRADIENT, DyeColors.LIGHT_BLUE));
        patterns.add(PatternLayer.of(BannerPatternShapes.BORDER, DyeColors.BLACK));
        patterns.add(PatternLayer.of(BannerPatternShapes.HALF_VERTICAL_MIRROR, DyeColors.BLACK));
        patterns.add(PatternLayer.of(BannerPatternShapes.STRIPE_MIDDLE, DyeColors.BLACK));
        // none of this banner key data stuff works for some reason, Sponge isn't doing this right for items
        shield.add(Keys.BANNER_BASE_COLOR, DyeColors.WHITE);
        shield.add(Keys.BANNER_PATTERNS, patterns);
        return shield.build();
    }

    private ItemStack makeWarrant(Optional<Living> target) {
        ItemStack warrant = ItemStack.builder()
                .itemType(ItemTypes.PAPER)
                .build();

        warrant.offer(Keys.DISPLAY_NAME, Text.of(TextColors.AQUA, "Warrant"));

        if (target.isPresent()) {
            Optional<Text> targetName = target.get().get(Keys.DISPLAY_NAME);
            if (targetName.isPresent()) {
                warrant.offer(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "This is a warrant for the arrest of ", targetName.get(), TextColors.GRAY, ".")));
                // add owner uuid to item nbt
                DataContainer itemInfo = warrant.toContainer();
                itemInfo.set(ITEM_OWNER_PATH, target.get().getUniqueId().toString());
                warrant.setRawData(itemInfo);
            }
        }
        return warrant;
    }

    private ItemStack makeHead(PoliceType type) {
        String texture;
        String signature;
        String uuid;
        switch (type) {
        case STEVE:
            texture = STEVE_SKIN_TEXTURE;
            signature = STEVE_SKIN_SIGNATURE;
            uuid = STEVE_UUID;
            break;
        case ALEX:
            texture = ALEX_SKIN_TEXTURE;
            signature = ALEX_SKIN_SIGNATURE;
            uuid = ALEX_UUID;
            break;
        case ZOMBIE:
            texture = ZOMBIE_SKIN_TEXTURE;
            signature = ZOMBIE_SKIN_SIGNATURE;
            uuid = ZOMBIE_UUID;
            break;
        case ZOMBIE_PIGMAN:
            texture = PIGZOMBIE_SKIN_TEXTURE;
            signature = PIGZOMBIE_SKIN_SIGNATURE;
            uuid = PIGZOMBIE_UUID;
            break;
        case PIG:
            texture = PIG_SKIN_TEXTURE;
            signature = PIG_SKIN_SIGNATURE;
            uuid = PIG_UUID;
            break;
        case SPACE:
            texture = SPACE_SKIN_TEXTURE;
            signature = SPACE_SKIN_SIGNATURE;
            uuid = SPACE_UUID;
            break;
        case RIOT:
            texture = RIOT_SKIN_TEXTURE;
            signature = RIOT_SKIN_SIGNATURE;
            uuid = RIOT_UUID;
            break;
        case RIOT_RIOT:
            texture = RIOT_RIOT_SKIN_TEXTURE;
            signature = RIOT_RIOT_SKIN_SIGNATURE;
            uuid = RIOT_RIOT_UUID;
            break;
        case ANNA:
            texture = ANNA_SKIN_TEXTURE;
            signature = ANNA_SKIN_SIGNATURE;
            uuid = ANNA_UUID;
            break;
        case ANNA_PINK:
            texture = ANNA_PINK_SKIN_TEXTURE;
            signature = ANNA_PINK_SKIN_SIGNATURE;
            uuid = ANNA_PINK_UUID;
            break;
        case BEE:
            texture = BEE_SKIN_TEXTURE;
            signature = BEE_SKIN_SIGNATURE;
            uuid = BEE_UUID;
            break;
        case CLASSIC:
            texture = CLASSIC_SKIN_TEXTURE;
            signature = CLASSIC_SKIN_SIGNATURE;
            uuid = CLASSIC_UUID;
            break;
        case CHRISTMAS:
            texture = CHRISTMAS_SKIN_TEXTURE;
            signature = CHRISTMAS_SKIN_SIGNATURE;
            uuid = CHRISTMAS_UUID;
            break;
        default:
            return ItemStack.empty();
        }

        return PlayerSkinHelper.makeHead(texture, signature, uuid, getEntityName(type).toPlain());
    }
}
