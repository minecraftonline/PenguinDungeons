package com.minecraftonline.penguindungeons.customentity.human;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataTransactionResult.Type;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.SkullTypes;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

public class PlayerSkinHelper {

    private static final DataQuery SKULL_INFO_PATH = DataQuery.of("UnsafeData", "SkullOwner");
    private static final DataQuery TEXTURES_PATH = DataQuery.of("Properties", "textures");
    private static final DataQuery ID_QUERY = DataQuery.of("Id");
    private static final DataQuery NAME_QUERY = DataQuery.of("Name");
    private static final DataQuery VALUE_QUERY = DataQuery.of("Value");
    private static final DataQuery SIGNATURE_QUERY = DataQuery.of("Signature");

    public static ItemStack makeHead(String texture, String signature, String uuid, String name) {

        ItemStack head = ItemStack.builder()
                .itemType(ItemTypes.SKULL)
                .add(Keys.SKULL_TYPE, SkullTypes.PLAYER)
                .build();

        DataView itemView = head.toContainer();

        DataView skullInfo = itemView.getView(SKULL_INFO_PATH).orElse(DataContainer.createNew());
        List<DataView> textures = skullInfo.getViewList(TEXTURES_PATH).orElse(new ArrayList<DataView>());

        skullInfo.set(ID_QUERY, uuid);
        skullInfo.set(NAME_QUERY, name);

        DataView textureProperty = DataContainer.createNew();
        textureProperty.set(VALUE_QUERY, texture);
        textureProperty.set(SIGNATURE_QUERY, signature);
        textures.add(textureProperty);
        skullInfo.set(TEXTURES_PATH, textures);

        itemView.set(SKULL_INFO_PATH, skullInfo);

        head.setRawData(itemView);
        return head;
    }

    public static boolean setSkinFromData(DataView data, Human human) {
        Optional<DataView> skullInfo = data.getView(SKULL_INFO_PATH);
        if (!skullInfo.isPresent()) return false;
        Optional<List<DataView>> textures = skullInfo.get().getViewList(TEXTURES_PATH);
        if (!textures.isPresent() || textures.get().size() < 1) return false;

        DataView skinData = textures.get().get(0);
        Optional<String> texture = skinData.getString(VALUE_QUERY);
        Optional<String> signature = skinData.getString(SIGNATURE_QUERY);
        if (!texture.isPresent() || !signature.isPresent()) return false;

        boolean result = (human.offer(Keys.SKIN_SIGNATURE, signature.get()).getType() == Type.SUCCESS)
                      && (human.offer(Keys.SKIN_TEXTURE,   texture.get()).getType()   == Type.SUCCESS);

        if (result) {
            // also update entity name if available
            Optional<String> name = skullInfo.get().getString(NAME_QUERY);
            if (name.isPresent()) {
                human.offer(Keys.DISPLAY_NAME, Text.of(name.get()));
            }
        }
        return result;
    }
}
