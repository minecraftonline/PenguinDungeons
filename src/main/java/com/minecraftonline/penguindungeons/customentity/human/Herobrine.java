package com.minecraftonline.penguindungeons.customentity.human;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.builtin.SwimmingAITask;
import org.spongepowered.api.entity.ai.task.builtin.WatchClosestAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.AttackPlayerIfReachable;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.HasEffects;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.mixin.EntityMixinAccessor;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.potion.PotionEffect;

public class Herobrine extends HumanType {

    protected double BOSS_HEALTH = 10.0F;

    protected static final String SKIN_TEXTURE   = "ewogICJ0aW1lc3RhbXAiIDogMTcxMTkzMzYzOTM5MywKICAicHJvZmlsZUlkIiA6ICIxM2EzMzc3YmEwOTk0MmFjODNiMGY3NDY1NWYxNjBiMyIsCiAgInByb2ZpbGVOYW1lIiA6ICJFeGNhbGlidXJfTlVNTlVNIiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzZiZmI3ZGY5Y2RmNmFiOGYzNWMwNDQ0OTE1MGI3N2U2YmFjYjU5N2NlZGE4MDY0NzNkYjQwN2ZmNjNhODg4NGYiCiAgICB9CiAgfQp9";
    protected static final String SKIN_SIGNATURE = "aeAQxQSYqWoaOlPPTGz+8Bk9amqmKHZ3UUqVFdML9aC12Xee7oaaZa7eCmfsyss+fn8Hy2vep7cvDPdLhOxUbeFilhYWjBCKQLUKR5M8Zz5LKiz7zoSd6L+K6F+X1b+h/UtW0cRBBosNEXyiJpJbpnwAdP9C+9qV2LoVkqE5C6guBN4PZmvbNWW9A8J6M+D6ih9SFafDgXyGYON8znteo8m6TKMCn7fMksVuu5F/l1Z+2Sv8RrXLI+EgfvjsiUz/hWUgt7BJTrZlmFL63mXCUoFd5+2f9cfgymcYL4zS42VsFYqBNjjbnH1YaOBjHGzra6k5IigDSLcOYc2Qi7rrI6wOY9YIiGAX+hhKGWZA6+pmJ06siMYaqY22LCM+VKxvU5VluMPiudzTWD2ctjW77trkY7A4xiC305CWZ6DuUC8Rgqsdx3ko1TM9lndI64eFdYz/px8Hu4tEwlKzynE2X3tnJFfqgYmj8GUSKRq/eCooSUgALq9SxX8DUmK3KBEvCOuxzMuToI0oDOjry9ANoZAqb5uMvBTQj3Vt3EWDqzEl3W56u7t2Rj9F5ouhCb0Fs9jlW0qFi14+O9PXK3bVm5KfJDDb1BzM6B1uNvNiDJYlncNaWfRk/yEOFyxdrcOPKjFVehzzk3qHWJx5MQ2FhRJMLw88lSb3WqZDIm4Epjg=";

    protected static final String UUID = "778ec64a-d299-5b07-895f-41e7b7a3a9d8"; // herobrine

    public Herobrine(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ZOMBIE;
    }

    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Human)) {
            throw new IllegalArgumentException("Expected a human to be given to Herobrine to load, but got: " + entity);
        }
        Human human = (Human) entity;
        addAITasks(human);
        ((EntityMixinAccessor) human).setIsImmuneToFire(true);
    }

    @Override
    protected Human makeHuman(World world, Vector3d blockPos) {
        Human human = super.makeHuman(world, blockPos);

        human.offer(Keys.MAX_HEALTH, BOSS_HEALTH);
        human.offer(Keys.HEALTH, BOSS_HEALTH);

        applySkinFromTexture(human, SKIN_TEXTURE, SKIN_SIGNATURE);

        return human;
    }

    protected void addAITasks(Human human) {
        Agent agent = (Agent) human;

        SwimmingAITask swimTask = SwimmingAITask.builder()
                .swimChance(0.9F)
                .build(agent);

        WatchClosestAITask watchTask = WatchClosestAITask.builder()
                .watch(Player.class)
                .maxDistance(30.0F)
                .build((Creature) agent);

        AITask<?> rangedAttackTask = new StraightProjectileAttack.RangedAttack(human, Optional.empty(), Optional.of(SoundEvents.ENTITY_BLAZE_SHOOT),
                AIUtil.ProjectileCreator.forSmallFireball(
                        AIUtil.ProjectileForImpact.forMultipleImpacts(
                            AIUtil.ProjectileForImpact.forImpactDamage(0.5F),
                            AIUtil.ProjectileForImpact.forImpactSetEffects(new HasEffects()
                            { public List<PotionEffect> getEffects() { return Collections.singletonList(new PotionEffect(MobEffects.BLINDNESS, TICKS_PER_SECOND*2)); }}, false)
                        )
                ), 0.7D, 25, 80, 30.0F);

        AITask<?> attackTask = new AttackPlayerIfReachable.AttackPlayerIfReachableTask(human, 3.0D);

        Optional<Goal<Agent>> normalGoals = agent.getGoal(GoalTypes.NORMAL);
        if (normalGoals.isPresent()) {
            normalGoals.get().addTask(0, swimTask)
                             .addTask(2, attackTask)
                             .addTask(3, rangedAttackTask)
                             .addTask(8, watchTask);
        }

        Optional<Goal<Agent>> targetGoals = agent.getGoal(GoalTypes.TARGET);
        if (targetGoals.isPresent()) {
            targetGoals.get().addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers(human, true, true))
                             .addTask(2, (AITask<?>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) human, EntityPlayer.class, true));
        }
    }

    @Override
    public Text getDisplayName() {
        return Text.of("Herobrine");
    }

    @Override
    public Text getEntityName() {
        return getDisplayName();
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Herobrine which shoots fireballs that give blindness"));
    }

    @Override
    public boolean shouldDespawn() {
        return true;
    }

    @Override
    public boolean shouldPickUpLoot() {
        return false;
    }
}
