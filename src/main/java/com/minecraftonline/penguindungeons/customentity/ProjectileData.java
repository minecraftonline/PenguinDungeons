package com.minecraftonline.penguindungeons.customentity;

import java.util.Arrays;
import java.util.List;

import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumParticleTypes;

public class ProjectileData implements HasProjectileData {
    private final List<PotionEffect> effects;
    private final EnumParticleTypes particle;
    private final int particleParam1;
    private final int particleParam2;

    public ProjectileData(PotionEffect potionEffect, EnumParticleTypes particle, int param1, int param2)
    {
        this(Arrays.asList(potionEffect), particle, param1, param2);
    }

    public ProjectileData(List<PotionEffect> effects, EnumParticleTypes particle, int param1, int param2)
    {
        this.effects = effects;
        this.particle = particle;
        this.particleParam1 = param1;
        this.particleParam2 = param2;
    }

    @Override
    public List<PotionEffect> getEffects() {
        return effects;
    }

    @Override
    public EnumParticleTypes getParticle() {
        return particle;
    }

    @Override
    public int getParticleParam1() {
        return particleParam1;
    }

    @Override
    public int getParticleParam2() {
        return particleParam2;
    }
}
