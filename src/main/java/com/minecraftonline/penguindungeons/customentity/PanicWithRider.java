package com.minecraftonline.penguindungeons.customentity;

import org.spongepowered.api.entity.living.Creature;

import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.util.math.Vec3d;

public class PanicWithRider extends EntityAIBase {
    protected final EntityCreature creature;
    protected double speed;
    protected double randPosX;
    protected double randPosY;
    protected double randPosZ;

    public PanicWithRider(EntityCreature creature, double speedIn) {
        this.creature = creature;
        this.speed = speedIn;
        this.setMutexBits(1);
    }

    public boolean shouldExecute() {
        return this.creature.getControllingPassenger() != null && this.findRandomPosition();
    }

    protected boolean findRandomPosition() {
        Vec3d vec3d = RandomPositionGenerator.findRandomTarget(this.creature, 5, 4);
        if (vec3d == null) {
            return false;
        } else {
            this.randPosX = vec3d.x;
            this.randPosY = vec3d.y;
            this.randPosZ = vec3d.z;
            return true;
        }
    }

    public void startExecuting() {
        this.creature.getNavigator().tryMoveToXYZ(this.randPosX, this.randPosY, this.randPosZ, this.speed);
    }

    public boolean shouldContinueExecuting() {
        return this.creature.getControllingPassenger() != null && !this.creature.getNavigator().noPath();
    }

    public void resetTask() {
        this.creature.getNavigator().clearPath();
    }

    public static class PanicWithPlayerRiderTask extends DelegatingToMCAI<Creature> {
        public PanicWithPlayerRiderTask(Creature creature, double speedIn) {
            super(PenguinDungeonAITaskTypes.PANIC_WITH_PLAYER_RIDER, new PanicWithRider((EntityCreature) creature, speedIn));
        }
    }
}
