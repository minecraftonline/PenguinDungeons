package com.minecraftonline.penguindungeons.customentity;

import java.util.Optional;

import org.spongepowered.api.entity.living.Creature;

import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.MathHelper;

public class JumpToTarget extends EntityAIBase {
    private EntityLiving leaper;
    private EntityLivingBase leapTarget;
    private float leapMotionY;
    private final double minDist;
    private final double maxDist;
    private final float chance;
    private final Optional<SoundEvent> sound;

    public JumpToTarget(EntityLiving leapingEntity, float leapMotionYIn) {
        this(leapingEntity, leapMotionYIn, 2.0D, 4.0D, 0.25f);
    }

    public JumpToTarget(EntityLiving leapingEntity, float leapMotionYIn, double minDistance, double maxDistance, float chance) {
        this(leapingEntity, leapMotionYIn, minDistance, maxDistance, chance, Optional.empty());
    }

    public JumpToTarget(EntityLiving leapingEntity, float leapMotionYIn, double minDistance, double maxDistance, float chance, Optional<SoundEvent> sound) {
        this.leaper = leapingEntity;
        this.leapMotionY = leapMotionYIn;
        this.setMutexBits(5);
        // use squared distances
        this.minDist = minDistance * minDistance;
        this.maxDist = maxDistance * maxDistance;
        this.chance = chance;
        this.sound = sound;
    }

    public boolean shouldExecute() {
        this.leapTarget = this.leaper.getAttackTarget();
        if (this.leapTarget == null) {
            return false;
        } else {
            double d0 = this.leaper.getDistanceSq(this.leapTarget);
            if (d0 >= this.minDist && d0 <= this.maxDist) {
                if (!this.leaper.onGround) {
                    return false;
                } else {
                    return this.leaper.getRNG().nextFloat() < this.chance;
                }
            } else {
                return false;
            }
        }
    }

    public boolean shouldContinueExecuting() {
        return !this.leaper.onGround && this.leapTarget.isEntityAlive();
    }

    public void startExecuting() {
        if (this.sound.isPresent() && this.leaper.onGround)
        {
            this.leaper.playSound(this.sound.get(), 1.0F, ((this.leaper.getRNG().nextFloat() - this.leaper.getRNG().nextFloat()) * 0.2F + 1.0F) * 0.8F);
        }
        double d0 = this.leapTarget.posX - this.leaper.posX;
        double d1 = this.leapTarget.posZ - this.leaper.posZ;
        float f = MathHelper.sqrt(d0 * d0 + d1 * d1);
        if ((double)f >= 1.0E-4D) {
            this.leaper.motionX += d0 / (double)f * 2.0D;
            this.leaper.motionZ += d1 / (double)f * 2.0D;
        }

        this.leaper.motionY = (double)this.leapMotionY;
    }

    public static class JumpAtTarget extends DelegatingToMCAI<Creature> {

        public JumpAtTarget(Creature creature, float leapMotionYIn, double minDistance, double maxDistance, float chance) {
            super(PenguinDungeonAITaskTypes.JUMP_AT_TARGET, new JumpToTarget((EntityLiving) creature, leapMotionYIn, minDistance, maxDistance, chance));
        }

        public JumpAtTarget(Creature creature, float leapMotionYIn, double minDistance, double maxDistance, float chance, SoundEvent sound) {
            super(PenguinDungeonAITaskTypes.JUMP_AT_TARGET, new JumpToTarget((EntityLiving) creature, leapMotionYIn, minDistance, maxDistance, chance, Optional.of(sound)));
        }
    }

}
