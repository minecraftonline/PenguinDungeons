package com.minecraftonline.penguindungeons.customentity;

import com.google.common.collect.ImmutableSet;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.bee.AngryBee;
import com.minecraftonline.penguindungeons.customentity.bee.AngryBombee;
import com.minecraftonline.penguindungeons.customentity.bee.AngryEnbee;
import com.minecraftonline.penguindungeons.customentity.bee.AngryMaybee;
import com.minecraftonline.penguindungeons.customentity.bee.ZombieBee;
import com.minecraftonline.penguindungeons.customentity.bee.MinionBee;
import com.minecraftonline.penguindungeons.customentity.dragon.AstralDragon;
import com.minecraftonline.penguindungeons.customentity.dragon.AstralMonarchFlippehDragon;
import com.minecraftonline.penguindungeons.customentity.dragon.EndDragon;
import com.minecraftonline.penguindungeons.customentity.dragon.RobotDragon;
import com.minecraftonline.penguindungeons.customentity.dragon.TemporalDragon;
import com.minecraftonline.penguindungeons.customentity.dragon.TerrainDragon;
import com.minecraftonline.penguindungeons.customentity.golem.EggGolem;
import com.minecraftonline.penguindungeons.customentity.golem.ExplodingSnowGolem;
import com.minecraftonline.penguindungeons.customentity.golem.FrostGolem;
import com.minecraftonline.penguindungeons.customentity.golem.IceSnowGolem;
import com.minecraftonline.penguindungeons.customentity.golem.SnowGolemType;
import com.minecraftonline.penguindungeons.customentity.golem.SnowflakeGolem;
import com.minecraftonline.penguindungeons.customentity.human.StandingHuman;
import com.minecraftonline.penguindungeons.customentity.human.AggressiveHuman;
import com.minecraftonline.penguindungeons.customentity.human.AstralMonarchFlippehHuman;
import com.minecraftonline.penguindungeons.customentity.human.DefensiveHuman;
import com.minecraftonline.penguindungeons.customentity.human.GuardingHuman;
import com.minecraftonline.penguindungeons.customentity.human.Herobrine;
import com.minecraftonline.penguindungeons.customentity.human.HerobrineBoss;
import com.minecraftonline.penguindungeons.customentity.human.PigchinkoPlayer;
import com.minecraftonline.penguindungeons.customentity.human.ProtectiveHuman;
import com.minecraftonline.penguindungeons.customentity.human.StevePolice;
import com.minecraftonline.penguindungeons.customentity.human.WalkingHuman;
import com.minecraftonline.penguindungeons.customentity.other.AngryEnderman;
import com.minecraftonline.penguindungeons.customentity.other.Butterfly;
import com.minecraftonline.penguindungeons.customentity.other.CakeSlime;
import com.minecraftonline.penguindungeons.customentity.other.ChaosGhast;
import com.minecraftonline.penguindungeons.customentity.other.HoneySlime;
import com.minecraftonline.penguindungeons.customentity.other.Kea;
import com.minecraftonline.penguindungeons.customentity.other.LaserBear;
import com.minecraftonline.penguindungeons.customentity.other.EasterBunny;
import com.minecraftonline.penguindungeons.customentity.other.FlyingCupid;
import com.minecraftonline.penguindungeons.customentity.other.LoveSilverfish;
import com.minecraftonline.penguindungeons.customentity.other.LoveVex;
import com.minecraftonline.penguindungeons.customentity.other.MinionHoneySlime;
import com.minecraftonline.penguindungeons.customentity.other.PigchinkoPig;
import com.minecraftonline.penguindungeons.customentity.other.PrideSheep;
import com.minecraftonline.penguindungeons.customentity.other.WanderingCupid;
import com.minecraftonline.penguindungeons.customentity.other.QueenBee;
import com.minecraftonline.penguindungeons.customentity.shulker.ChaosShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.ShulkerType;
import com.minecraftonline.penguindungeons.customentity.shulker.potion.ClearEffectShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.potion.GlowingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.potion.PotionEffectShulkers;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.DragonShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.ExperienceShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.ExplodingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.FieryShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.HealthyShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.InvisibleShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.NormalShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.PaladinShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.RainbowShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.RegeneratingShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.SniperShulker;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.SpikeyShulker;
import com.minecraftonline.penguindungeons.customentity.wither.PumpkinWither;
import com.minecraftonline.penguindungeons.customentity.zombie.FireworkZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.GiantZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.HorseZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.LaserZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.LoveZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.NinjaZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.NormalZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.AngryZombiePigman;
import com.minecraftonline.penguindungeons.customentity.zombie.PoliceZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.PriestZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.PumpkinZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.ShieldZombie;
import com.minecraftonline.penguindungeons.customentity.zombie.ZombieDog;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.value.ValueContainer;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@SuppressWarnings("unused")
public class CustomEntityTypes {

    private static Map<ResourceKey, CustomEntityType> REGISTRY = null;

    // Java code entities
    public static final ShulkerType NORMAL_SHULKER              = new NormalShulker(ResourceKey.pd("shulker_normal"));
    public static final ShulkerType HEALTHY_SHULKER             = new HealthyShulker(ResourceKey.pd("shulker_healthy"));
    public static final ShulkerType GLOWING_SHULKER             = new GlowingShulker(ResourceKey.pd("shulker_glowing"));
    public static final ShulkerType INVISIBLE_SHULKER           = new InvisibleShulker(ResourceKey.pd("shulker_invisible"));
    public static final ShulkerType REGENERATING_SHULKER        = new RegeneratingShulker(ResourceKey.pd("shulker_regenerating"));
    public static final ShulkerType SNIPER_SHULKER              = new SniperShulker(ResourceKey.pd("shulker_sniper"));
    public static final ShulkerType EXPLODING_SHULKER           = new ExplodingShulker(ResourceKey.pd("shulker_exploding"));
    public static final ShulkerType FIERY_SHULKER               = new FieryShulker(ResourceKey.pd("shulker_fiery"));
    public static final ShulkerType SPIKEY_SHULKER              = new SpikeyShulker(ResourceKey.pd("shulker_spikey"));
    public static final ShulkerType POISON_SHULKER              = PotionEffectShulkers.poison(ResourceKey.pd("shulker_poison"));
    public static final ShulkerType JUMP_BOOST_SHULKER          = PotionEffectShulkers.jumpBoost(ResourceKey.pd("shulker_jump_boost"));
    public static final ShulkerType MINING_FATIGUE_SHULKER      = PotionEffectShulkers.miningFatigue(ResourceKey.pd("shulker_mining_fatigue"));
    public static final ShulkerType SLOWNESS_SHULKER            = PotionEffectShulkers.slowness(ResourceKey.pd("shulker_slowness"));
    public static final ShulkerType SPEED_SHULKER               = PotionEffectShulkers.speed(ResourceKey.pd("shulker_speed"));
    public static final ShulkerType WITHER_SHULKER              = PotionEffectShulkers.wither(ResourceKey.pd("shulker_wither"));
    public static final ShulkerType BLIDNING_SHULKER            = PotionEffectShulkers.blindness(ResourceKey.pd("shulker_blinding"));
    public static final ShulkerType CLEAR_SHULKER               = new ClearEffectShulker(ResourceKey.pd("shulker_clear"));
    public static final ShulkerType EXPERIENCE_SHULKER          = new ExperienceShulker(ResourceKey.pd("shulker_experience"));
    public static final ShulkerType PALADIN_SHULKER             = new PaladinShulker(ResourceKey.pd("shulker_paladin"));
    public static final ShulkerType DRAGON_SHULKER              = new DragonShulker(ResourceKey.pd("shulker_dragon"));
    public static final ShulkerType RAINBOW_SHULKER             = new RainbowShulker(ResourceKey.pd("shulker_rainbow"));
    public static final ShulkerType CHAOS_SHULKER               = new ChaosShulker(ResourceKey.pd("shulker_chaos"));
    public static final SnowGolemType SNOWFLAKE_GOLEM           = new SnowflakeGolem(ResourceKey.pd("snow_golem_snowflake"));
    public static final SnowGolemType EXPLODING_SNOW_GOLEM      = new ExplodingSnowGolem(ResourceKey.pd("snow_golem_explode"));
    public static final SnowGolemType ICE_SNOW_GOLEM            = new IceSnowGolem(ResourceKey.pd("snow_golem_ice"));
    public static final CustomPolarBear POLAR_BEAR              = new CustomPolarBear(ResourceKey.pd("polar_bear_custom"));
    public static final TerrainDragon TERRAIN_DRAGON            = new TerrainDragon(ResourceKey.pd("terrain_dragon"));
    public static final RobotDragon ROBOT_DRAGON                = new RobotDragon(ResourceKey.pd("robot_dragon"));
    public static final NinjaZombie NINJA_ZOMBIE                = new NinjaZombie(ResourceKey.pd("ninja_zombie"));
    public static final ZombieDog ZOMBIE_DOG                    = new ZombieDog(ResourceKey.pd("zombie_dog"));
    public static final HorseZombie HORSE_ZOMBIE                = new HorseZombie(ResourceKey.pd("horse_zombie"));
    public static final LaserZombie LASER_ZOMBIE                = new LaserZombie(ResourceKey.pd("laser_zombie"));
    public static final GiantZombie GIANT_ZOMBIE                = new GiantZombie(ResourceKey.pd("giant_zombie"));
    public static final ShieldZombie SHIELD_ZOMBIE              = new ShieldZombie(ResourceKey.pd("shield_zombie"));
    public static final PumpkinZombie PUMPKIN_ZOMBIE            = new PumpkinZombie(ResourceKey.pd("pumpkin_zombie"));
    public static final PumpkinWither PUMPKIN_WITHER            = new PumpkinWither(ResourceKey.pd("pumpkin_wither"));
    public static final AngryBee ANGRY_BEE                      = new AngryBee(ResourceKey.pd("angry_bee"));
    public static final AngryEnbee ANGRY_ENBEE                  = new AngryEnbee(ResourceKey.pd("angry_enbee"));
    public static final ZombieBee ZOMBIE_BEE                    = new ZombieBee(ResourceKey.pd("zombie_bee"));
    public static final PoliceZombie POLICE_ZOMBIE              = new PoliceZombie(ResourceKey.pd("police_zombie"));
    public static final NormalZombie NORMAL_ZOMBIE              = new NormalZombie(ResourceKey.pd("normal_zombie"));
    public static final PriestZombie PRIEST_ZOMBIE              = new PriestZombie(ResourceKey.pd("priest_zombie"));
    public static final FireworkZombie FIREWORK_ZOMBIE          = new FireworkZombie(ResourceKey.pd("firework_zombie"));
    public static final AngryZombiePigman ANGRY_ZOMBIE_PIGMAN   = new AngryZombiePigman(ResourceKey.pd("angry_zombie_pigman"));
    public static final FrostGolem FROST_GOLEM                  = new FrostGolem(ResourceKey.pd("frost_golem"));
    public static final FlyingCupid FLYING_CUPID                = new FlyingCupid(ResourceKey.pd("cupid"));
    public static final WanderingCupid WANDERING_CUPID          = new WanderingCupid(ResourceKey.pd("lost_cupid"));
    public static final LoveSilverfish LOVE_SILVERFISH          = new LoveSilverfish(ResourceKey.pd("love_silverfish"));
    public static final LoveZombie LOVE_ZOMBIE                  = new LoveZombie(ResourceKey.pd("love_zombie"));
    public static final LoveVex LOVE_VEX                        = new LoveVex(ResourceKey.pd("love_vex"));
    public static final EggGolem EGG_GOLEM                      = new EggGolem(ResourceKey.pd("egg_golem"));
    public static final EasterBunny EASTER_BUNNY                = new EasterBunny(ResourceKey.pd("easter_rabbit"));
    public static final CakeSlime CAKE_SLIME                    = new CakeSlime(ResourceKey.pd("cake_slime"));
    public static final HoneySlime HONEY_SLIME                  = new HoneySlime(ResourceKey.pd("honey_slime"));
    public static final AngryMaybee ANGRY_MAYBEE                = new AngryMaybee(ResourceKey.pd("angry_maybee"));
    public static final AngryBombee ANGRY_BOMBEE                = new AngryBombee(ResourceKey.pd("angry_bombee"));
    public static final PrideSheep PRIDE_SHEEP                  = new PrideSheep(ResourceKey.pd("pride_sheep"));
    public static final PigchinkoPig PIGCHINKO_PIG              = new PigchinkoPig(ResourceKey.pd("pigchinko_pig"));
    public static final PigchinkoPlayer PIGCHINKO_PLAYER        = new PigchinkoPlayer(ResourceKey.pd("pigchinko_player"));
    public static final StevePolice STEVE_POLICE                = new StevePolice(ResourceKey.pd("steve_police"));
    public static final StandingHuman STANDING_HUMAN            = new StandingHuman(ResourceKey.pd("standing_human"));
    public static final WalkingHuman WALKING_HUMAN              = new WalkingHuman(ResourceKey.pd("walking_human"));
    public static final AggressiveHuman AGGRESSIVE_HUMAN        = new AggressiveHuman(ResourceKey.pd("aggressive_human"));
    public static final DefensiveHuman DEFENSIVE_HUMAN          = new DefensiveHuman(ResourceKey.pd("defensive_human"));
    public static final ProtectiveHuman PROTECTIVE_HUMAN        = new ProtectiveHuman(ResourceKey.pd("protective_human"));
    public static final GuardingHuman GUARDING_HUMAN            = new GuardingHuman(ResourceKey.pd("guarding_human"));
    public static final AstralDragon ASTRAL_DRAGON              = new AstralDragon(ResourceKey.pd("astral_dragon"));
    public static final Butterfly BUTTERFLY                     = new Butterfly(ResourceKey.pd("butterfly"));
    public static final ChaosGhast CHAOS_GHAST                  = new ChaosGhast(ResourceKey.pd("chaos_ghast"));
    public static final Herobrine HEROBRINE                     = new Herobrine(ResourceKey.pd("herobrine"));
    public static final HerobrineBoss HEROBRINE_BOSS            = new HerobrineBoss(ResourceKey.pd("herobrine_boss"));
    public static final QueenBee QUEEN_BEE                      = new QueenBee(ResourceKey.pd("queen_bee"));
    public static final MinionBee MINION_BEE                    = new MinionBee(ResourceKey.pd("minion_bee"));
    public static final MinionHoneySlime MINION_HONEY_SLIME     = new MinionHoneySlime(ResourceKey.pd("minion_honey_slime"));
    public static final LaserBear LASER_BEAR                    = new LaserBear(ResourceKey.pd("laser_bear"));
    public static final Kea KEA                                 = new Kea(ResourceKey.pd("kea"));
    public static final AngryEnderman ANGRY_ENDERMAN            = new AngryEnderman(ResourceKey.pd("angry_enderman"));
    public static final EndDragon END_DRAGON                    = new EndDragon(ResourceKey.pd("end_dragon"));
    public static final TemporalDragon TEMPORAL_DRAGON          = new TemporalDragon(ResourceKey.pd("temporal_dragon"));

    public static final AstralMonarchFlippehDragon ASTRAL_MONARCH_FLIPPEH_DRAGON = new AstralMonarchFlippehDragon(ResourceKey.pd("astral_monarch_flippeh_dragon"));
    public static final AstralMonarchFlippehHuman  ASTRAL_MONARCH_FLIPPEH_HUMAN  = new AstralMonarchFlippehHuman(ResourceKey.pd("astral_monarch_flippeh_human"));

    // NBT entities
    public static final StoredNBTEntityType TRADING_CUPID  = new StoredNBTEntityType(ResourceKey.pd("valentines_villager"), "valentines_villager.json");

    public static void initRegistry() {
        if (REGISTRY != null) {
            throw new IllegalStateException("Registry is already initialised!");
        }
        REGISTRY = new HashMap<>();
        for (Field field : CustomEntityTypes.class.getFields()) {
            if (!CustomEntityType.class.isAssignableFrom(field.getType())) {
                return;
            }
            try {
                CustomEntityType entityType = (CustomEntityType) field.get(null);
                if (entityType instanceof StoredNBTEntityType && PenguinDungeons.getInstance() != null) {
                    try {
                        ((StoredNBTEntityType)entityType).load(PenguinDungeons.getInstance().getContainer());
                    } catch (IOException e) {
                        PenguinDungeons.getLogger().error("Error while loading stored NBT entity type: '" + entityType.getId() + "'.", e);
                    }
                }
                REGISTRY.put(entityType.getId(), entityType);
            } catch (IllegalAccessException e) {
                PenguinDungeons.getLogger().error("Error initialising registry", e);
            }
        }
    }

    public static Optional<CustomEntityType> getById(final ResourceKey key) {
        return Optional.of(REGISTRY.get(key));
    }

    public static Optional<CustomEntityType> get(ValueContainer<?> valueContainer) {
        return valueContainer.get(PenguinDungeonKeys.PD_ENTITY_TYPE).flatMap(CustomEntityTypes::getById);
    }

    public static Collection<CustomEntityType> getAll() {
        return ImmutableSet.copyOf(REGISTRY.values());
    }
}
