package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.world.World;

public class MinionHoneySlime extends HoneySlime {

    public MinionHoneySlime(ResourceKey key) {
        super(key);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A blob of honey created by the Queen bee"));
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        // Only small and medium slimes can be created
        return Spawnable.of(makeSlime(world, pos, new Random().nextInt(2)), this::onLoad);
    }
}
