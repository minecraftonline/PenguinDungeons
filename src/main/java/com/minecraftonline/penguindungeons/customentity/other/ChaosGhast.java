package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.EnderCrystal;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.Ghast;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.ProjectileData;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.item.EntityEnderCrystal;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumParticleTypes;

public class ChaosGhast extends AbstractCustomEntity {

    private static final double NEW_HEALTH = 80.0;

    public ChaosGhast(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.GHAST;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.LIGHT_PURPLE, "Chaos Ghast");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Ghast with Ender Crystal"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Ghast)) {
            throw new IllegalArgumentException("Expected a Ghast to be given to ChaosGhast to load, but got: " + entity);
        }
        Ghast ghast = (Ghast) entity;
        EntityGhast mcGhast = (EntityGhast) ghast;

        mcGhast.getEntityAttribute(SharedMonsterAttributes.ARMOR).applyModifier(new AttributeModifier("Chaos Armor", 10, 0));
        mcGhast.getEntityAttribute(SharedMonsterAttributes.ARMOR_TOUGHNESS).applyModifier(new AttributeModifier("Chaos Armor", 6, 0));

        Goal<Agent> normalGoals = ghast.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();
        // can't easily remove normal fireball task without also clearing the flying and looking tasks
        // because they are all private classes inside EntityGhast, so they are repeated in GhastAITasks.
        normalGoals.addTask(5, (AITask<? extends Agent>) new GhastAITasks.AIRandomFly(mcGhast));
        normalGoals.addTask(7, (AITask<? extends Agent>) new GhastAITasks.AILookAround(mcGhast));

        // so just add this as a higher priority
        normalGoals.addTask(7, new StraightProjectileAttack.RangedAttack(ghast,
                Optional.of(Text.of(TextColors.DARK_PURPLE, "Magic Skull")), Optional.empty(),
                AIUtil.ProjectileCreator.forSkull(AIUtil.ProjectileForImpact.forImpactWithSound(
                        AIUtil.ProjectileForImpact.forImpactAreaOfEffect(0.5F, TICKS_PER_SECOND*3,
                                new ProjectileData((net.minecraft.potion.PotionEffect)
                                        PotionEffect.of(PotionEffectTypes.INSTANT_DAMAGE, 0, 1),
                                        EnumParticleTypes.SPELL_WITCH, 0, 0)),
                        SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE)), 1.0D, 40, 120, 100F));
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Ghast ghast = (Ghast) world.createEntity(getType(), blockPos);

        ghast.offer(new PDEntityTypeData(getId()));
        ghast.offer(new OwnerLootData(ownerLoot()));
        ghast.offer(new PDSpawnData(true));
        ghast.offer(Keys.DISPLAY_NAME, getDisplayName());

        // boss health
        ghast.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        ghast.offer(Keys.HEALTH, NEW_HEALTH);

        ghast.offer(Keys.IS_SILENT, true);

        // entity should despawn
        ghast.offer(Keys.PERSISTS, false);

        applyEquipment(ghast);
        applyLootTable(ghast);

        EnderCrystal endCrystal = (EnderCrystal) world.createEntity(EntityTypes.ENDER_CRYSTAL, blockPos);
        ((EntityEnderCrystal) endCrystal).setShowBottom(false);

        return world1 -> {
            world1.spawnEntity(ghast);
            world1.spawnEntity(endCrystal);
            ghast.addPassenger(endCrystal);
            onLoad(ghast);
            return ghast;
        };
    }

    public boolean ownerLoot()
    {
        return false;
    }

}
