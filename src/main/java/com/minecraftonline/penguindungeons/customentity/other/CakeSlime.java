package com.minecraftonline.penguindungeons.customentity.other;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.monster.Slime;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.monster.EntitySlime;

import java.util.Collections;
import java.util.List;

public class CakeSlime extends SlimeType<Slime> {

    public static final double NEW_HEALTH = 15.0;

    public CakeSlime(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.WHITE, "Cake");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A cake monster"));
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        // smallest size slime, so it does not split in to default (but same name) slimes
        return Spawnable.of(makeSlime(world, pos, 0), this::onLoad);
    }

    @Override
    public Slime makeSlime(World world, Vector3d blockPos, int size) {
        Slime slime = super.makeSlime(world, blockPos, size);

        slime.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        slime.offer(Keys.HEALTH, NEW_HEALTH);
        slime.offer(Keys.IS_SILENT, true);

        return slime;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);

        Slime slime = (Slime) entity;
        EntitySlime entitySlime = (EntitySlime) slime;

        // health has to be reset here because vanilla loading from nbt sets max health
        slime.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        slime.offer(Keys.HEALTH, NEW_HEALTH);
    }

    @Override
    public boolean ownerLoot()
    {
        return false;
    }
}
