package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.ParrotVariants;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.animal.Parrot;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIFollow;
import net.minecraft.entity.ai.EntityAIFollowOwnerFlying;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILandOnOwnersShoulder;
import net.minecraft.entity.ai.EntityAIOwnerHurtByTarget;
import net.minecraft.entity.ai.EntityAIOwnerHurtTarget;
import net.minecraft.entity.ai.EntityAISit;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWaterFlying;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.passive.EntityParrot;
import net.minecraft.entity.player.EntityPlayer;

public class Kea extends AbstractCustomEntity {

    protected double HEALTH = 20.0F;

    public Kea(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.PARROT;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.DARK_GREEN, "Kea");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeParrot(world, pos), this::onLoad);
    }

    public Spawnable createEntity(World world, Vector3d pos, UUID owner) {
        return Spawnable.of(makeParrot(world, pos, owner), this::onLoad);
    }

    public Parrot makeParrot(World world, Vector3d blockPos, UUID owner) {
        Parrot parrot = makeParrot(world, blockPos);
        ((EntityParrot) parrot).setOwnerId(owner);
        ((EntityParrot) parrot).setTamed(true);
        parrot.offer(Keys.PERSISTS, true);
        return parrot;
    }

    public Parrot makeParrot(World world, Vector3d blockPos) {
        Parrot parrot = (Parrot) world.createEntity(getType(), blockPos);
        parrot.offer(new PDEntityTypeData(getId()));
        parrot.offer(new PDSpawnData(true));
        parrot.offer(Keys.DISPLAY_NAME, getDisplayName());
        parrot.offer(Keys.PARROT_VARIANT, ParrotVariants.GREEN);

        ((EntityLivingBase) parrot).getEntityAttribute(SharedMonsterAttributes.ARMOR).applyModifier(new AttributeModifier("Kea Armor", 7, 0));
        ((EntityLivingBase) parrot).getEntityAttribute(SharedMonsterAttributes.FLYING_SPEED).setBaseValue(0.6D);
        ((EntityLivingBase) parrot).getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3D);

        parrot.offer(Keys.MAX_HEALTH, HEALTH);
        parrot.offer(Keys.HEALTH, HEALTH);

        applyEquipment(parrot);
        applyLootTable(parrot);
        return parrot;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Parrot)) {
            throw new IllegalArgumentException("Expected a Parrot to be given to Kea to load, but got: " + entity);
        }
        Parrot parrot = (Parrot) entity;
        EntityParrot entityParrot = (EntityParrot) parrot;
        Goal<Agent> normalGoals = parrot.getGoal(GoalTypes.NORMAL).get();

        // remove everything besides EntityAISit, which is priority 2
        normalGoals.getTasks().stream().filter(aiTask -> !(aiTask instanceof EntityAISit))
                   .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        // add back everything (besides panic task) but with different priorities
        normalGoals.addTask(0, (AITask<? extends Agent>) new EntityAISwimming(entityParrot))
                   // AttackMelee must be after AISit so it remains sitting
                   .addTask(3, (AITask<? extends Agent>) new EntityAIAttackMelee(entityParrot, 1.0D, true))
                   // following owner and landing on shoulder should be after so it will attack
                   .addTask(4, (AITask<? extends Agent>) new EntityAIFollowOwnerFlying(entityParrot, 1.0D, 5.0F, 1.0F))
                   .addTask(4, (AITask<? extends Agent>) new EntityAIWanderAvoidWaterFlying(entityParrot, 1.0D))
                   .addTask(5, (AITask<? extends Agent>) new EntityAILandOnOwnersShoulder(entityParrot))
                   .addTask(5, (AITask<? extends Agent>) new EntityAIFollow(entityParrot, 1.0D, 3.0F, 7.0F))
                   .addTask(6, (AITask<? extends Agent>) new EntityAIWatchClosest(entityParrot, EntityPlayer.class, 8.0F));

        Goal<Agent> targetGoals = parrot.getGoal(GoalTypes.TARGET).get();
        targetGoals.addTask(1, (AITask<? extends Agent>) new EntityAIOwnerHurtByTarget(entityParrot));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAIOwnerHurtTarget(entityParrot));
        targetGoals.addTask(3, (AITask<? extends Agent>) new EntityAIHurtByTarget(entityParrot, false, new Class[0]));

        // this happens onLoad instead of makeParrot because parrots do not have attack damage at all by default
        // so if one is unloaded and reloaded this needs to be added back
        if (((EntityLivingBase) parrot).getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE) == null) {
            ((EntityLivingBase) parrot).getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(5.0D);
        }
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A Giant Mountain Parrot which defends their owner"));
    }

}
