package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.animal.Pig;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.PanicWithRider;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.passive.EntityPig;

public class PigchinkoPig extends AbstractCustomEntity {

    public PigchinkoPig(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.PIG;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.LIGHT_PURPLE, "Pigchinko Pig");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makePig(world, pos), this::onLoad);
    }

    public Pig makePig(World world, Vector3d blockPos) {
        Pig pig = (Pig) world.createEntity(getType(), blockPos);
        pig.offer(new PDEntityTypeData(getId()));
        pig.offer(new OwnerLootData(ownerLoot()));
        pig.offer(new PDSpawnData(true));
        pig.offer(Keys.PERSISTS, false);
        pig.offer(Keys.PIG_SADDLE, true);

        EntityPig mcPig = (EntityPig) pig;
        IAttributeInstance speedAttributes = mcPig.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED);
        speedAttributes.getModifiers().stream().forEach(
                modifier -> speedAttributes.removeModifier(modifier));
        speedAttributes.setBaseValue(0.3D);
        mcPig.addTag("Pigchinko");

        applyEquipment(pig);
        applyLootTable(pig);
        return pig;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Pig)) {
            throw new IllegalArgumentException("Expected a Pig to be given to Pigchinko Pig to load, but got: " + entity);
        }
        Pig pig = (Pig) entity;
        EntityPig entityPig = (EntityPig) pig;

        // remove regular tasks
        Goal<Agent> normalGoals = pig.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();
        normalGoals.addTask(1, (AITask<? extends Agent>) new EntityAISwimming(entityPig));
        normalGoals.addTask(2, new PanicWithRider.PanicWithPlayerRiderTask(pig, 1.25D));
        normalGoals.addTask(3, (AITask<? extends Agent>) new EntityAIWanderAvoidWater(entityPig, 0.8D));

        // no targets
        Goal<Agent> targetGoals = pig.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
    }

    public boolean ownerLoot()
    {
        return false;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A Pigchinko Pig that runs around randomly when ridden"));
    }

}
