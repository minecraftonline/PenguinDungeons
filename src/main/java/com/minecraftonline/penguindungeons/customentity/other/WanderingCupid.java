package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.WitherSkeleton;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityWitherSkeleton;
import net.minecraft.entity.player.EntityPlayer;

import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class WanderingCupid extends AbstractCustomEntity {

    private static final double MOB_HEALTH = 25;

    public WanderingCupid(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.WITHER_SKELETON;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.LIGHT_PURPLE, "Cupid");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Spawns a wandering cupid"));
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    public WitherSkeleton makeWitherSkeleton(World world, Vector3d blockPos) {
        WitherSkeleton witherSkeleton = (WitherSkeleton) world.createEntity(getType(), blockPos);

        witherSkeleton.offer(Keys.MAX_HEALTH, MOB_HEALTH);
        witherSkeleton.offer(Keys.HEALTH, MOB_HEALTH);

        witherSkeleton.offer(new PDEntityTypeData(getId()));
        witherSkeleton.offer(new OwnerLootData(true));
        witherSkeleton.offer(new PDSpawnData(true));
        witherSkeleton.offer(Keys.DISPLAY_NAME, getDisplayName());

        witherSkeleton.offer(Keys.IS_SILENT, true);

        // entity should despawn
        witherSkeleton.offer(Keys.PERSISTS, false);

        applyEquipment(witherSkeleton);
        applyLootTable(witherSkeleton);
        return witherSkeleton;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof WitherSkeleton)) {
            throw new IllegalArgumentException("Expected a WitherSkeleton to be given to WanderingCupid to load, but got: " + entity);
        }
        WitherSkeleton witherSkeleton = (WitherSkeleton) entity;

        // remove tasks for avoiding wolves or sunlight
        Goal<Agent> normalGoals = witherSkeleton.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();

        EntityWitherSkeleton mcWitherSkeleton = (EntityWitherSkeleton) witherSkeleton;
        normalGoals.addTask(1,  (AITask<? extends Agent>) new EntityAISwimming(mcWitherSkeleton));
        normalGoals.addTask(5,  (AITask<? extends Agent>) new EntityAIWanderAvoidWater(mcWitherSkeleton, 1.0D));
        normalGoals.addTask(9,  (AITask<? extends Agent>) new EntityAIWatchClosest(mcWitherSkeleton, EntityPlayer.class, 3.0F, 1.0F));
        normalGoals.addTask(10, (AITask<? extends Agent>) new EntityAIWatchClosest(mcWitherSkeleton, EntityLiving.class, 8.0F));
        normalGoals.addTask(11, (AITask<? extends Agent>) new EntityAILookIdle(mcWitherSkeleton));

        // remove all target goals, this mob does not attack
        Goal<Agent> targetGoals = witherSkeleton.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos)
    {
        return Spawnable.of(makeWitherSkeleton(world, pos), this::onLoad);
    }

}
