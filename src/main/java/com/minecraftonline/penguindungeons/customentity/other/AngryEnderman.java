package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.monster.Enderman;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;

import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class AngryEnderman extends AbstractCustomEntity {

    private static final double MOB_HEALTH = 20;

    public AngryEnderman(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ENDERMAN;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.RESET, "Enderman");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Spawns an angry enderman"));
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    public Enderman makeEnderman(World world, Vector3d blockPos) {
        Enderman enderman = (Enderman) world.createEntity(getType(), blockPos);

        enderman.offer(Keys.MAX_HEALTH, MOB_HEALTH);
        enderman.offer(Keys.HEALTH, MOB_HEALTH);

        enderman.offer(new PDEntityTypeData(getId()));
        enderman.offer(new PDSpawnData(true));
        enderman.offer(Keys.ANGRY, true);

        // entity should despawn
        enderman.offer(Keys.PERSISTS, false);

        applyEquipment(enderman);
        applyLootTable(enderman);
        return enderman;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Enderman)) {
            throw new IllegalArgumentException("Expected a Enderman to be given to WanderingCupid to load, but got: " + entity);
        }
        Enderman enderman = (Enderman) entity;

        Goal<Agent> normalGoals = enderman.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();

        EntityEnderman mcEnderman = (EntityEnderman) enderman;
        normalGoals.addTask(1,  (AITask<? extends Agent>) new EntityAISwimming(mcEnderman));
        normalGoals.addTask(2,  (AITask<? extends Agent>) new EntityAIAttackMelee(mcEnderman, 1.0D, false));
        normalGoals.addTask(5,  (AITask<? extends Agent>) new EntityAIWanderAvoidWater(mcEnderman, 1.0D));
        normalGoals.addTask(9,  (AITask<? extends Agent>) new EntityAIWatchClosest(mcEnderman, EntityPlayer.class, 3.0F, 1.0F));
        normalGoals.addTask(10, (AITask<? extends Agent>) new EntityAIWatchClosest(mcEnderman, EntityLiving.class, 8.0F));
        normalGoals.addTask(11, (AITask<? extends Agent>) new EntityAILookIdle(mcEnderman));

        Goal<Agent> targetGoals = enderman.getGoal(GoalTypes.TARGET).get();
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(mcEnderman, EntityPlayer.class, true));
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos)
    {
        return Spawnable.of(makeEnderman(world, pos), this::onLoad);
    }

}
