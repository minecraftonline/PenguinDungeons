package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.monster.Vex;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.monster.EntityVex;
import net.minecraft.entity.player.EntityPlayer;

public class Butterfly extends AbstractCustomEntity {

    public Butterfly(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.VEX;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.YELLOW, "Butterfly");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeVex(world, pos), this::onLoad);
    }

    public Vex makeVex(World world, Vector3d blockPos) {
        Vex vex = (Vex) world.createEntity(getType(), blockPos);
        vex.offer(new PDEntityTypeData(getId()));
        vex.offer(new OwnerLootData(ownerLoot()));
        vex.offer(new PDSpawnData(true));
        vex.offer(Keys.DISPLAY_NAME, getDisplayName());
        vex.offer(Keys.PERSISTS, false);
        ((EntityVex) vex).setLimitedLife(TICKS_PER_SECOND*30);
        applyEquipment(vex);
        applyLootTable(vex);
        return vex;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Vex)) {
            throw new IllegalArgumentException("Expected a Vex to be given to Butterfly to load, but got: " + entity);
        }
        Vex vex = (Vex) entity;
        EntityVex entityVex = (EntityVex) vex;

        Goal<Agent> targetGoals = vex.getGoal(GoalTypes.TARGET).get();
        targetGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAINearestAttackableTarget
                || aiTask instanceof EntityAIHurtByTarget)
        .forEach(task -> targetGoals.removeTask((AITask<? extends Agent>) task));
        // note EntityVex.AICopyOwnerTarget task remains

        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) vex, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(entityVex, EntityPlayer.class, true));
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A dangerous butterfly"));
    }

}
