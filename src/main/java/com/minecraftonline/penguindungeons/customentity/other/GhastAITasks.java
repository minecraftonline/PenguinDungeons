package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Random;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.EntityMoveHelper;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.util.math.MathHelper;

public class GhastAITasks {

    static class AILookAround extends EntityAIBase {
       private final EntityGhast parentEntity;

       public AILookAround(EntityGhast ghast) {
          this.parentEntity = ghast;
          this.setMutexBits(2);
       }

       /**
        * Returns whether the EntityAIBase should begin execution.
        */
       public boolean shouldExecute() {
          return true;
       }

       /**
        * Keep ticking a continuous task that has already been started
        */
       public void updateTask() {
          if (this.parentEntity.getAttackTarget() == null) {
             this.parentEntity.rotationYaw = -((float)MathHelper.atan2(this.parentEntity.motionX, this.parentEntity.motionZ)) * 57.295776F;
             this.parentEntity.renderYawOffset = this.parentEntity.rotationYaw;
          } else {
             EntityLivingBase entitylivingbase = this.parentEntity.getAttackTarget();
             double d0 = 64.0D;
             if (entitylivingbase.getDistanceSq(this.parentEntity) < 4096.0D) {
                double d1 = entitylivingbase.posX - this.parentEntity.posX;
                double d2 = entitylivingbase.posZ - this.parentEntity.posZ;
                this.parentEntity.rotationYaw = -((float)MathHelper.atan2(d1, d2)) * 57.295776F;
                this.parentEntity.renderYawOffset = this.parentEntity.rotationYaw;
             }
          }

       }
    }

    static class AIRandomFly extends EntityAIBase {
       private final EntityGhast parentEntity;

       public AIRandomFly(EntityGhast ghast) {
          this.parentEntity = ghast;
          this.setMutexBits(1);
       }

       /**
        * Returns whether the EntityAIBase should begin execution.
        */
       public boolean shouldExecute() {
          EntityMoveHelper entitymovehelper = this.parentEntity.getMoveHelper();
          if (!entitymovehelper.isUpdating()) {
             return true;
          } else {
             double d0 = entitymovehelper.getX() - this.parentEntity.posX;
             double d1 = entitymovehelper.getY() - this.parentEntity.posY;
             double d2 = entitymovehelper.getZ() - this.parentEntity.posZ;
             double d3 = d0 * d0 + d1 * d1 + d2 * d2;
             return d3 < 1.0D || d3 > 3600.0D;
          }
       }

       /**
        * Returns whether an in-progress EntityAIBase should continue executing
        */
       public boolean shouldContinueExecuting() {
          return false;
       }

       /**
        * Execute a one shot task or start executing a continuous task
        */
       public void startExecuting() {
          Random random = this.parentEntity.getRNG();
          double d0 = this.parentEntity.posX + (double)((random.nextFloat() * 2.0F - 1.0F) * 16.0F);
          double d1 = this.parentEntity.posY + (double)((random.nextFloat() * 2.0F - 1.0F) * 16.0F);
          double d2 = this.parentEntity.posZ + (double)((random.nextFloat() * 2.0F - 1.0F) * 16.0F);
          this.parentEntity.getMoveHelper().setMoveTo(d0, d1, d2, 1.0D);
       }
    }
}
