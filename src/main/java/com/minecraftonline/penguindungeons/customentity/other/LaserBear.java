package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.animal.PolarBear;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.HasEffects;
import com.minecraftonline.penguindungeons.customentity.LaserGuardian;
import com.minecraftonline.penguindungeons.customentity.ShootLasers.LaserAttack;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.monster.EntityPolarBear;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;

public class LaserBear extends AbstractCustomEntity implements HasEffects {

    protected double HEALTH = 112.0F;

    public LaserBear(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.POLAR_BEAR;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.RED, "Lazer Bear");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makePolarBear(world, pos), this::onLoad);
    }

    public PolarBear makePolarBear(World world, Vector3d blockPos) {
        PolarBear bear = (PolarBear) world.createEntity(getType(), blockPos);
        bear.offer(new PDEntityTypeData(getId()));
        bear.offer(new OwnerLootData(ownerLoot()));
        bear.offer(new PDSpawnData(true));

        bear.offer(Keys.DISPLAY_NAME, getDisplayName());
        bear.offer(Keys.PERSISTS, false);
        bear.offer(Keys.ANGRY, true);

        // this bear is always standing
        ((EntityPolarBear) bear).setStanding(true);

        // armor
        ((EntityLivingBase) bear).getEntityAttribute(SharedMonsterAttributes.ARMOR).applyModifier(new AttributeModifier("Laser Bear Armor", 10, 0));
        ((EntityLivingBase) bear).getEntityAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).applyModifier(new AttributeModifier("Laser Bear Armor", 100, 0));

        bear.offer(Keys.MAX_HEALTH, HEALTH);
        bear.offer(Keys.HEALTH, HEALTH);

        // regenerate health
        bear.offer(Keys.POTION_EFFECTS, Collections.singletonList(
                org.spongepowered.api.effect.potion.PotionEffect.of(PotionEffectTypes.REGENERATION, 0, Integer.MAX_VALUE)));

        applyEquipment(bear);
        applyLootTable(bear);
        return bear;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof PolarBear)) {
            throw new IllegalArgumentException("Expected a PolarBear to be given to LaserBear to load, but got: " + entity);
        }
        PolarBear bear = (PolarBear) entity;
        EntityPolarBear entityBear = (EntityPolarBear) bear;

        // this bear is always standing
        entityBear.setStanding(true);

        Goal<Agent> normalGoals = bear.getGoal(GoalTypes.NORMAL).get();
        // this clears the polar bear specific regular attack which would change the standing state
        normalGoals.clear();
        normalGoals.addTask(0, (AITask<? extends Agent>) new EntityAISwimming(entityBear))
                   .addTask(1, (AITask<? extends Agent>) new EntityAIAttackMelee(entityBear, 2.5, false))
                   .addTask(2,                           new LaserAttack(bear, this))
                   // wandering doesn't seem to work, possibly because of the laser turret riding this entity
                   //.addTask(5, (AITask<? extends Agent>) new EntityAIWander(entityBear, 1.5D))
                   .addTask(6, (AITask<? extends Agent>) new EntityAIWatchClosest(entityBear, EntityPlayer.class, 6.0F))
                   .addTask(7, (AITask<? extends Agent>) new EntityAILookIdle(entityBear));

        Goal<Agent> targetGoals = bear.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();

        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) bear, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(
                (EntityCreature) entity, EntityPlayer.class, 10, true, false, new LaserGuardian.GuardianTargetSelector((EntityCreature) entity)));
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A bear which stands up and shoot lasers"));
    }

    @Override
    public List<PotionEffect> getEffects() {
        return Arrays.asList(
                new PotionEffect(MobEffects.INSTANT_DAMAGE),
                new PotionEffect(MobEffects.SLOWNESS, TICKS_PER_SECOND*5, 0));
    }

}
