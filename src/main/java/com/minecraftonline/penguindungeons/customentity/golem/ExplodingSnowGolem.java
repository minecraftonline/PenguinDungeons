package com.minecraftonline.penguindungeons.customentity.golem;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleOptions;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.golem.SnowGolem;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.FollowFromDistance;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes.FindExplodableTarget;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIAttackRanged;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.entity.player.EntityPlayer;

public class ExplodingSnowGolem extends SnowGolemType {
    public ExplodingSnowGolem(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.RED, "Explosive Snow Golem");
    }

    public boolean wearPumpkin() {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A snow golem that explodes"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        if (!(entity instanceof SnowGolem)) {
            throw new IllegalArgumentException("Expected a SnowGolem to be given to ExplodingSnowGolem to load, but got: " + entity);
        }
        SnowGolem snowGolem = (SnowGolem) entity;

        // prevent damage to fire, it will explode instead when on fire
        PotionEffect fireResistance = PotionEffect.builder().potionType(PotionEffectTypes.FIRE_RESISTANCE).amplifier(1).duration(Integer.MAX_VALUE).ambience(true).build();
        snowGolem.offer(Keys.POTION_EFFECTS, Collections.singletonList(fireResistance));

        Goal<Agent> targetGoals = snowGolem.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();

        Goal<Agent> normalGoals = snowGolem.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIAttackRanged)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(0, new StartExploding());
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
        targetGoals.addTask(2, new FindExplodableTarget((EntityCreature) snowGolem));
        targetGoals.addTask(3, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) snowGolem, EntityPlayer.class, true));
        // run towards target
        normalGoals.addTask(1, new FollowFromDistance.FollowDistance(snowGolem, 0.0F));
    }

    public static class StartExploding extends AbstractAITask<SnowGolem> {

        public static final int STARTING_FUSE_LENGTH = 30;
        private int fuseLength;

        public StartExploding() {
            super(PenguinDungeonAITaskTypes.SNOW_GOLEM_EXPLODE);
        }

        @Override
        public void start() {
            fuseLength = STARTING_FUSE_LENGTH;
        }

        @Override
        public boolean shouldUpdate() {
            if (!getOwner().isPresent()) {
                return false;
            }
            SnowGolem snowGolem = getOwner().get();
            Integer onFire = snowGolem.get(Keys.FIRE_TICKS).orElse(0);
            if (((EntitySnowman) snowGolem).isInWater() || onFire > 0)
            {
                // explode when in water or on fire
                return true;
            }
            if (!getOwner().get().getTarget().isPresent())
            {
                return false;
            }
            if (snowGolem.isRemoved()) {
                return false;
            }
            if (!snowGolem.getTarget().isPresent())
            {
                return false;
            }
            Entity target = snowGolem.getTarget().get();
            if (target.isRemoved()) {
                return false;
            }
            if (!snowGolem.getLocation().getExtent().equals(target.getLocation().getExtent())) {
                return false;
            }
            return !(snowGolem.getLocation().getPosition().distanceSquared(target.getLocation().getPosition()) > 5 * 5);
        }

        @Override
        public void update() {
            SnowGolem snowGolem = getOwner().get();
            if (fuseLength == STARTING_FUSE_LENGTH) {
                snowGolem.getLocation().getExtent().playSound(SoundTypes.ENTITY_TNT_PRIMED, snowGolem.getLocation().getPosition(), 1.0D);
                // set on fire to indicate explosion
                getOwner().get().offer(Keys.FIRE_TICKS, 200);
            }
            fuseLength--;
            // its gonna blow chief.
            if (fuseLength <= 0) {
                BlockState snowBlock = BlockState.builder().blockType(BlockTypes.SNOW).build();
                ParticleEffect snowBreak = ParticleEffect.builder().type(ParticleTypes.BREAK_BLOCK).option(ParticleOptions.BLOCK_STATE, snowBlock).build();
                EntitySnowman mcSnowGolem = (EntitySnowman) snowGolem;
                Location<World> location = snowGolem.getLocation();
                location.getExtent().spawnParticles(snowBreak, snowGolem.getLocation().getPosition().add(0.0F, mcSnowGolem.height / 2.0F, 0.0F));
                ((EntityLivingBase) snowGolem).world.createExplosion((EntityLivingBase) snowGolem, location.getX(), location.getY() + mcSnowGolem.height / 2.0F, location.getZ(), 3.5F, false);

                snowGolem.remove();
            }
        }

        @Override
        public boolean continueUpdating() {
            return shouldUpdate();
        }

        @Override
        public void reset() {
            getOwner().get().offer(Keys.FIRE_TICKS, 0);
            this.fuseLength = STARTING_FUSE_LENGTH;
        }

        @Override
        public boolean canRunConcurrentWith(AITask<SnowGolem> other) {
            return true;
        }

        @Override
        public boolean canBeInterrupted() {
            return false;
        }
    }
}
