package com.minecraftonline.penguindungeons.customentity.golem;

public interface HasBossBar {

    void useBossBar(boolean value);

}
