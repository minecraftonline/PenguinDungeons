package com.minecraftonline.penguindungeons.customentity.golem;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.golem.SnowGolem;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.monster.EntitySnowman;

public abstract class SnowGolemType extends AbstractCustomEntity {

    public static final double NEW_HEALTH = 20;

    public SnowGolemType(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.SNOWMAN;
    }

    public abstract Text getDisplayName();

    public abstract boolean wearPumpkin();

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeSnowGolem(world, pos), this::onLoad);
    }

    protected SnowGolem makeSnowGolem(World world, Vector3d blockPos) {
        SnowGolem snowGolem = (SnowGolem) world.createEntity(getType(), blockPos);
        ((EntitySnowman) snowGolem).setPumpkinEquipped(wearPumpkin());
        snowGolem.offer(Keys.DISPLAY_NAME, getDisplayName());
        snowGolem.offer(new PDEntityTypeData(getId()));
        snowGolem.offer(new OwnerLootData(ownerLoot()));
        snowGolem.offer(new PDSpawnData(true));
        // increase health to 20 (same as Zombie/Skeleton)
        snowGolem.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        snowGolem.offer(Keys.HEALTH, NEW_HEALTH);
        applyEquipment(snowGolem);
        applyLootTable(snowGolem);
        return snowGolem;
    }

    public boolean ownerLoot()
    {
        return true;
    }

}
