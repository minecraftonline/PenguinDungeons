package com.minecraftonline.penguindungeons.customentity.bee;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.util.ResourceKey;

public class ZombieBee extends BeeType<Zombie> {

    public ZombieBee(ResourceKey key) {
        super(key);
    }

    @Override
    public Text getDisplayName() {
        return getEntityName();
    }

    @Override
    public Text getEntityName() {
        return Text.of(TextColors.RESET, "Zombee");
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ZOMBIE;
    }

    @Override
    protected String getTexture() {
        return "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNmM1NTczZGU5ZjZhODgyMzNiMDBhMmYzOTNkMzk1ZjczOGQ3YzM3ZGY3MWZmMThjNTM1MmIwMDM2NmRlMmU0MSJ9fX0=";
    }

    @Override
    protected String getUUID() {
        return "5fa8658b-74e7-5fe0-8d5c-e9b3d71ddc14";
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A zombie bee"));
    }

}
