package com.minecraftonline.penguindungeons.customentity.bee;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.monster.Husk;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.customentity.FollowFromDistance;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes.FindExplodableTarget;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIZombieAttack;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.monster.EntityHusk;
import net.minecraft.entity.player.EntityPlayer;

public class AngryBombee extends BeeType<Husk> {

    public AngryBombee(ResourceKey key) {
        super(key);
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.RESET, "Angry Bombee");
    }

    @Override
    public Text getEntityName() {
        return Text.of(TextColors.RESET, "Bombee");
    }

    @Override
    protected String getTexture() {
        return "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmIwODNmODU1ZDEzMDExN2FlZTIzNWJmYmRhNjZkYWYxZjMxMTRhMjY0MDA0ZjE3NDc1ZTk4NGViMDI1ZTc2NSJ9fX0=";
    }

    @Override
    protected String getUUID() {
        return "991b594c-b735-5e6b-a9ae-82ce277a0efa";
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "An explosive bee"));
    }

    @Override
    public Husk makeBee(World world, Vector3d blockPos) {
        Husk bee = super.makeBee(world, blockPos);
        EntityHusk mcHusk = (EntityHusk) bee;
        IAttributeInstance speedAttributes = mcHusk.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED);
        speedAttributes.getModifiers().stream().forEach(
                modifier -> speedAttributes.removeModifier(modifier));
        speedAttributes.setBaseValue(0.2D);
        return bee;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        if (!(entity instanceof Husk)) {
            throw new IllegalArgumentException("Expected a Husk to be given to Angry Bombee to load, but got: " + entity);
        }
        Husk bee = (Husk) entity;

        Goal<Agent> targetGoals = bee.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();

        Goal<Agent> normalGoals = bee.getGoal(GoalTypes.NORMAL).get();
        // Remove zombie goals
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIZombieAttack
                                                      || aiTask instanceof EntityAIMoveThroughVillage)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(0, new StartExploding());
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
        targetGoals.addTask(2, new FindExplodableTarget((EntityCreature) bee));
        targetGoals.addTask(3, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) bee, EntityPlayer.class, true));
        // move towards target
        normalGoals.addTask(1, new FollowFromDistance.FollowDistance(bee, 0.0F));
    }

    public static class StartExploding extends AbstractAITask<Husk> {

        public static final int STARTING_FUSE_LENGTH = 40;
        private int fuseLength;

        public StartExploding() {
            super(PenguinDungeonAITaskTypes.BEE_EXPLODE);
        }

        @Override
        public void start() {
            fuseLength = STARTING_FUSE_LENGTH;
        }

        @Override
        public boolean shouldUpdate() {
            if (!getOwner().isPresent()) {
                return false;
            }
            Husk bee = getOwner().get();
            if (!getOwner().get().getTarget().isPresent())
            {
                return false;
            }
            if (bee.isRemoved()) {
                return false;
            }
            if (!bee.getTarget().isPresent())
            {
                return false;
            }
            Entity target = bee.getTarget().get();
            if (target.isRemoved()) {
                return false;
            }
            if (!bee.getLocation().getExtent().equals(target.getLocation().getExtent())) {
                return false;
            }
            return !(bee.getLocation().getPosition().distanceSquared(target.getLocation().getPosition()) > 5 * 5);
        }

        @Override
        public void update() {
            Husk bee = getOwner().get();
            if (fuseLength == STARTING_FUSE_LENGTH) {
                bee.getLocation().getExtent().playSound(SoundTypes.ENTITY_TNT_PRIMED, bee.getLocation().getPosition(), 1.0D);
            }
            fuseLength--;
            // its gonna blow chief.
            if (fuseLength <= 0) {
                EntityHusk husk = (EntityHusk) bee;
                Location<World> location = bee.getLocation();
                ((EntityLivingBase) bee).world.createExplosion((EntityLivingBase) bee, location.getX(), location.getY() + husk.height / 2.0F, location.getZ(), 3.5F, false);

                bee.remove();
            }
        }

        @Override
        public boolean continueUpdating() {
            return shouldUpdate();
        }

        @Override
        public void reset() {
            this.fuseLength = STARTING_FUSE_LENGTH;
        }

        @Override
        public boolean canRunConcurrentWith(AITask<Husk> other) {
            return true;
        }

        @Override
        public boolean canBeInterrupted() {
            return false;
        }
    }

}
