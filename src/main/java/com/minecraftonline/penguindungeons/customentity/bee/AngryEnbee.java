package com.minecraftonline.penguindungeons.customentity.bee;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.util.ResourceKey;

public class AngryEnbee extends BeeType<Zombie> {

    public AngryEnbee(ResourceKey key) {
        super(key);
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.RESET, "Angry Enbee");
    }

    @Override
    public Text getEntityName() {
        return Text.of(TextColors.RESET, "Enbee");
    }

    @Override
    protected String getTexture() {
        return "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTA2M2I5OWU5MjJhYTRmMTA0YzY4NGNlMzI4NmI1YTM3Y2EyOThhZWZiMGE0OTFiNWU4MjdlN2Y5OTFlOWQyMyJ9fX0=";
    }

    @Override
    protected String getUUID() {
        return "4d2e044c-0d1f-55a5-888a-ca7b315a0a3d";
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "An angry non-binary bee"));
    }

}
