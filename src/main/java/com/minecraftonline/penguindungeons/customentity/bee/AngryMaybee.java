package com.minecraftonline.penguindungeons.customentity.bee;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.monster.Husk;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.minecraftonline.penguindungeons.customentity.AvoidTarget;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIAttackMelee;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIZombieAttack;
import net.minecraft.entity.player.EntityPlayer;

public class AngryMaybee extends BeeType<Husk> {

    public AngryMaybee(ResourceKey key) {
        super(key);
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.RESET, "Angry Maybee");
    }

    @Override
    public Text getEntityName() {
        return Text.of(TextColors.RESET, "Maybee");
    }

    @Override
    protected String getTexture() {
        return "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTQ3MTFlNTdlMzc0NDE5Y2MyMThlYTJmNWMyYTJlMzkwMWYwMjRhYWY1MTIwZWY5MWM4MWVjOTU1MWU4N2NhMiJ9fX0=";
    }

    @Override
    protected String getUUID() {
        return "1a31c8a5-0d6b-57ad-8036-9d9b7959a44c";
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A maybe angry bee"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        if (!(entity instanceof Husk)) {
            throw new IllegalArgumentException("Expected a Husk to be given to Angry Maybee to load, but got: " + entity);
        }
        Husk bee = (Husk) entity;
        EntityCreature creature = (EntityCreature) bee;

        // remove task for attacking villager/irongolem/player
        Goal<Agent> targetGoals = bee.getGoal(GoalTypes.TARGET).get();
        targetGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAINearestAttackableTarget
                                                      || aiTask instanceof EntityAIHurtByTarget)
        .forEach(task -> targetGoals.removeTask((AITask<? extends Agent>) task));

        // add back attacking players
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) entity, EntityPlayer.class, true));

        Goal<Agent> normalGoals = bee.getGoal(GoalTypes.NORMAL).get();
        // Remove zombie goals
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIZombieAttack
                                                      || aiTask instanceof EntityAIMoveThroughVillage)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        // fight or flight, 50/50 chance
        boolean angry = new Random().nextFloat() <= 0.5F;

        if (angry) {
            // attack!
            normalGoals.addTask(2, (AITask<? extends Agent>) new EntityAIAttackMelee(creature, 1.0D, true));
        } else {
            // run!
            normalGoals.addTask(1, new AvoidTarget.AvoidTargetTask(bee, 10.0F, 1.0D, 1.2D));
        }
    }

}
