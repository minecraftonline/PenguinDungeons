package com.minecraftonline.penguindungeons.customentity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.World;

import java.util.Objects;
import java.util.Optional;

public class SimpleEntityType implements PDEntityType {

    private final EntityType entityType;

    public SimpleEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    @Override
    public ResourceKey getId() {
        return ResourceKey.resolve(entityType.getId());
    }

    @Override
    public EntityType getType() {
        return this.entityType;
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder()
                .itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, this.entityType)
                .build();
    }

    @Override
    public Optional<EntityArchetype> getEntityArchetype() {
        return Optional.of(EntityArchetype.of(this.entityType));
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(world.createEntity(this.entityType, pos));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleEntityType)) return false;
        SimpleEntityType that = (SimpleEntityType) o;
        return entityType.equals(that.entityType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entityType);
    }
}
