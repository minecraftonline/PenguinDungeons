package com.minecraftonline.penguindungeons.customentity.wither;

import org.spongepowered.api.entity.Entity;

import com.minecraftonline.penguindungeons.ai.AnyProjectile;
import com.minecraftonline.penguindungeons.ai.AIUtil.ProjectileForImpact;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityEgg;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class Egg extends EntityEgg {
    private ProjectileForImpact forImpact;

    public Egg(World world, EntityLivingBase shooter, double accelX, double accelY, double accelZ, ProjectileForImpact forImpact) {
        super(world, shooter);
        shooter.playSound(SoundEvents.ENTITY_EGG_THROW, 1.0F, 1.0F / (shooter.getRNG().nextFloat() * 0.4F + 0.8F));
        this.setLocationAndAngles(shooter.posX, shooter.posY + (double)shooter.getEyeHeight() - 0.10000000149011612D, shooter.posZ, shooter.rotationYaw, shooter.rotationPitch);
        this.forImpact = forImpact;
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        boolean hit = this.forImpact.forImpact(result, new AnyProjectile((Entity) this));
        if (hit) this.setDead();
    }
}