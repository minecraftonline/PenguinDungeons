package com.minecraftonline.penguindungeons.customentity.wither;

import org.spongepowered.api.entity.Entity;

import com.minecraftonline.penguindungeons.ai.AnyProjectile;
import com.minecraftonline.penguindungeons.ai.AIUtil.ProjectileForImpact;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityDragonFireball;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;

public class DragonFireball extends EntityDragonFireball {
    private ProjectileForImpact forImpact;

    public DragonFireball(World world, ProjectileForImpact forImpact) {
        super(world);
        this.forImpact = forImpact;
    }

    public DragonFireball(World world, EntityLivingBase shooter, double accelX, double accelY, double accelZ, ProjectileForImpact forImpact) {
        super(world, shooter, accelX, accelY, accelZ);
        this.forImpact = forImpact;
    }

    @Override
    protected void onImpact(RayTraceResult result) {
        boolean hit = this.forImpact.forImpact(result, new AnyProjectile((Entity) this));
        if (hit) this.setDead();
    }
}
