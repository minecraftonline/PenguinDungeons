package com.minecraftonline.penguindungeons.customentity.wither;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.Wither;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil.ProjectileCreator;
import com.minecraftonline.penguindungeons.ai.AIUtil.ProjectileForImpact;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.ai.EntityAIAttackRanged;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.BossInfo;

public class PumpkinWither extends AbstractCustomEntity {

    public PumpkinWither(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.GOLD, "Pumpking");
    }

    @Override
    public EntityType getType() {
        return EntityTypes.WITHER;
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(org.spongepowered.api.world.World world, Vector3d pos) {
        return Spawnable.of(makeWither(world, pos, true), this::onLoad);
    }

    public Wither makeWither(org.spongepowered.api.world.World world, Vector3d blockPos, boolean showBossBar) {
        EntityWither wither;
        CustomWither cWither = new CustomWither((net.minecraft.world.World) world, BossInfo.Color.YELLOW, true, showBossBar);
        cWither.setEffects(Arrays.asList(
            new PotionEffect(MobEffects.INSTANT_DAMAGE), // this also heals zombies/husks
            new PotionEffect(MobEffects.HUNGER, TICKS_PER_SECOND*3, 1))
        );
        cWither.setParticle(EnumParticleTypes.FLAME);
        cWither.setAmbientSound(SoundEvents.BLOCK_FURNACE_FIRE_CRACKLE);
        cWither.setHurtSound(SoundEvents.BLOCK_WOOD_BREAK);
        cWither.setDeathSound(SoundEvents.ENTITY_ELDER_GUARDIAN_DEATH);
        cWither.setProjectileName(Text.of(TextColors.GOLD, "Pumpkin"));
        cWither.setProjectileCreators(ProjectileCreator.forSkull(ProjectileForImpact.forImpactSetOnFire(5, 5.0F)),
                                      ProjectileCreator.forSkull(ProjectileForImpact.forImpactAreaOfEffect(0.5F, TICKS_PER_SECOND*3, cWither)));
        wither = cWither;
        wither.setPosition(blockPos.getX(), blockPos.getY(), blockPos.getZ());
        Wither eWither = (Wither) wither;
        eWither.offer(new PDEntityTypeData(getId()));
        eWither.offer(new OwnerLootData(ownerLoot()));
        eWither.offer(new PDSpawnData(true));
        eWither.offer(Keys.DISPLAY_NAME, getDisplayName());
        eWither.offer(Keys.PERSISTS, false);
        applyEquipment(eWither);
        applyLootTable(eWither);
        return eWither;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Wither)) {
            throw new IllegalArgumentException("Expected a Wither to be given to PumpkinWither to load, but got: " + entity);
        }

        Wither wither = (Wither) entity;
        EntityWither mcWither = (EntityWither) wither;

        Goal<Agent> normalGoals = wither.getGoal(GoalTypes.NORMAL).get();
        // no normal invulnerability startup task
        normalGoals.clear();
        normalGoals.addTask(1, (AITask<? extends Agent>) new EntityAISwimming(mcWither));
        normalGoals.addTask(2, (AITask<? extends Agent>) new EntityAIAttackRanged(mcWither, 1.0D, 40, 20.0F));
        normalGoals.addTask(5, (AITask<? extends Agent>) new EntityAIWanderAvoidWater(mcWither, 1.0D));
        normalGoals.addTask(6, (AITask<? extends Agent>) new EntityAIWatchClosest(mcWither, EntityPlayer.class, 8.0F));
        normalGoals.addTask(7, (AITask<? extends Agent>) new EntityAILookIdle(mcWither));

        Goal<Agent> targetGoals = wither.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers(wither, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(mcWither, EntityPlayer.class, 0, false, false, null));
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A wither that gives hunger effect"));
    }

}
