package com.minecraftonline.penguindungeons.customentity;

import org.spongepowered.api.entity.living.Creature;

import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityCreature;
public class AvoidTargetOnLowHealth extends AvoidTarget {
   private final double maxHealth;

   public AvoidTargetOnLowHealth(EntityCreature entityIn, float avoidDistanceIn, double farSpeedIn, double nearSpeedIn, double maxHealthIn) {
      super(entityIn, avoidDistanceIn, farSpeedIn, nearSpeedIn);
      this.maxHealth = maxHealthIn;
   }

   /**
    * Returns whether the EntityAIBase should begin execution.
    */
   public boolean shouldExecute() {
      return this.entity.getHealth() <= this.maxHealth && super.shouldExecute();
   }

   public static class AvoidTargetOnLowHealthTask extends DelegatingToMCAI<Creature> {
       public AvoidTargetOnLowHealthTask(Creature creature, float avoidDistanceIn, double farSpeedIn, double nearSpeedIn, double maxHealthIn) {
           super(PenguinDungeonAITaskTypes.AVOID_TARGET_ON_LOW_HEALTH, new AvoidTargetOnLowHealth((EntityCreature) creature, avoidDistanceIn, farSpeedIn, nearSpeedIn, maxHealthIn));
       }
   }
}


