package com.minecraftonline.penguindungeons.customentity.dragon;

import javax.annotation.Nullable;

import net.minecraft.entity.boss.dragon.phase.PhaseTakeoff;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class CustomPhaseTakeoff extends PhaseTakeoff implements CustomIPhase {
    private boolean firstTick;
    private Path currentPath;
    private Vec3d targetLocation;
    private CustomDragon dragon;

    public CustomPhaseTakeoff(CustomDragon dragonIn) {
        super(dragonIn);
        this.dragon = dragonIn;
    }

    @Override
    public void doLocalUpdate() {
        if (!this.firstTick && this.currentPath != null) {
            BlockPos blockpos = this.dragon.podium;
            double d0 = this.dragon.getDistanceSqToCenter(blockpos);
            if (d0 > 100.0D) {
                this.dragon.getPhaseManager().setPhase(CustomPhaseList.HOLDING_PATTERN);
            }
        } else {
            this.firstTick = false;
            this.findNewTarget();
        }

    }

    private void findNewTarget() {
        int i = this.dragon.initPathPoints();
        Vec3d vec3d = this.dragon.getHeadLookVec(1.0F);
        int j = this.dragon.getNearestPpIdx(-vec3d.x * 40.0D, 105.0D, -vec3d.z * 40.0D);
        // if low on health use smaller circles close to center
        if (this.dragon.getHealth() > (this.dragon.getMaxHealth() * 0.3)) {
            j = j % 12;
            if (j < 0) {
                j += 12;
            }
        } else {
            j = j - 12;
            j = j & 7;
            j = j + 12;
        }

        this.currentPath = this.dragon.findPath(i, j, (PathPoint)null);
        if (this.currentPath != null) {
            this.currentPath.incrementPathIndex();
            this.navigateToNextPathNode();
        }

    }

    private void navigateToNextPathNode() {
        Vec3d vec3d = this.currentPath.getCurrentPos();
        this.currentPath.incrementPathIndex();

        double d0;
        while(true) {
            d0 = vec3d.y + (double)(this.dragon.getRNG().nextFloat() * 20.0F);
            if (d0 >= vec3d.y) {
                break;
            }
        }

        this.targetLocation = new Vec3d(vec3d.x, d0, vec3d.z);
    }

    /**
     * Returns the location the dragon is flying toward
     */
    @Nullable
    public Vec3d getTargetLocation() {
        return this.targetLocation;
    }

    @Override
    public CustomPhaseList<? extends CustomIPhase> getCustomType() {
        return CustomPhaseList.TAKEOFF;
    }

}
