package com.minecraftonline.penguindungeons.customentity.dragon;

import javax.annotation.Nullable;

import net.minecraft.entity.boss.dragon.phase.PhaseLanding;
import net.minecraft.entity.boss.dragon.phase.PhaseSittingFlaming;
import net.minecraft.util.math.Vec3d;

public class CustomPhaseLanding extends PhaseLanding implements CustomIPhase {
    private Vec3d targetLocation;
    private CustomDragon dragon;
    private float lastLandingHealth;

    public CustomPhaseLanding(CustomDragon dragonIn) {
        super(dragonIn);
        this.dragon = dragonIn;
        this.lastLandingHealth = this.dragon.getHealth();
    }

    /**
     * Gives the phase a chance to update its status.
     * Called by dragon's onLivingUpdate. Only used when !worldObj.isRemote.
     */
    public void doLocalUpdate() {
        if (this.targetLocation == null) {
            this.targetLocation = new Vec3d(this.dragon.podium);
        }

        if (this.targetLocation.squareDistanceTo(this.dragon.posX, this.dragon.posY, this.dragon.posZ) < 1.0D) {
            this.lastLandingHealth = this.dragon.getHealth();
            ((PhaseSittingFlaming)this.dragon.getPhaseManager().getPhase(CustomPhaseList.SITTING_FLAMING)).resetFlameCount();
            this.dragon.getPhaseManager().setPhase(CustomPhaseList.SITTING_SCANNING);
        }

    }

    /**
     * Returns the location the dragon is flying toward
     */
    @Nullable
    public Vec3d getTargetLocation() {
        return this.targetLocation;
    }

    @Override
    public CustomPhaseList<? extends CustomIPhase> getCustomType() {
        return CustomPhaseList.LANDING;
    }

    public float getLastLandingHealth() {
        return this.lastLandingHealth;
    }
}
