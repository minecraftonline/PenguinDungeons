package com.minecraftonline.penguindungeons.customentity.dragon;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.complex.EnderDragon;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.HasEffects;
import com.minecraftonline.penguindungeons.customentity.dragon.CustomDragon.AttackType;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BossInfo;
import net.minecraft.world.end.DragonFightManager;

public class AstralMonarchFlippehDragon extends AstralDragon implements DragonType {

    // apparently 1024 is the max health a mob can actually have?
    // even though max health can be set higher
    public static final double BOSS_HEALTH = 1000;

    public AstralMonarchFlippehDragon(ResourceKey key) {
        super(key);
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.YELLOW, "Astral_Monarch_Flippeh");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeDragon(world, pos, null), this::onLoad);
    }

    @Override
    public EnderDragon makeDragon(World world, Vector3d blockPos, DragonFightManager fightManager) {
        PenguinDungeons.getLogger().debug("Making Astral Monarch Flippeh Dragon");
        EntityDragon dragon;
        BlockPos start = new BlockPos(blockPos.getX(), blockPos.getY(), blockPos.getZ());
        CustomDragon cDragon = new CustomDragon((net.minecraft.world.World) world, BossInfo.Color.YELLOW, fightManager);
        cDragon.setPodium(start);
        cDragon.setEffects(Arrays.asList(
            new PotionEffect(MobEffects.INSTANT_DAMAGE),
            new PotionEffect(MobEffects.GLOWING, TICKS_PER_SECOND*6))
        );
        cDragon.setSound(SoundEvents.BLOCK_END_PORTAL_FRAME_FILL);
        cDragon.setParticle(EnumParticleTypes.REDSTONE);
        cDragon.setFireballName(Text.of(TextColors.YELLOW, "Astral Cube"));
        cDragon.setFireballCreator(AIUtil.ProjectileCreator.forSkull(AIUtil.ProjectileForImpact.forImpactWithSound(
                AIUtil.ProjectileForImpact.forMultipleImpacts(
                    AIUtil.ProjectileForImpact.forImpactAreaOfEffect(5.0F, TICKS_PER_SECOND*50, cDragon),
                    AIUtil.ProjectileForImpact.forImpactDamage(6.0F),
                    AIUtil.ProjectileForImpact.forImpactSetEffects(new HasEffects()
                    { public List<PotionEffect> getEffects() { return Collections.singletonList(new PotionEffect(MobEffects.GLOWING, TICKS_PER_SECOND*8)); }}, false),
                    AIUtil.ProjectileForImpact.forImpactTeleportRandomly()
                ), cDragon.getSound())));
        cDragon.attackTypes.add(AttackType.MINIONS);
        cDragon.setMinionEntityType(CustomEntityTypes.BUTTERFLY);
        cDragon.attackTypes.add(AttackType.TELEPORT);
        cDragon.attackTypes.add(AttackType.CHANGE_FORM);
        cDragon.setAlternateFormEntityType(CustomEntityTypes.ASTRAL_MONARCH_FLIPPEH_HUMAN);
        cDragon.setBoosted(true);
        dragon = cDragon;
        ((net.minecraft.entity.Entity) dragon).setPosition(blockPos.getX(), blockPos.getY(), blockPos.getZ());
        EnderDragon eDragon = (EnderDragon) dragon;
        eDragon.offer(new PDEntityTypeData(getId()));
        eDragon.offer(new OwnerLootData(ownerLoot()));
        eDragon.offer(new PDSpawnData(true));
        eDragon.offer(Keys.DISPLAY_NAME, getDisplayName());
        eDragon.offer(Keys.PERSISTS, true);
        eDragon.offer(Keys.MAX_HEALTH, BOSS_HEALTH);
        eDragon.offer(Keys.HEALTH, BOSS_HEALTH);
        applyEquipment(eDragon);
        applyLootTable(eDragon);
        return eDragon;
    }

    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof EnderDragon)) {
            throw new IllegalArgumentException("Expected a EnderDragon to be given to Astral Monarch Flippeh Dragon to load, but got: " + entity);
        }
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Astral Monarch Flippeh dragon version, changes to human form when at 1/5 health"));
    }

}
