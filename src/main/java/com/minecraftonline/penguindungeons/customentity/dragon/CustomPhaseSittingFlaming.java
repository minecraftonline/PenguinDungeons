package com.minecraftonline.penguindungeons.customentity.dragon;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

import static com.minecraftonline.penguindungeons.customentity.CustomEntityType.TICKS_PER_SECOND;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.dragon.CustomDragon.AttackType;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAreaEffectCloud;
import net.minecraft.entity.boss.dragon.phase.PhaseSittingFlaming;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

public class CustomPhaseSittingFlaming extends PhaseSittingFlaming implements CustomIPhase {
    private int attackTicks;
    private int attackCount;
    private EntityAreaEffectCloud areaEffectCloud;
    private double startAngle;
    private double maxAngle;
    private double angle;
    // possible attack types
    private List<AttackType> attackTypes;
    private Optional<AttackType> currentAttack = Optional.empty();
    private int bulletCooldown = TICKS_PER_SECOND / 2;
    private int teleportCooldown = TICKS_PER_SECOND * 3;
    private int teleportCount = 5;
    CustomDragon dragon;

    public CustomPhaseSittingFlaming(CustomDragon dragonIn) {
        super(dragonIn);
        this.dragon = dragonIn;
        // only allow attack types this dragon has
        this.attackTypes = Arrays.asList(AttackType.BREATH, AttackType.BULLETS, AttackType.MINIONS, AttackType.TELEPORT).stream()
                .filter(a -> this.dragon.attackTypes.contains(a))
                .collect(Collectors.toList());
    }

    /**
     * Gives the phase a chance to update its status.
     * Called by dragon's onLivingUpdate. Only used when !worldObj.isRemote.
     */
    public void doLocalUpdate() {

        if (!currentAttack.isPresent()) {
            int attackIndex;
            if (attackTypes.size() == 1) {
                // only one choice
                attackIndex = 0;
            } else {
                // pick random attack
                attackIndex = new Random().nextInt(attackTypes.size());
            }
            currentAttack = Optional.of(attackTypes.get(attackIndex));
        }

        switch(currentAttack.get()) {
            case BREATH:
                makeFlames();
                break;
            case BULLETS:
                shootBullets();
                break;
            case MINIONS:
                spawnMinions();
                break;
            case TELEPORT:
                teleportPlayers();
                break;
            default:
                break;
        }

    }

    private void makeFlames() {
        ++this.attackTicks;
        if (this.attackTicks >= 200) {
            attackComplete();
        } else if (this.attackTicks == 10) {
            Vec3d vec3d = (new Vec3d(this.dragon.dragonPartHead.posX - this.dragon.posX, 0.0D, this.dragon.dragonPartHead.posZ - this.dragon.posZ)).normalize();
            float f = 5.0F;
            double d0 = this.dragon.dragonPartHead.posX + vec3d.x * 5.0D / 2.0D;
            double d1 = this.dragon.dragonPartHead.posZ + vec3d.z * 5.0D / 2.0D;
            double d2 = this.dragon.dragonPartHead.posY + (double)(this.dragon.dragonPartHead.height / 2.0F);
            BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos(MathHelper.floor(d0), MathHelper.floor(d2), MathHelper.floor(d1));

            while(this.dragon.world.isAirBlock(blockpos$mutableblockpos) && d2 > 0) {
                --d2;
                blockpos$mutableblockpos.setPos(MathHelper.floor(d0), MathHelper.floor(d2), MathHelper.floor(d1));
            }
            if (d2 <= 0)
            {
                // there is nowhere to make flames here
                attackComplete();
                return;
            }

            d2 = (double)(MathHelper.floor(d2) + 1);
            this.areaEffectCloud = new EntityAreaEffectCloud(this.dragon.world, d0, d2, d1);
            this.areaEffectCloud.setOwner(this.dragon);
            this.areaEffectCloud.setRadius(5.0F);
            this.areaEffectCloud.setDuration(200);
            this.areaEffectCloud.setParticle(this.dragon.getParticle());
            this.areaEffectCloud.setParticleParam1(this.dragon.getParticleParam1());
            this.areaEffectCloud.setParticleParam2(this.dragon.getParticleParam2());
            this.dragon.getEffects().forEach((effect) -> {
                 this.areaEffectCloud.addEffect(effect);
            });
            this.dragon.world.spawnEntity(this.areaEffectCloud);
        }
    }

    public void shootBullets() {
        if (this.dragon.getBulletCreator() == null) attackComplete();
        ++this.attackTicks;
        double x = 5 * Math.cos(this.angle);
        double y = -2; // aim slightly down?
        double z = 5 * Math.sin(this.angle);
        // stop once full circle or enough time passed
        if (this.angle >= this.maxAngle || this.attackTicks >= 400) {
            attackComplete();
        } else if (this.attackTicks % this.bulletCooldown == 0) {

            // fire every half a second
            EntityShulkerBullet shulkerBullet = this.dragon.getBulletCreator().create(this.dragon.world, this.dragon, null, x, y, z);
            // lower slightly
            shulkerBullet.setPosition(shulkerBullet.posX, shulkerBullet.posY - 1.5, shulkerBullet.posZ);
            shulkerBullet.setCustomNameTag(this.dragon.getBulletName());
            this.dragon.world.spawnEntity(shulkerBullet);
            this.dragon.playSound(SoundEvents.ENTITY_ENDERDRAGON_SHOOT, 2.5F, 1.0F);
            this.angle += 0.4;
        }

    }

    public void spawnMinions() {
        AbstractCustomEntity minionType = this.dragon.getMinionEntityType();
        if (minionType == null) attackComplete();
        String minionName = this.dragon.getMinionName();
        ++this.attackTicks;
        double x = 10 * Math.cos(this.angle);
        double z = 10 * Math.sin(this.angle);
        // stop once full circle or enough time passed
        if (this.angle >= this.maxAngle || this.attackTicks >= 400) {
            attackComplete();
        } else if (this.attackTicks % this.bulletCooldown == 0) {
            Entity minion = ((Entity) minionType.createEntity(
                (World) this.dragon.world, new Vector3d(this.dragon.posX + x, this.dragon.posY, this.dragon.posZ + z))
                .spawn((World) this.dragon.world));
            if (minionName != null) minion.setCustomNameTag(minionName);
            minion.playSound(this.dragon.getSound(), 2.5F, 1.0F);
            this.angle += 1.5;
        }
    }

    public void teleportPlayers() {
        ++this.attackTicks;
        if (this.attackTicks >= this.teleportCooldown * this.teleportCount) {
            // stop after enough player teleports
            attackComplete();
        }
        if (this.attackTicks % this.teleportCooldown == 0) {
            EntityPlayer player = this.dragon.world.getClosestPlayerToEntity(this.dragon, 15.0D);
            if (player == null) {
                // no players to teleport
                attackComplete();
                return;
            }
            AIUtil.randomlyTeleportNearby(player);
        }
    }

    private void attackComplete() {
        this.currentAttack = Optional.empty();
        if (this.attackCount >= 4) {
            this.dragon.getPhaseManager().setPhase(CustomPhaseList.TAKEOFF);
        } else {
            this.dragon.getPhaseManager().setPhase(CustomPhaseList.SITTING_SCANNING);
        }
    }

    /**
     * Called when this phase is set to active
     */
    public void initPhase() {
        this.attackTicks = 0;
        ++this.attackCount;
        this.startAngle = Math.toRadians(this.dragon.getRotationYawHead());
        this.angle = 0; // this.startAngle;
        this.maxAngle = 2 * Math.PI; // this.startAngle + (2 * Math.PI);
        if (this.dragon.isBoosted() || this.dragon.getHealth() < (this.dragon.getMaxHealth() * 0.3)) {
            this.bulletCooldown = TICKS_PER_SECOND / 4;
            this.teleportCooldown = TICKS_PER_SECOND * 1;
            this.teleportCount = 10;
        } else {
            this.bulletCooldown = TICKS_PER_SECOND / 2;
            this.teleportCooldown = TICKS_PER_SECOND * 3;
            this.teleportCount = 5;
        }
    }

    public void resetFlameCount() {
        this.attackCount = 0;
    }

    @Override
    public CustomPhaseList<? extends CustomIPhase> getCustomType() {
        return CustomPhaseList.SITTING_FLAMING;
    }

}
