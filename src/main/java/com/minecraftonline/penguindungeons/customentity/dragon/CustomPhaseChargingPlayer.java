package com.minecraftonline.penguindungeons.customentity.dragon;

import javax.annotation.Nullable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.minecraftonline.penguindungeons.customentity.dragon.CustomDragon.AttackType;

import net.minecraft.entity.boss.dragon.phase.PhaseChargingPlayer;
import net.minecraft.util.math.Vec3d;

public class CustomPhaseChargingPlayer extends PhaseChargingPlayer implements CustomIPhase {
    private static final Logger LOGGER = LogManager.getLogger();
    private Vec3d targetLocation;
    private int timeSinceCharge;
    private CustomDragon dragon;

    public CustomPhaseChargingPlayer(CustomDragon dragonIn) {
        super(dragonIn);
        this.dragon = dragonIn;
    }

    /**
     * Gives the phase a chance to update its status.
     * Called by dragon's onLivingUpdate. Only used when !worldObj.isRemote.
     */
    public void doLocalUpdate() {
        if (this.targetLocation == null) {
            LOGGER.warn("Aborting charge player as no target was set.");
            this.dragon.getPhaseManager().setPhase(CustomPhaseList.HOLDING_PATTERN);
        } else if (!this.dragon.attackTypes.contains(AttackType.CHARGE)) {
            // this dragon does not charge
            this.dragon.getPhaseManager().setPhase(CustomPhaseList.HOLDING_PATTERN);
        } else if (this.timeSinceCharge > 0 && this.timeSinceCharge++ >= 10) {
            this.dragon.getPhaseManager().setPhase(CustomPhaseList.HOLDING_PATTERN);
        } else {
            double d0 = this.targetLocation.squareDistanceTo(this.dragon.posX, this.dragon.posY, this.dragon.posZ);
            if (d0 < 100.0D || d0 > 22500.0D || this.dragon.collidedHorizontally || this.dragon.collidedVertically) {
                ++this.timeSinceCharge;
            }

        }
    }

    /**
     * Called when this phase is set to active
     */
    public void initPhase() {
        this.targetLocation = null;
        this.timeSinceCharge = 0;
    }

    public void setTarget(Vec3d p_188668_1_) {
        this.targetLocation = p_188668_1_;
    }

    /**
     * Returns the maximum amount dragon may rise or fall during this phase
     */
    public float getMaxRiseOrFall() {
        return 3.0F;
    }

    /**
     * Returns the location the dragon is flying toward
     */
    @Nullable
    public Vec3d getTargetLocation() {
        return this.targetLocation;
    }
    
    @Override
    public CustomPhaseList<? extends CustomIPhase> getCustomType() {
        return CustomPhaseList.CHARGING_PLAYER;
    }

}
