package com.minecraftonline.penguindungeons.customentity;

import com.google.common.base.Predicate;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.Entity;

import com.minecraftonline.penguindungeons.mixin.EntityGuardianAccessor;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.monster.EntityElderGuardian;
import net.minecraft.entity.monster.EntityGuardian;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import net.minecraft.world.storage.loot.LootTableList;

public class LaserGuardian extends EntityGuardian {
   private final HasEffects shooter;
   private final EntityLiving baseEntity;

   public LaserGuardian(World worldIn, HasEffects shooter) {
       this(worldIn, shooter, (EntityLiving) shooter);
   }
   public LaserGuardian(World worldIn, HasEffects shooter, EntityLiving creature) {
      super(worldIn);
      this.shooter = shooter;
      this.baseEntity = creature;
      this.isImmuneToFire = true;
   }

   @Override
   protected void initEntityAI() {
      this.tasks.addTask(4, new LaserGuardian.AIGuardianAttack(this));
      this.targetTasks.addTask(1, new EntityAINearestAttackableTarget<EntityPlayer>(this, EntityPlayer.class, 10, true, false, new LaserGuardian.GuardianTargetSelector(this)));
   }

   @Override
   public boolean isImmuneToExplosions() {
       return true;
   }

   @Override
   protected void entityInit() {
      super.entityInit();
   }

   @Override
   public int getAttackDuration() {
      return 80;
   }

   /**
    * Returns true if other Entities should be prevented from moving through this Entity.
    */
   @Override
   public boolean canBeCollidedWith() {
      return false;
   }

   @Nullable
   @Override
   public EntityLivingBase getTargetedEntity() {
      if (!this.hasTargetedEntity()) {
         return null;
      } else {
         return this.getAttackTarget();
      }
   }

   /**
    * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
    * use this to react to sunlight and start to burn.
    */
   @Override
   public void onLivingUpdate() {
      if (this.hasTargetedEntity()) {
         this.rotationYaw = this.rotationYawHead;
      }

      if (this.baseEntity == null || this.baseEntity.getHealth() == 0.0f || !this.baseEntity.isEntityAlive()) {
          removeLaser();
      }

      super.onLivingUpdate();
   }

   public void removeLaser()
   {
       this.setHealth(0.0f);
       this.world.removeEntity(this);
   }

   @Nullable
   @Override
   protected ResourceLocation getLootTable() {
      return LootTableList.EMPTY;
   }

   @Override
   public boolean attackEntityFrom(DamageSource source, float amount) {
       // ignore suffocation damage
       if (source == DamageSource.IN_WALL) return false;
       // attacking the laser is really just attacking the base entity
       return this.baseEntity.attackEntityFrom(source, amount);
   }

   static class AIGuardianAttack extends EntityAIBase {
      private final LaserGuardian guardian;
      private int tickCounter;
      private final boolean isElder;

      public AIGuardianAttack(EntityGuardian guardian) {
         this.guardian = (LaserGuardian) guardian;
         this.isElder = guardian instanceof EntityElderGuardian;
         this.setMutexBits(3);
      }

      protected EntityLivingBase getTarget() {
          // use base entity target if available
          EntityLivingBase baseTarget = this.guardian.baseEntity.getAttackTarget();
          if (baseTarget != null && baseTarget.isEntityAlive())
          {
              // if can see target and they are within 15 blocks
              if (this.guardian.canEntityBeSeen(baseTarget) && this.guardian.getDistanceSq(baseTarget) <= 15.0D)
              {
                  if (baseTarget != this.guardian.baseEntity)
                  {
                      this.guardian.setAttackTarget(baseTarget);
                  }
              }
          }
          if (this.guardian.baseEntity == this.guardian.getAttackTarget())
          {
              // do not target base entity
              this.guardian.setAttackTarget(null);
          }
          return this.guardian.getAttackTarget();
      }

      /**
       * Returns whether the EntityAIBase should begin execution.
       */
      public boolean shouldExecute() {
         if (this.guardian.baseEntity == null || !this.guardian.baseEntity.isEntityAlive())
         {
             // no parent, remove laser
             this.guardian.removeLaser();
             return false;
         }
         EntityLivingBase entitylivingbase = this.getTarget();
         return entitylivingbase != null && entitylivingbase.isEntityAlive();
      }

      /**
       * Returns whether an in-progress EntityAIBase should continue executing
       */
      public boolean shouldContinueExecuting() {
         return super.shouldContinueExecuting();
      }

      /**
       * Execute a one shot task or start executing a continuous task
       */
      public void startExecuting() {
         this.tickCounter = -10;
         this.guardian.getNavigator().clearPath();
         this.guardian.getLookHelper().setLookPositionWithEntity(this.getTarget(), 90.0F, 90.0F);
         this.guardian.isAirBorne = true;
      }

      /**
       * Reset the task's internal state. Called when this task is interrupted by another one
       */
      public void resetTask() {
         ((EntityGuardianAccessor) this.guardian).setTargetedEntity(0);
         this.guardian.setAttackTarget((EntityLivingBase)null);
      }

      /**
       * Keep ticking a continuous task that has already been started
       */
      public void updateTask() {
         EntityLivingBase entitylivingbase = this.getTarget();
         this.guardian.getNavigator().clearPath();
         this.guardian.getLookHelper().setLookPositionWithEntity(entitylivingbase, 90.0F, 90.0F);
         if (!this.guardian.canEntityBeSeen(entitylivingbase)) {
            this.guardian.setAttackTarget((EntityLivingBase)null);
         } else {
            ++this.tickCounter;
            this.guardian.shooter.getEffects().forEach((effect) -> {
                Entity spongeEntity = (Entity)entitylivingbase;
                List<PotionEffect> effects = spongeEntity.get(Keys.POTION_EFFECTS).orElse(new ArrayList<>());
                if (!effect.getPotion().isInstant()) {
                    effects.add((PotionEffect) effect);
                }
                spongeEntity.offer(Keys.POTION_EFFECTS, effects);
            });
            if (this.tickCounter == 0) {
                ((EntityGuardianAccessor) this.guardian).setTargetedEntity(Integer.valueOf(this.getTarget().getEntityId()));
               this.guardian.world.setEntityState(this.guardian, (byte)21);
            } else if (this.tickCounter >= this.guardian.getAttackDuration()) {
               float f = 1.0F;
               if (this.guardian.world.getDifficulty() == EnumDifficulty.HARD) {
                  f += 2.0F;
               }

               if (this.isElder) {
                  f += 2.0F;
               }

               entitylivingbase.attackEntityFrom(DamageSource.causeIndirectMagicDamage(this.guardian, this.guardian.baseEntity), f);
               entitylivingbase.attackEntityFrom(DamageSource.causeMobDamage((EntityLiving)this.guardian.baseEntity), 5.0F);
               this.guardian.setAttackTarget((EntityLivingBase)null);
               this.tickCounter = -10; // reset attack time
            }

            super.updateTask();
         }
      }
   }

   public static class GuardianTargetSelector implements Predicate<EntityPlayer> {
      private final EntityLiving parentEntity;

      public GuardianTargetSelector(EntityLiving entity) {
         this.parentEntity = entity;
      }

      public boolean apply(@Nullable EntityPlayer p_apply_1_) {
         return p_apply_1_.getDistanceSq(this.parentEntity) > 9.0D;
      }
   }
}
