package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityShulker;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Collections;
import java.util.List;

public class RegeneratingShulker extends StandardShulkerType {

    public RegeneratingShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that ", TextColors.LIGHT_PURPLE, "regenerates ", TextColors.WHITE, "health"));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.PINK;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.LIGHT_PURPLE, "Regenerating", TextColors.WHITE, " Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Entity shulker = super.makeShulker(world, blockPos);
        shulker.offer(Keys.POTION_EFFECTS, Collections.singletonList(PotionEffect.of(PotionEffectTypes.REGENERATION, 1, Integer.MAX_VALUE)));
        EntityShulker mcShulker = (EntityShulker) shulker;
        mcShulker.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(0);
        return Spawnable.of(shulker);
    }

    @Override
    public void onLoad(Entity entity) {
        // intentionally don't call super(entity) here so a new PD target task is not added
        if (!(entity instanceof Shulker)) {
            throw new IllegalArgumentException("Expected a shulker to be given to Regenerating Shulker to load, but got: " + entity);
        }
        Shulker shulker = (Shulker) entity;

        Goal<Agent> targetGoals = shulker.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();

        Goal<Agent> normalGoals = shulker.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityShulker.AIAttack)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));
    }
}
