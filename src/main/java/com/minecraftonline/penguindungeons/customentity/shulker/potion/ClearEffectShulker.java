package com.minecraftonline.penguindungeons.customentity.shulker.potion;

import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.StandardShulkerType;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class ClearEffectShulker extends StandardShulkerType {

    private final AITaskType aiTaskType;

    public ClearEffectShulker(ResourceKey key) {
        super(key);
        this.aiTaskType = PenguinDungeonAITaskTypes.SHULKER_EFFECT_ATTACK;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that clears potion effects"));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.WHITE;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.WHITE, "Clear Shulker");
    }

    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof Shulker)) {
            return;
        }
        Shulker shulker = (Shulker) entity;
        AIUtil.swapShulkerAttackForCustom(shulker, AIUtil.ShulkerBulletCreator.forClearEffects(), this.aiTaskType, this.getBulletName());
    }

    protected String getBulletName() {
        return "Clear Bullet";
    }
}
