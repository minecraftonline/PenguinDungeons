package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.List;

public class ExperienceShulker extends StandardShulkerType {

    public ExperienceShulker(ResourceKey key) {
        super(key);
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.BLUE;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.DARK_BLUE, "Experience ", TextColors.WHITE, "Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Entity shulker = super.makeShulker(world, blockPos);
        return Spawnable.of(shulker);
    }

    @Override
    public List<Text> getEntityDescription() {
        return Arrays.asList(Text.of(TextColors.WHITE, "A shulker that drops more experience"));
    }
}
