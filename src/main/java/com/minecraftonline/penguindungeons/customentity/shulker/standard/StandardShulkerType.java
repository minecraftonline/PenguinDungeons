package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;

import com.minecraftonline.penguindungeons.customentity.shulker.ShulkerType;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public abstract class StandardShulkerType extends ShulkerType {

    public StandardShulkerType(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.SHULKER;
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        return EquipmentLoadout.builder();
    }
}
