package com.minecraftonline.penguindungeons.customentity.shulker.potion;

import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.shulker.standard.StandardShulkerType;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class GlowingShulker extends StandardShulkerType {

    private final AITaskType aiTaskType;

    public GlowingShulker(ResourceKey key) {
        super(key);
        this.aiTaskType = PenguinDungeonAITaskTypes.SHULKER_EFFECT_ATTACK;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A shulker that ", TextColors.YELLOW, "glows"));
    }

    @Override
    public DyeColor getColor() {
        return DyeColors.YELLOW;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.YELLOW, "Glowing", TextColors.WHITE, " Shulker");
    }

    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof Shulker)) {
            return;
        }
        Shulker shulker = (Shulker) entity;
        shulker.offer(Keys.GLOWING, true);
        AIUtil.swapShulkerAttackForCustom(shulker, AIUtil.ShulkerBulletCreator.forCustomEffect(getPotionEffect()), this.aiTaskType, this.getBulletName());
    }

    protected PotionEffect getPotionEffect() {
        return PotionEffect.of(PotionEffectTypes.GLOWING, 0, TICKS_PER_SECOND*15);
    }

    protected String getBulletName() {
        return "Glowing Bullet";
    }
}
