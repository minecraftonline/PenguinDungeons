package com.minecraftonline.penguindungeons.customentity.shulker.potion;

import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.effect.potion.PotionEffectType;
import org.spongepowered.api.text.Text;

import java.util.List;

public class SimplePotionEffectShulkerBuilder implements SimplePotionEffectShulker.Builder {

    private static final int DEFAULT_AMPLIFIER = 1;
    private static final int DEFAULT_DURATION_TICKS = 20;

    private ResourceKey key;
    private Text displayName;
    private List<Text> description;
    private DyeColor dyeColor;
    private PotionEffectType effect;
    private int durationTicks = DEFAULT_DURATION_TICKS;
    private int amplifier = DEFAULT_AMPLIFIER;
    private String bulletName;

    @Override
    public SimplePotionEffectShulker.Builder key(ResourceKey key) {
        this.key = key;
        return this;
    }

    @Override
    public SimplePotionEffectShulker.Builder displayName(Text displayName) {
        this.displayName = displayName;
        return this;
    }

    @Override
    public SimplePotionEffectShulker.Builder description(List<Text> description) {
        this.description = description;
        return this;
    }

    @Override
    public SimplePotionEffectShulker.Builder color(DyeColor dyeColor) {
        this.dyeColor = dyeColor;
        return this;
    }

    @Override
    public SimplePotionEffectShulker.Builder effect(PotionEffectType effect) {
        this.effect = effect;
        return this;
    }

    @Override
    public SimplePotionEffectShulker.Builder amplifier(int amplifier) {
        this.amplifier = amplifier;
        return this;
    }

    @Override
    public SimplePotionEffectShulker.Builder duration(int ticks) {
        this.durationTicks = ticks;
        return this;
    }

    @Override
    public SimplePotionEffectShulker.Builder bulletName(String name) {
        this.bulletName = name;
        return this;
    }

    @Override
    public SimplePotionEffectShulker build() {
        if (this.key == null) {
            throw new IllegalStateException("Missing key!");
        }
        if (this.displayName == null) {
            throw new IllegalStateException("Missing displayname!");
        }
        if (this.description == null) {
            throw new IllegalStateException("Missing description!");
        }
        if (this.dyeColor == null) {
            throw new IllegalStateException("Missing dye color!");
        }
        if (this.bulletName == null) {
            this.bulletName = "Shulker Bullet";
        }
        return new SimplePotionEffectShulker(this.key, this.displayName, this.description,
                this.dyeColor, this.effect, this.durationTicks, this.amplifier, this.bulletName);
    }
}
