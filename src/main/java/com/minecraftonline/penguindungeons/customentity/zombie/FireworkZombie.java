package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.effect.sound.SoundType;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.ZombieVillager;
import org.spongepowered.api.entity.projectile.Firework;
import org.spongepowered.api.item.FireworkEffect;
import org.spongepowered.api.item.FireworkShapes;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.FollowFromDistance;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes.FindExplodableTarget;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIZombieAttack;
import net.minecraft.entity.monster.EntityZombieVillager;
import net.minecraft.entity.player.EntityPlayer;

public class FireworkZombie extends ZombieType<ZombieVillager> {

    private static final double NEW_YEAR_FIREWORKS_DROP_CHANCE = 0.2;
    private static final double FIREWORKS_DROP_CHANCE = 1.0;
    private static final double EXTRA_FIREWORKS_DROP_CHANCE = 0.25;
    private static List<FireworkEffect> fireworkEffects = null;
    private static ParticleEffect fireworkParticle = null;
    private static final int year;
    private static boolean newYear;

    static
    {
        Calendar calendar = Calendar.getInstance();
        int yearValue = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        // new year for next year when in last month
        if (month == Calendar.DECEMBER) yearValue++;
        year = yearValue;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        // new year firework variant
        newYear = (month == Calendar.DECEMBER && day >= 26) || (month == Calendar.JANUARY && day <= 5);
    }

    protected static List<FireworkEffect> getFireworkEffects()
    {
        if (fireworkEffects == null)
        {
            // create instance
            if (newYear)
            {
                // new year firework
                fireworkEffects = Arrays.asList(
                    FireworkEffect.builder()
                        .color(Color.ofRgb(16777045))
                        .fade(Color.ofRgb(16755200))
                        .shape(FireworkShapes.LARGE_BALL)
                        .trail(true)
                        .build()
                );
            }
            else
            {
                // zombie coloured firework
                fireworkEffects = Arrays.asList(
                    FireworkEffect.builder()
                        .colors(Color.ofRgb(99, 133, 81))
                        .fades(Color.ofRgb(73, 137, 51))
                        .shape(FireworkShapes.BALL)
                        .trail(false)
                        .build()
                );
            }
        }
        return fireworkEffects;
    }
    
    protected static ParticleEffect getFireworkParticle()
    {
        if (fireworkParticle == null)
        {
            fireworkParticle = ParticleEffect.builder().type(ParticleTypes.FIREWORKS_SPARK).build();
        }
        return fireworkParticle;
    }

    public FireworkZombie(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ZOMBIE_VILLAGER;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.GREEN, "Zomboom");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Spawns a zombie that explodes with fireworks"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof ZombieVillager)) {
            throw new IllegalArgumentException("Expected a ZombieVillager to be given to FireworkZombie to load, but got: " + entity);
        }
        ZombieVillager zombie = (ZombieVillager) entity;

        Goal<Agent> normalGoals = zombie.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIZombieAttack)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        Goal<Agent> targetGoals = zombie.getGoal(GoalTypes.TARGET).get();
        normalGoals.addTask(0, new StartExploding());
        targetGoals.addTask(1, new FindExplodableTarget((EntityCreature) zombie));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>((EntityCreature) zombie, EntityPlayer.class, true));
        // run towards target
        normalGoals.addTask(1, new FollowFromDistance.FollowDistance(zombie, 0.0F));
    }

    @Override
    protected ZombieVillager makeZombie(World world, Vector3d blockPos) {
        ZombieVillager zombie = super.makeZombie(world, blockPos);
        EntityZombieVillager mcZombie = (EntityZombieVillager) zombie;
        mcZombie.setProfession(5); // green robe
        return zombie;
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        ItemStack.Builder fireworkBuilder = ItemStack.builder().itemType(ItemTypes.FIREWORKS);
        if (newYear)
        {
            // add new year firework data
            fireworkBuilder
                    .add(Keys.FIREWORK_EFFECTS, getFireworkEffects())
                    .add(Keys.FIREWORK_FLIGHT_MODIFIER, 1)
                    .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "New Year Firework"))
                    .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Happy New Year ", year)));
            return super.makeEquipmentLoadout().mainHand(fireworkBuilder.build(), NEW_YEAR_FIREWORKS_DROP_CHANCE);
        }
        else
        {
            // drop multiple normal fireworks
            fireworkBuilder.add(Keys.FIREWORK_FLIGHT_MODIFIER, 3);
            ItemStack fireworks1, fireworks2;
            fireworks1 = fireworkBuilder.build();
            fireworks2 = fireworkBuilder.build();
            fireworks1.setQuantity(8);
            fireworks2.setQuantity(3);
            return super.makeEquipmentLoadout()
                    .mainHand(fireworks1, FIREWORKS_DROP_CHANCE)
                    .chest(fireworks2, EXTRA_FIREWORKS_DROP_CHANCE);
        }
    }

    public static class StartExploding extends AbstractAITask<ZombieVillager> {

        private static final int STARTING_FUSE_LENGTH = 30;
        private static final float fireworkOffset = 0.75F; // normal zombie raised hand height
        private final SoundType fireworkSound;
        private int fuseLength;

        public StartExploding() {
            super(PenguinDungeonAITaskTypes.FIREWORK_ZOMBIE_EXPLODE);

            if (newYear) fireworkSound = SoundTypes.ENTITY_FIREWORK_LARGE_BLAST;
            else fireworkSound = SoundTypes.ENTITY_FIREWORK_BLAST;
        }

        @Override
        public void start() {
            fuseLength = STARTING_FUSE_LENGTH;
        }

        @Override
        public boolean shouldUpdate() {
            if (!getOwner().isPresent()) {
                return false;
            }
            ZombieVillager zombie = getOwner().get();
            if (!getOwner().get().getTarget().isPresent())
            {
                return false;
            }
            if (zombie.isRemoved()) {
                return false;
            }
            if (!zombie.getTarget().isPresent())
            {
                return false;
            }
            Entity target = zombie.getTarget().get();
            if (target.isRemoved()) {
                return false;
            }
            if (!zombie.getLocation().getExtent().equals(target.getLocation().getExtent())) {
                return false;
            }
            return !(zombie.getLocation().getPosition().distanceSquared(target.getLocation().getPosition()) > 5 * 5);
        }

        @Override
        public void update() {
            ZombieVillager zombie = getOwner().get();
            if (fuseLength == STARTING_FUSE_LENGTH) {
                zombie.getLocation().getExtent().playSound(SoundTypes.ENTITY_FIREWORK_LAUNCH, zombie.getLocation().getPosition(), 1.0D);
                ((EntityZombieVillager) zombie).setArmsRaised(true);
            }
            if (fuseLength % 2 == 0)
            {
                // firework particle
                Location<World> location = zombie.getLocation();
                location.getExtent().spawnParticles(getFireworkParticle(), location.getPosition().add(0.0F, ((EntityZombieVillager) zombie).height * fireworkOffset, 0.0F));
            }
            fuseLength--;
            if (fuseLength <= 0) {
                // explode with fireworks
                Location<World> location = zombie.getLocation();
                Firework firework = (Firework) zombie.getWorld().createEntity(EntityTypes.FIREWORK, location.getPosition().add(0.0F, ((EntityZombieVillager) zombie).height * fireworkOffset, 0.0F));
                firework.offer(Keys.FIREWORK_EFFECTS, getFireworkEffects());
                firework.offer(Keys.FUSE_DURATION, 0);
                firework.offer(Keys.IS_SILENT, true);
                location.getExtent().playSound(fireworkSound, location.getPosition(), 1.0D);
                zombie.getWorld().spawnEntity(firework);
                zombie.remove();
            }
        }

        @Override
        public boolean continueUpdating() {
            return shouldUpdate();
        }

        @Override
        public void reset() {
            this.fuseLength = STARTING_FUSE_LENGTH;
            ((EntityZombieVillager) getOwner().get()).setArmsRaised(false);
        }

        @Override
        public boolean canRunConcurrentWith(AITask<ZombieVillager> other) {
            return true;
        }

        @Override
        public boolean canBeInterrupted() {
            return false;
        }
    }

}
