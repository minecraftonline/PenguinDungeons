package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.monster.Husk;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStack.Builder;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class PumpkinZombie extends ZombieType<Husk> {

    private static final double PAIL_DROP_CHANCE = 0.2;
    private static final double PUMPKIN_DROP_CHANCE = 0.1;
    private static final double NO_DROP_CHANCE = 0.0;
    private static int year = Calendar.getInstance().get(Calendar.YEAR);
    private EquipmentLoadout cachedBoostedLoadout = null;

    public PumpkinZombie(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.HUSK;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.GOLD, "Zompkin");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Spawns a zombie husk wearing a pumpkin"));
    }

    @Override
    public Husk makeZombie(World world, Vector3d blockPos) {
        Husk zombie = (Husk) world.createEntity(getType(), blockPos);

        // 10% chance to be a boosted version
        boolean boosted = new Random().nextFloat() < 0.1F;

        zombie.offer(Keys.DISPLAY_NAME, getDisplayName());
        zombie.offer(new PDEntityTypeData(getId()));
        zombie.offer(new OwnerLootData(ownerLoot()));
        zombie.offer(new PDSpawnData(true));

        // entity should despawn
        zombie.offer(Keys.PERSISTS, false);

        applyLootTable(zombie);

        if (boosted)
        {
            // more powerful pumpkin zombie
            this.getBoostedEquipmentLoadout().apply(zombie);
            zombie.offer(Keys.WALKING_SPEED, 0.3D);
            zombie.offer(Keys.MAX_HEALTH, 40.0D);
            zombie.offer(Keys.HEALTH, 40.0D);
        }
        else
        {
            applyEquipment(zombie);
            zombie.offer(Keys.WALKING_SPEED, 0.25D);
            zombie.offer(Keys.MAX_HEALTH, 30.0D);
            zombie.offer(Keys.HEALTH, 30.0D);
        }
        return zombie;
    }

    protected ItemStack makePumpkin(boolean boosted)
    {
        return ItemStack.builder()
                .itemType(boosted ? ItemTypes.LIT_PUMPKIN : ItemTypes.PUMPKIN)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Spooky Pumpkin"))
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Halloween ", year)))
                .build();
    }

    protected ItemStack makeChestplate()
    {
        return ItemStack.builder()
                .itemType(ItemTypes.LEATHER_CHESTPLATE)
                .add(Keys.COLOR, Color.ofRgb(227, 144, 29)) // Pumpkin Orange
                .build();
    }

    protected ItemStack makeBowl()
    {
        return ItemStack.builder()
                .itemType(ItemTypes.BOWL)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Pumpkin Pail"))
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Halloween ", year)))
                .build();
    }

    protected ItemStack makeHoe(boolean boosted)
    {
        Builder hoe = ItemStack.builder()
                .itemType(ItemTypes.IRON_HOE);
        if (boosted) {
            List<Enchantment> enchants = new ArrayList<Enchantment>();
            enchants.add(Enchantment.of(EnchantmentTypes.SHARPNESS, 1));
            enchants.add(Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1));
            enchants.add(Enchantment.of(EnchantmentTypes.FIRE_ASPECT, 1));
            hoe.add(Keys.ITEM_ENCHANTMENTS, enchants);
        }
        return hoe.build();
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        return super.makeEquipmentLoadout()
                .head(makePumpkin(false), PUMPKIN_DROP_CHANCE)
                .chest(makeChestplate(), NO_DROP_CHANCE)
                .boots(makeBowl(), PAIL_DROP_CHANCE)
                .offHand(makeHoe(false), NO_DROP_CHANCE);
    }

    protected EquipmentLoadout.Builder makeBoostedEquipmentLoadout()
    {
        return super.makeEquipmentLoadout()
                .head(makePumpkin(true), PUMPKIN_DROP_CHANCE)
                .chest(makeChestplate(), NO_DROP_CHANCE)
                .offHand(makeBowl(), PAIL_DROP_CHANCE)
                .mainHand(makeHoe(true), NO_DROP_CHANCE);
    }

    private EquipmentLoadout getBoostedEquipmentLoadout() {
        if (this.cachedBoostedLoadout == null) {
            this.cachedBoostedLoadout = makeBoostedEquipmentLoadout().build();
        }
        return this.cachedBoostedLoadout;
    }

}
