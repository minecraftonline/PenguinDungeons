package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;

import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.AIUtil.DirectShulkerBulletCreator;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.ai.EntityAIZombieAttack;
import net.minecraft.init.SoundEvents;

public class NinjaZombie extends ZombieType<Zombie> {

    private static final double ARMOR_DROP_CHANCE = 0.0;

    public NinjaZombie(ResourceKey key) {
        super(key);
    }

    public Text getDisplayName() {
        return Text.of(TextColors.DARK_GRAY, "Ninja Zombie");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A zombie with black leather armour that shoots throwing stars"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof Zombie)) {
            throw new IllegalArgumentException("Expected a Zombie to be given to NinjaZombie to load, but got: " + entity);
        }
        Zombie zombie = (Zombie) entity;

        Goal<Agent> normalGoals = zombie.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIZombieAttack)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(1, new StraightProjectileAttack.RangedAttack(zombie, Optional.of(Text.of("Throwing Star")), Optional.empty(),
                DirectShulkerBulletCreator.forDirectBullet(
                        AIUtil.ShulkerBulletCreator.forHit(
                                PotionEffect.of(PotionEffectTypes.SLOWNESS, 1, TICKS_PER_SECOND*10)),
                                SoundEvents.ENTITY_ITEM_BREAK), 1.3D, 25, 20F));
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        final ItemStack helmet = ItemStack.builder()
                .itemType(ItemTypes.LEATHER_HELMET)
                .add(Keys.COLOR, Color.BLACK)
                .build();
        final ItemStack chestplate = ItemStack.builder()
                .itemType(ItemTypes.LEATHER_CHESTPLATE)
                .add(Keys.COLOR, Color.BLACK)
                .build();
        final ItemStack leggings = ItemStack.builder()
                .itemType(ItemTypes.LEATHER_LEGGINGS)
                .add(Keys.COLOR, Color.BLACK)
                .build();
        final ItemStack boots = ItemStack.builder()
                .itemType(ItemTypes.LEATHER_BOOTS)
                .add(Keys.COLOR, Color.BLACK)
                .build();
        return super.makeEquipmentLoadout()
                .head(helmet, ARMOR_DROP_CHANCE)
                .chest(chestplate, ARMOR_DROP_CHANCE)
                .leggings(leggings, ARMOR_DROP_CHANCE)
                .boots(boots, ARMOR_DROP_CHANCE);
    }
}
