package com.minecraftonline.penguindungeons.customentity;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.common.util.Constants;

import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class FireworkHelper {

    private static final DataQuery FIREWORKS_EXPLOSIONS_PATH = DataQuery.of("Fireworks", "Explosions");
    private static final DataQuery COLORS_PATH = DataQuery.of("Colors");
    private static final DataQuery FADE_COLORS_PATH = DataQuery.of("FadeColors");
    private static final DataQuery[] FIREWORK_COLORS = { COLORS_PATH, FADE_COLORS_PATH };

    public static DataContainer entityTagFromItem(ItemStackSnapshot itemSnapshot) {
        DataContainer entityTag = DataContainer.createNew();

        DataContainer itemContainer = itemSnapshot.toContainer();
        Optional<DataView> itemTag = itemContainer.getView(Constants.Sponge.UNSAFE_NBT);
        if (!itemTag.isPresent()) return entityTag;
        entityTag.set(DataQuery.of("id"), EntityTypes.FIREWORK);
        entityTag.set(DataQuery.of("FireworksItem", "id"), itemSnapshot.getType());
        entityTag.set(DataQuery.of("FireworksItem", "Count"), 1);
        Optional<Integer> itemDamage = itemContainer.getInt(Constants.ItemStack.DAMAGE_VALUE);
        if (itemDamage.isPresent()) {
            entityTag.set(DataQuery.of("FireworksItem", "Damage"), itemDamage.get());
        }
        Optional<String> itemName = itemContainer.getString(DataQuery.of("display", "Name"));
        if (itemName.isPresent()) {
            entityTag.set(DataQuery.of("CustomName"), itemName);
        }
        entityTag.set(DataQuery.of("FireworksItem", "tag"), convertIntListsToIntArrays(itemTag.get()));
        // add lifetime so it doesn't instantly explode
        Optional<Integer> flight = itemContainer.getInt(DataQuery.of("UnsafeData", "Fireworks", "Flight"));
        if (flight.isPresent())
        {
            // this lifetime generation would probably be better done when the entity spawns, not when it is set in spawner
            Random rand = new Random();
            int lifeTime = ((flight.get() + 1) * 10 + rand.nextInt(6) + rand.nextInt(7));
            // this prevents the fireworks from just instantly exploding
            entityTag.set(DataQuery.of("LifeTime"), lifeTime);
        }
        return entityTag;
    }

    public static ItemStackSnapshot toSpawnEgg(ItemStackSnapshot firework) {
        ItemStack.Builder eggSnapshot = ItemStack.builder()
                .itemType(ItemTypes.SPAWN_EGG);

        ItemStack egg = eggSnapshot.build();
        DataContainer itemContainer = egg.toContainer();
        itemContainer.set(DataQuery.of("UnsafeData", "EntityTag"), entityTagFromItem(firework));

        egg.setRawData(itemContainer);
        
        Optional<Text> name = firework.get(Keys.DISPLAY_NAME);
        if (name.isPresent()) {
            egg.offer(Keys.DISPLAY_NAME, name.get());
        }

        return egg.createSnapshot();
    }

    public static ItemStackSnapshot toFireworkItem(DataContainer entityData) {
        ItemStack.Builder builder = ItemStack.builder();
        builder.itemType(ItemTypes.FIREWORKS);
        DataContainer firework = builder.build().toContainer();
        firework.set(Constants.ItemStack.DAMAGE_VALUE, entityData.getInt(DataQuery.of("FireworksItem", "Damage")).orElse(0));
        firework.set(Constants.ItemStack.COUNT, entityData.getInt(DataQuery.of("FireworksItem", "Count")).orElse(1));
        firework.set(Constants.Sponge.UNSAFE_NBT, entityData.getView(DataQuery.of("FireworksItem", "tag")).orElse(DataContainer.createNew()));
        return builder.fromContainer(firework).build().createSnapshot();
    }

    public static DataView convertIntListsToIntArrays(DataView fireworkTag) {
        Optional<List<DataView>> explosions = fireworkTag.getViewList(FIREWORKS_EXPLOSIONS_PATH);
        if (explosions.isPresent()) {
            boolean modified = false;
            List<DataView> newExplosions = new ArrayList<DataView>();
            for (DataView explosion : explosions.get()) {
                for (DataQuery colorType : FIREWORK_COLORS) {
                    Optional<List<?>> colors = explosion.getList(colorType);
                    if (colors.isPresent() && colors.get().stream().allMatch(i -> i instanceof Integer)) {
                        // convert to an int array
                        @SuppressWarnings("unchecked")
                        int[] newColors = ((List<Integer>)colors.get()).stream().mapToInt(Integer::intValue).toArray();
                        explosion.set(colorType, newColors);
                        modified = true;
                    }
                }
                newExplosions.add(explosion);
            }
            if (modified) {
                // override with new explosions
                fireworkTag.set(FIREWORKS_EXPLOSIONS_PATH, newExplosions);
            }
        }
        return fireworkTag;
    }
}
