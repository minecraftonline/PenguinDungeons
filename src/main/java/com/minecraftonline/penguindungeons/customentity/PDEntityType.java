package com.minecraftonline.penguindungeons.customentity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.Queries;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.World;
import org.spongepowered.common.util.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface PDEntityType {

    ResourceKey getId();

    default String getPermission() {
        return PenguinDungeons.BASE_PERMISSION + "entity." + getId().formattedWith(".");
    }

    public EntityType getType();

    ItemStack getSpawnEgg();

    /**
     * Creates an entity of this type.
     * @param world World to spawn.
     * @param pos Position to spawn
     * @return Entity, using this type.
     */
    Spawnable createEntity(World world, Vector3d pos);

    default Optional<EntityArchetype> getEntityArchetype() {
        DataView entityTag = DataContainer.createNew();
        List<DataView> manipulators = new ArrayList<DataView>();
        DataView manipulator = DataContainer.createNew();
        manipulator.set(Queries.CONTENT_VERSION, 2);
        manipulator.set(Constants.Sponge.DATA_ID, PenguinDungeonKeys.PD_ENTITY_TYPE.getId());
        manipulator.set(Constants.Sponge.INTERNAL_DATA, new PDEntityTypeData(getId()).toContainer());
        manipulators.add(manipulator);
        entityTag.set(DataQuery.of(Constants.Forge.FORGE_DATA, Constants.Sponge.SPONGE_DATA, Constants.Sponge.CUSTOM_MANIPULATOR_TAG_LIST), manipulators);
        return Optional.of(EntityArchetype.builder()
                .type(getType())
                .entityData(entityTag)
                .build());
    }
}
