package com.minecraftonline.penguindungeons.config;

import com.minecraftonline.penguindungeons.customentity.NBTEntityType;
import com.minecraftonline.penguindungeons.registry.PDRegistry;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.ValueContainer;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

import java.util.Collection;
import java.util.Optional;

public class PDConfigEntityRegistry extends PDRegistry<NBTEntityType> {

    public Optional<NBTEntityType> find(ItemStack spawnEgg) {
        return getKey(spawnEgg).flatMap(this::find);
    }

    public static Optional<ResourceKey> getKey(ValueContainer<?> dataHolder) {
        return dataHolder.get(Keys.DISPLAY_NAME).map(Text::toPlain)
                .map(String::toLowerCase)
                .map(s -> s.replaceAll(" ", "_"))
                .map(ResourceKey::pdConfig);
    }

    /**
     * Gets all values, that are not in the fallback registries.
     * @return All base values, excluding fallback registries.
     */
    public Collection<NBTEntityType> allBase() {
        return this.map.values();
    }
}
