package com.minecraftonline.penguindungeons.config.serializer;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.UUID;

public class UUIDSerializer implements TypeSerializer<UUID> {
    @Override
    public @Nullable UUID deserialize(@NonNull TypeToken<?> type, @NonNull ConfigurationNode value) throws ObjectMappingException {
        try {
            return UUID.fromString(value.getString());
        } catch (IllegalArgumentException e) {
            throw new ObjectMappingException("Invalid UUID", e);
        }

    }

    @Override
    public void serialize(@NonNull TypeToken<?> type, @Nullable UUID obj, @NonNull ConfigurationNode value) throws ObjectMappingException {
        if (obj != null) {
            value.setValue(obj.toString());
        }
    }
}
