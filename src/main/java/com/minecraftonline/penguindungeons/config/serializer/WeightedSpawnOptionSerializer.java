package com.minecraftonline.penguindungeons.config.serializer;

import com.google.common.reflect.TypeToken;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.dungeon.WeightedSpawnOption;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Map;

public class WeightedSpawnOptionSerializer implements TypeSerializer<WeightedSpawnOption> {
    @Override
    public @Nullable WeightedSpawnOption deserialize(@NonNull TypeToken<?> type, @NonNull ConfigurationNode value) throws ObjectMappingException {
        final WeightedSpawnOption weightedSpawnOption = new WeightedSpawnOption();
        for (Map.Entry<Object, ? extends ConfigurationNode> entry : value.getChildrenMap().entrySet()) {
            final ResourceKey key;
            try {
                key = ResourceKey.resolve(entry.getKey().toString());
            } catch (IllegalArgumentException e) {
                throw new ObjectMappingException("Invalid ResourceKey '" + entry.getKey() + "'", e);
            }
            PDEntityType pdEntityType = PenguinDungeons.getInstance().getPDEntityRegistry().find(key)
                    .orElseThrow(() -> new ObjectMappingException("No PD EntityType with id: " + entry.getKey()));
            int weight = entry.getValue().getInt();
            weightedSpawnOption.set(pdEntityType, weight);
        }
        return weightedSpawnOption;
    }

    @Override
    public void serialize(@NonNull TypeToken<?> type, @Nullable WeightedSpawnOption obj, @NonNull ConfigurationNode value) throws ObjectMappingException {
        if (obj == null) {
            return;
        }
        value.setValue(null); // Clear sub nodes.
        obj.all().forEach((entityType, weight) -> value.getNode(entityType.getId()).setValue(weight));
    }
}
