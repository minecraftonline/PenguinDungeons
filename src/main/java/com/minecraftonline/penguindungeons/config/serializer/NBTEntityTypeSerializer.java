package com.minecraftonline.penguindungeons.config.serializer;

import com.google.common.reflect.TypeToken;
import com.minecraftonline.penguindungeons.customentity.NBTEntityType;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.entity.EntityType;

public class NBTEntityTypeSerializer implements TypeSerializer<NBTEntityType> {

    private static final String ENTITY_TYPE_NODE = "entity-type";
    private static final String NBT_NODE = "nbt";

    private static final DataQuery FireworksItemTag = DataQuery.of("EntityTag", "FireworksItem", "tag");

    @Override
    public @Nullable NBTEntityType deserialize(@NonNull TypeToken<?> type, @NonNull ConfigurationNode value) throws ObjectMappingException {
        final String key = value.getKey().toString();
        final String entityType = value.getNode(ENTITY_TYPE_NODE).getString();
        if (entityType == null) {
            throw new ObjectMappingException("Missing entity type in custom entity!");
        }
        final EntityType entity = Sponge.getRegistry().getType(EntityType.class, entityType)
                .orElseThrow(() -> new ObjectMappingException("Unknown entity type: '" + entityType + "'"));
        final String nbt = value.getNode(NBT_NODE).getString();

        if (nbt == null) {
            throw new ObjectMappingException("Missing nbt in custom entity!");
        }
        final NBTTagCompound compound;
        try {
            compound = JsonToNBT.getTagFromJson(nbt);
        } catch (NBTException e) {
            throw new ObjectMappingException("Invalid json", e);
        }

        // TODO: In theory NBT should be parsed the same as command blocks now - and hence be fine.
        /*if (entity == EntityTypes.FIREWORK) {
            Optional<DataView> itemTag = json.getView(FireworksItemTag);
            // make sure fireworks use int arrays (instead of a list of ints) for firework colors
            if (itemTag.isPresent()) {
                json.set(FireworksItemTag, FireworkHelper.convertIntListsToIntArrays(itemTag.get()));
            }
        }*/

        return new NBTEntityType(ResourceKey.pdConfig(key), entity, compound);
    }

    @Override
    public void serialize(@NonNull TypeToken<?> type, @Nullable NBTEntityType obj, @NonNull ConfigurationNode value) throws ObjectMappingException {
        if (obj == null) {
            value.setValue(null);
            return;
        }
        value.getNode(ENTITY_TYPE_NODE).setValue(obj.getEntityType().getId());
        value.getNode(NBT_NODE).setValue(obj.getNbt().toString());
    }
}
