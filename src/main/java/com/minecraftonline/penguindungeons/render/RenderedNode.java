package com.minecraftonline.penguindungeons.render;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.collect.Sets;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.equipment.EquipmentType;
import org.spongepowered.api.item.inventory.equipment.EquipmentTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

class RenderedNode {
    private final UUID markerBase;
    private final UUID top;
    private final Vector3d referencedPosition;

    private RenderedNode(final UUID markerBase, final UUID top, Vector3d referencedPosition) {
        this.markerBase = markerBase;
        this.top = top;
        this.referencedPosition = referencedPosition;
    }

    public static RenderedNode spawn(World world, Vector3d pos) {
        ArmorStand topEntity = (ArmorStand) world.createEntity(EntityTypes.ARMOR_STAND, pos);
        topEntity.offer(Keys.DISPLAY_NAME, Text.of("PDRenderedSpawnerNode")); // Useful for /kill @e[name=..] if something goes wrong.
        topEntity.offer(Keys.INVULNERABLE, true);
        topEntity.offer(Keys.INVISIBLE, true);
        Set<EquipmentType> allTypes = Sets.newHashSet(EquipmentTypes.HEADWEAR, EquipmentTypes.CHESTPLATE, EquipmentTypes.LEGGINGS, EquipmentTypes.BOOTS, EquipmentTypes.MAIN_HAND, EquipmentTypes.OFF_HAND);
        topEntity.offer(Keys.ARMOR_STAND_TAKING_DISABLED, allTypes);
        topEntity.offer(Keys.ARMOR_STAND_PLACING_DISABLED, allTypes);
        topEntity.offer(Keys.ARMOR_STAND_IS_SMALL, true);
        topEntity.offer(Keys.HAS_GRAVITY, false);
        ItemStack head = ItemStack.of(ItemTypes.MOB_SPAWNER);
        head.offer(Keys.ITEM_ENCHANTMENTS, Collections.singletonList(Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1)));
        topEntity.equip(EquipmentTypes.HEADWEAR, head);
        topEntity.setRotation(Vector3d.ZERO);

        final ArmorStand base = (ArmorStand) world.createEntity(EntityTypes.ARMOR_STAND, pos.toDouble().add(0, -0.4, 0));
        base.offer(Keys.DISPLAY_NAME, Text.of("PDRenderedSpawnerNodeBase"));
        base.offer(Keys.INVULNERABLE, true);
        base.offer(Keys.INVISIBLE, true);
        base.offer(Keys.ARMOR_STAND_MARKER, true);
        base.offer(Keys.HAS_GRAVITY, false);
        base.setRotation(Vector3d.ZERO);

        world.spawnEntity(base);
        world.spawnEntity(topEntity);
        base.addPassenger(topEntity);
        return new RenderedNode(base.getUniqueId(), topEntity.getUniqueId(), pos);
    }

    public List<UUID> getEntities() {
        return Arrays.asList(this.markerBase, this.top);
    }

    public Vector3d getReferencedPosition() {
        return referencedPosition;
    }

    public void remove(World world) {
        Optional<Entity> markerBase = world.getEntity(this.markerBase);
        Optional<Entity> top = world.getEntity(this.top);
        if (markerBase.isPresent()) {
            markerBase.get().remove();
        } else {
            PenguinDungeons.getLogger().error("Failed to cleanup rendered node (base), entity with uuid " + this.markerBase);
        }
        if (top.isPresent()) {
            top.get().remove();
        } else {
            PenguinDungeons.getLogger().error("Failed to cleanup rendered node (top), entity with uuid " + this.top);
        }
    }
}
