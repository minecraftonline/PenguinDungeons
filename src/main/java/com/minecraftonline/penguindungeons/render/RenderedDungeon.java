package com.minecraftonline.penguindungeons.render;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import com.minecraftonline.penguindungeons.dungeon.WeightedSpawnOption;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.storage.ChunkLayout;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class RenderedDungeon {
    private final Multimap<Vector3i, RenderedNode> renderedSpawnerNodes = HashMultimap.create();
    private final Map<UUID, RenderedNode> uuidToRenderedNodes = new HashMap<>();
    private final Dungeon dungeon;

    public RenderedDungeon(final Dungeon dungeon) {
        this.dungeon = dungeon;
    }

    public void renderOnlyChunks(final Set<Vector3i> chunks) {
        final Optional<World> worldOptional = Sponge.getServer().loadWorld(this.dungeon.world());
        if (!worldOptional.isPresent()) {
            PenguinDungeons.getLogger().error("World: " + this.dungeon.world() + " cannot be got/loaded!");
            return;
        }
        final World world = worldOptional.get();
        ChunkLayout chunkLayout = Sponge.getServer().getChunkLayout();

        // Cleanup chunks that in the chunks that need rendering //
        final List<Vector3i> toCleanup = new ArrayList<>();
        for (Vector3i currentChunk : this.renderedSpawnerNodes.keys()) {
            if (!chunks.contains(currentChunk)) {
                toCleanup.add(currentChunk);
            }
        }
        toCleanup.forEach(pos -> cleanupChunk(world, pos));

        // Render new ones //
        for (Vector3i chunkPos : chunks) {
            if (renderedSpawnerNodes.containsKey(chunkPos)) {
                continue;
            }
            for (Map.Entry<Vector3d, WeightedSpawnOption> entry : this.dungeon.spawnLocations().entrySet()) {
                Vector3i entryChunkPos = chunkLayout.forceToChunk(entry.getKey().toInt());
                if (!entryChunkPos.equals(chunkPos)) {
                    continue;
                }
                RenderedNode renderedNode = RenderedNode.spawn(world, entry.getKey());
                this.renderedSpawnerNodes.put(chunkPos, renderedNode);
                for (UUID uuid : renderedNode.getEntities()) {
                    this.uuidToRenderedNodes.put(uuid, renderedNode);
                }
            }
        }
    }

    public void addNode(Vector3d pos) {
        final Optional<World> worldOptional = Sponge.getServer().loadWorld(this.dungeon.world());
        if (!worldOptional.isPresent()) {
            PenguinDungeons.getLogger().error("World: " + this.dungeon.world() + " cannot be got/loaded!");
            return;
        }
        ChunkLayout chunkLayout = Sponge.getServer().getChunkLayout();
        final World world = worldOptional.get();

        RenderedNode renderedNode = RenderedNode.spawn(world, pos);
        this.renderedSpawnerNodes.put(chunkLayout.forceToChunk(pos.toInt()), renderedNode);
        for (UUID uuid : renderedNode.getEntities()) {
            this.uuidToRenderedNodes.put(uuid, renderedNode);
        }
    }

    public void removeNode(UUID uuid) {
        final Optional<World> worldOptional = Sponge.getServer().loadWorld(this.dungeon.world());
        if (!worldOptional.isPresent()) {
            PenguinDungeons.getLogger().error("World: " + this.dungeon.world() + " cannot be got/loaded!");
            return;
        }
        RenderedNode renderedNode = this.uuidToRenderedNodes.get(uuid);
        if (renderedNode == null) {
            PenguinDungeons.getLogger().warn("Attempted to remove rendered node that didn't exist");
            return;
        }
        this.renderedSpawnerNodes.values().remove(renderedNode);
        renderedNode.remove(worldOptional.get());
    }

    public Dungeon dungeon() {
        return this.dungeon;
    }

    public boolean ownsNode(final UUID uuid) {
        return this.uuidToRenderedNodes.containsKey(uuid);
    }

    @Nullable
    public Vector3d getReferencedLocation(final UUID uuid) {
        RenderedNode renderedNode = this.uuidToRenderedNodes.get(uuid);
        if (renderedNode == null) {
            return null;
        }
        return renderedNode.getReferencedPosition();
    }

    private void cleanupChunk(final World world, final Vector3i chunkPos) {
        final Optional<Chunk> chunk = world.loadChunk(chunkPos, false);
        if (!chunk.isPresent()) {
            PenguinDungeons.getLogger().error("Cannot get or load chunk " + chunkPos + " in world " + this.dungeon.world() + " to clean up armor stands!");
            return;
        }
        for (final RenderedNode renderedNode : this.renderedSpawnerNodes.removeAll(chunkPos)) {
            renderedNode.remove(world);
        }
    }

    public void cleanupAll() {
        final Optional<World> worldOptional = Sponge.getServer().loadWorld(this.dungeon.world());
        if (!worldOptional.isPresent()) {
            PenguinDungeons.getLogger().error("Cannot get or load world " + this.dungeon.world() + " to clean up armor stands, was it deleted?");
            return;
        }
        this.renderedSpawnerNodes.asMap().forEach((chunkPos, nodes) -> {
            Optional<Chunk> chunk = worldOptional.get().loadChunk(chunkPos, false);
            if (!chunk.isPresent()) {
                PenguinDungeons.getLogger().error("Cannot get or load chunk " + chunkPos + " in world " + this.dungeon.world() + " to clean up armor stands!");
                return;
            }
            for (final RenderedNode renderedNode : nodes) {
                renderedNode.remove(worldOptional.get());
            }
        });
        this.renderedSpawnerNodes.clear();
    }
}
