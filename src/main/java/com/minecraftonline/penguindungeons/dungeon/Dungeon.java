package com.minecraftonline.penguindungeons.dungeon;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;

public class Dungeon {

    public static final Pattern NAME_PATTERN = Pattern.compile("[a-z0-9_]+");

    private final String name;
    private final UUID world;
    private final Map<Vector3d, WeightedSpawnOption> spawnLocations = new HashMap<>();

    public static void checkNameValid(final String name) throws IllegalArgumentException {
        if (!NAME_PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("Invalid name, does not match regex: '" + NAME_PATTERN + "'");
        }
        if (Character.isDigit(name.charAt(0))) {
            throw new IllegalArgumentException("Invalid name, starts with a number");
        }
    }

    public Dungeon(final String name, final UUID world) {
        Objects.requireNonNull(name, "name of dungeon cannot be null!");
        checkNameValid(name);
        this.name = name;
        this.world = Objects.requireNonNull(world, "World UUID cannot be null!");
    }

    public String name() {
        return this.name;
    }

    public UUID world() {
        return this.world;
    }

    public void set(final Vector3d pos, final WeightedSpawnOption spawnOption) {
        this.spawnLocations.put(pos, spawnOption);
    }

    public void remove(final Vector3d pos) {
        this.spawnLocations.remove(pos);
    }

    public Map<Vector3d, WeightedSpawnOption> spawnLocations() {
        return Collections.unmodifiableMap(this.spawnLocations);
    }

    public void spawn(final boolean allowLoadingWorld) {
        final Optional<World> loadedWorld = Sponge.getServer().getWorld(this.world);
        final World world;
        if (!loadedWorld.isPresent()) {
            if (!allowLoadingWorld) {
                throw new IllegalStateException("World with uuid '" + this.world + "' is not loaded!");
            }
            world = Sponge.getServer().loadWorld(this.world).orElseThrow(() -> new IllegalStateException("No world with UUID: '" + this.world + "'"));
        }
        else {
            world = loadedWorld.get();
        }
        final Random random = new Random();
        for (final Map.Entry<Vector3d, WeightedSpawnOption> entry : this.spawnLocations.entrySet()) {
            final Vector3d pos = entry.getKey();
            final Location<World> loc = world.getLocation(pos);
            final SpawnOption spawnOption = entry.getValue();
            if (!spawnOption.shouldSpawn(loc)) {
                PenguinDungeons.getLogger().info("Skipping spawning mob at " + loc);
                continue;
            }
            Optional<Chunk> loadedChunk = world.getChunk(loc.getChunkPosition());

            final Chunk chunk = loadedChunk.orElseGet(() -> world.loadChunk(loc.getChunkPosition(), false)
                    .orElseThrow(() -> new IllegalStateException("Failed to load chunk that a dungeon mob was going to spawn in!")));

            Optional<PDEntityType> pdEntityType = spawnOption.next(random);
            if (!pdEntityType.isPresent()) {
                PenguinDungeons.getLogger().info("Skipping spawning mob at " + loc + " - Nothing to spawn");
                continue;
            }
            Spawnable spawnable = pdEntityType.get().createEntity(world, pos);
            Entity entity = spawnable.spawn(world);
            Optional<Entity> spawnedEntity = world.getEntity(entity.getUniqueId()); // chunk.getEntity seems to throw abstract method error yay.
            PenguinDungeons.getLogger().info("Spawned " + spawnedEntity);
        }
    }
}
