package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.PickCustomEntityMenu;
import com.minecraftonline.penguindungeons.command.AbstractCommand;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class BrowseCommand extends AbstractCommand {

    public BrowseCommand() {
        super(PenguinDungeons.BASE_PERMISSION + "browse");

        setExecutor((src, args) -> {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("You must be a player to use this command!"));
            }
            Player player = (Player) src;
            player.openInventory(PickCustomEntityMenu.createForSubject(player));
            return CommandResult.success();
        });
    }
}
