package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.command.PDArguments;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class ListDungeonNodesCommand extends AbstractCommand {

    private final Text DUNGEON = Text.of("dungeon");

    public ListDungeonNodesCommand() {
        super(DungeonCommand.BASE_PERMISSION + "listnodes");
        addArguments(PDArguments.dungeon(DUNGEON));
        setExecutor(((src, args) -> {
            final Dungeon dungeon = args.requireOne(DUNGEON);
            sendNodes(src, dungeon);
            return CommandResult.success();
        }));
    }

    public static void sendNodes(final CommandSource src, final Dungeon dungeon) {
        List<Text> content = new ArrayList<>();
        dungeon.spawnLocations().forEach((pos, spawnOption) -> {
            Text.Builder hoverBuilder = Text.builder();
            Iterator<PDEntityType> iterator = spawnOption.all().keySet().iterator();
            while (iterator.hasNext()) {
                final PDEntityType type = iterator.next();
                hoverBuilder.append(Text.of(TextColors.GRAY, type.getId().namespace(), ":", TextColors.GOLD, type.getId().value()));
                if (iterator.hasNext()) {
                    hoverBuilder.append(Text.NEW_LINE);
                }
            }
            Text.Builder builder = Text.builder();
            builder.append(Text.of(TextActions.showText(hoverBuilder.build()),
                    TextColors.GRAY, "(", TextColors.WHITE, pos.getX(),
                    TextColors.GRAY, ", ", TextColors.WHITE, pos.getY(),
                    TextColors.GRAY, ", ", TextColors.WHITE, pos.getZ(),
                    TextColors.GRAY, ")"));

            if (src instanceof Entity) {
                builder.append(Text.of(TextActions.showText(Text.of(TextColors.GOLD, "Click to teleport")),
                        TextActions.executeCallback(caller -> {
                            if (!(caller instanceof Entity)) {
                                return;
                            }
                            Entity entity = (Entity) caller;
                            Optional<World> optWorld = Sponge.getServer().loadWorld(dungeon.world());
                            if (!optWorld.isPresent()) {
                                caller.sendMessage(Text.of(TextColors.RED, "World with uuid '" + dungeon.world() + "' could not be got or loaded"));
                                return;
                            }
                            entity.setLocation(optWorld.get().getLocation(pos));
                        }),
                        TextColors.GRAY, " [", TextColors.GOLD, "TP", TextColors.GRAY, "]"));
            }
            content.add(builder.build());
        });
        PaginationList.builder()
                .title(Text.of(TextColors.GOLD, "Nodes for dungeon ", dungeon.name()))
                .contents(content)
                .build()
                .sendTo(src);
    }
}
