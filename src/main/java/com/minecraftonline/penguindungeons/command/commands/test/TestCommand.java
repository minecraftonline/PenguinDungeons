package com.minecraftonline.penguindungeons.command.commands.test;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.command.AbstractCommand;

public class TestCommand extends AbstractCommand {

    public TestCommand() {
        super(PenguinDungeons.BASE_PERMISSION + "test");
        addChild(new TestScrollableCommand(), "scrollableinventory");
        addChild(new TestDualScrollableCommand(), "dualscrollableinventory");
        addChild(new TestSignEditor(), "signeditor");
        addChild(new EntityArchetypeBugTest(), "entityarchetypebugtest");
    }
}
