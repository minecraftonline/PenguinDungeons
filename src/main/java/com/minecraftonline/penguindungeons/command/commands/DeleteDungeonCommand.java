package com.minecraftonline.penguindungeons.command.commands;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.command.PDArguments;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class DeleteDungeonCommand extends AbstractCommand {

    public DeleteDungeonCommand() {
        super(DungeonCommand.BASE_PERMISSION + "delete");

        addArguments(PDArguments.dungeon(Text.of("dungeon")));

        setExecutor((src, args) -> {
            Dungeon dungeon = args.requireOne("dungeon");
            PenguinDungeons.getInstance().deleteDungeon(dungeon);
            src.sendMessage(Text.of(TextColors.GREEN, "Deleted dungeon " + dungeon.name()));
            return CommandResult.success();
        });
    }
}
