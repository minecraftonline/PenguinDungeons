package com.minecraftonline.penguindungeons.interact;

import java.util.Optional;
import java.util.UUID;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.data.type.ParrotVariants;
import org.spongepowered.api.data.type.RabbitType;
import org.spongepowered.api.data.type.RabbitTypes;
import org.spongepowered.api.data.type.SkullTypes;
import org.spongepowered.api.effect.sound.SoundType;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.animal.Rabbit;
import org.spongepowered.api.entity.living.animal.Wolf;
import org.spongepowered.api.entity.living.animal.Pig;
import org.spongepowered.api.entity.living.Human;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.entity.living.animal.Animal;
import org.spongepowered.api.entity.living.animal.Parrot;
import org.spongepowered.api.entity.living.animal.Sheep;
import org.spongepowered.api.entity.living.monster.Husk;
import org.spongepowered.api.entity.living.monster.WitherSkeleton;
import org.spongepowered.api.entity.living.monster.ZombieVillager;
import org.spongepowered.api.entity.living.animal.PolarBear;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.cause.entity.damage.DamageTypes;
import org.spongepowered.api.event.cause.entity.damage.source.DamageSource;
import org.spongepowered.api.event.cause.entity.damage.source.EntityDamageSource;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.entity.projectile.DamagingProjectile;
import org.spongepowered.api.entity.projectile.source.ProjectileSource;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.customentity.human.PlayerSkinHelper;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.item.EntityEnderCrystal;

public class PDInteract {

    @Listener
    public void onInteractPDEntity(InteractEntityEvent event, @First Player player)
    {
        Optional<ItemStackSnapshot> usedItemOptional = event.getContext().get(EventContextKeys.USED_ITEM);
        if (!usedItemOptional.isPresent()) return;

        ItemStackSnapshot usedItem = usedItemOptional.get();
        Entity target = event.getTargetEntity();

        Optional<ResourceKey> pdType = target.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (target instanceof Human) {
            if (pdType.isPresent() && usedItem.getType() == ItemTypes.WOODEN_SWORD &&
                pdType.get().equals(CustomEntityTypes.STEVE_POLICE.getId()) && !target.get(Keys.PERSISTS).orElse(false))
            {
                Optional<Text> itemName = usedItem.get(Keys.DISPLAY_NAME);
                if (itemName.isPresent() && itemName.get().equals(Text.of(TextColors.DARK_BLUE, "Baton")))
                {
                    // make this officer not despawn, and give them the baton
                    target.offer(Keys.PERSISTS, true);
                    ((Human) target).setItemInHand(HandTypes.MAIN_HAND, usedItem.createStack());
                    if (player.gameMode().get() != GameModes.CREATIVE)
                    {
                        // remove baton from player
                        ItemStack item = usedItem.createStack();
                        item.setQuantity(item.getQuantity() - 1);
                        HandType hand = HandTypes.MAIN_HAND;
                        Optional<ItemStack> handItem = player.getItemInHand(hand);
                        // check if it isn't the main hand holding this used item
                        if (handItem.isPresent())
                        {
                            if (handItem.get().getType() != ItemTypes.WOODEN_SWORD)
                            {
                                hand = HandTypes.OFF_HAND;
                            }
                            else
                            {
                                Optional<Text> handItemName = handItem.get().get(Keys.DISPLAY_NAME);
                                if (!handItemName.isPresent() || !handItemName.get().equals(Text.of(TextColors.DARK_BLUE, "Baton")))
                                {
                                    hand = HandTypes.OFF_HAND;
                                }
                            }
                        }
                        player.setItemInHand(hand, item);
                    }
                }
            }
            else if (usedItem.getType() == ItemTypes.SKULL && usedItem.get(Keys.SKULL_TYPE).orElse(SkullTypes.SKELETON) == SkullTypes.PLAYER)
            {
                if (player.hasPermission(PenguinDungeons.BASE_PERMISSION + "changeHumanSkin"))
                {
                    boolean result = PlayerSkinHelper.setSkinFromData(usedItem.toContainer(), (Human) target);
                    if (result) {
                        player.sendMessage(Text.of(TextColors.GREEN, "Updated player skin for this human."));
                    } else {
                        player.sendMessage(Text.of(TextColors.RED, "Could not update player skin for this human with player head."));
                    }
                }
            }
            else if (usedItem.getType() == ItemTypes.NAME_TAG)
            {
                // disallow renaming humans
                if (!player.hasPermission(PenguinDungeons.BASE_PERMISSION + "changeHumanName"))
                {
                    event.setCancelled(true);
                }
            }
        }
        else if (target instanceof PolarBear && !target.get(Keys.DISPLAY_NAME).isPresent()
                 && usedItem.getType() == ItemTypes.POTION && usedItem.get(Keys.DISPLAY_NAME).isPresent())
        {
            Text potionName = Text.of(TextColors.GOLD, "Royal Honey");
            if (!usedItem.get(Keys.DISPLAY_NAME).get().equals(potionName)) return;

            // don't create multiple mobs from the same entity or after it has died
            if (target.isRemoved() || ((Living) target).health().get() <= 0) return;

            if (player.gameMode().get() != GameModes.CREATIVE)
            {
                // use up the item
                ItemStack item = usedItem.createStack();
                item.setQuantity(item.getQuantity() - 1);
                HandType hand = HandTypes.MAIN_HAND;
                Optional<ItemStack> handItem = player.getItemInHand(hand);
                // check if it isn't the main hand holding this used item
                if (handItem.isPresent())
                {
                    if (handItem.get().getType() != ItemTypes.POTION)
                    {
                        hand = HandTypes.OFF_HAND;
                    }
                    else
                    {
                        Optional<Text> handItemName = handItem.get().get(Keys.DISPLAY_NAME);
                        if (!handItemName.isPresent() || !handItemName.get().equals(potionName))
                        {
                            hand = HandTypes.OFF_HAND;
                        }
                    }
                }
                player.setItemInHand(hand, item);
            }

            // replace with laser bear
            Vector3d position = target.getLocation().getPosition();
            player.playSound(SoundTypes.ENTITY_POLAR_BEAR_WARNING, position, 1);
            CustomEntityTypes.LASER_BEAR.createEntity(target.getWorld(), position).spawn(target.getWorld());
            target.remove();
        }
        else if (target instanceof Parrot && !target.get(Keys.DISPLAY_NAME).isPresent()
                && usedItem.getType() == ItemTypes.EMERALD && usedItem.get(Keys.DISPLAY_NAME).isPresent())
        {
            Text itemName = Text.of(TextColors.DARK_GREEN, "Pounamu");
            if (!usedItem.get(Keys.DISPLAY_NAME).get().equals(itemName)) return;

            // don't create multiple mobs from the same entity or after it has died
            if (target.isRemoved() || ((Living) target).health().get() <= 0) return;

            // must be green
            if (!target.get(Keys.PARROT_VARIANT).isPresent() || !target.get(Keys.PARROT_VARIANT).get().equals(ParrotVariants.GREEN)) return;

            Optional<Optional<UUID>> owner = target.get(Keys.TAMED_OWNER);
            // must be tamed
            if (!owner.isPresent() || !owner.get().isPresent()) return;
            UUID uuid = owner.get().get();
            // different owner
            if (!uuid.equals(player.getUniqueId())) return;

            // replace with kea
            Vector3d position = target.getLocation().getPosition();
            player.playSound(SoundTypes.ENTITY_PARROT_EAT, position, 1);
            CustomEntityTypes.KEA.createEntity(target.getWorld(), position, uuid).spawn(target.getWorld());
            target.remove();
        }
        else if (pdType.isPresent())
        {
            if (usedItem.getType() == ItemTypes.NAME_TAG || usedItem.getType() == ItemTypes.LEAD)
            {
                // deny name tag use and lead
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.GOLDEN_APPLE && target instanceof ZombieVillager)
            {
                // deny zombie villager curing
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.BONE && target instanceof Wolf)
            {
                // deny wolf taming
                event.setCancelled(true);
            }
            else if ((usedItem.getType() == ItemTypes.SADDLE || usedItem.getType() == ItemTypes.CARROT) && target instanceof Pig)
            {
                // deny pig saddling and breeding
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.WHEAT && target instanceof Animal)
            {
                // deny animal breeding
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.SHEARS && target instanceof Sheep)
            {
                // deny sheep shearing
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.DYE && target instanceof Sheep)
            {
                // deny changing sheep colour with dye
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.COOKIE && target instanceof Parrot)
            {
                // Don't let parrots be fed cookies
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.NETHER_STAR && target instanceof Husk)
            {
                if (pdType.get().equals(CustomEntityTypes.PUMPKIN_ZOMBIE.getId()))
                {
                    Optional<ItemStack> hat = ((Husk)target).getHelmet();
                    if (hat.isPresent() && hat.get().getType() == ItemTypes.LIT_PUMPKIN)
                    {
                        // don't create multiple bosses from the same entity or after it has died
                        if (target.isRemoved() || ((Living) target).health().get() <= 0) return;

                        // this is a boosted version, convert to boss mob upon using a nether star

                        if (player.gameMode().get() != GameModes.CREATIVE)
                        {
                            // use up the item
                            ItemStack item = usedItem.createStack();
                            item.setQuantity(item.getQuantity() - 1);
                            HandType hand = HandTypes.MAIN_HAND;
                            Optional<ItemStack> handItem = player.getItemInHand(hand);
                            if (handItem.isPresent() && handItem.get().getType() != ItemTypes.NETHER_STAR) hand = HandTypes.OFF_HAND;
                            player.setItemInHand(hand, item);
                        }

                        // replace with boss
                        Vector3d position = target.getLocation().getPosition();
                        player.playSound(SoundTypes.ENTITY_ELDER_GUARDIAN_CURSE, position, 1);
                        CustomEntityTypes.PUMPKIN_WITHER.createEntity(target.getWorld(), position).spawn(target.getWorld());
                        target.remove();
                    }
                }
            }
            else if ((usedItem.getType() == ItemTypes.SPIDER_EYE || usedItem.getType() == ItemTypes.APPLE) && target instanceof WitherSkeleton)
            {
                if (usedItem.get(Keys.DISPLAY_NAME).isPresent() &&
                    pdType.get().equals(CustomEntityTypes.WANDERING_CUPID.getId()))
                {
                    // don't create multiple bosses from the same entity or after it has died
                    if (target.isRemoved() || ((Living) target).health().get() <= 0) return;

                    final SoundType spawnSound;
                    final PDEntityType spawnEntity;

                    // convert to desired mob upon using a special item
                    Text itemName = usedItem.get(Keys.DISPLAY_NAME).get();
                    if (usedItem.getType() == ItemTypes.SPIDER_EYE && itemName.equals(Text.of(TextColors.GREEN, "Forgotten Words"))) {
                        spawnSound = SoundTypes.ENTITY_WITHER_DEATH;
                        spawnEntity = CustomEntityTypes.FLYING_CUPID;
                    } else if (usedItem.getType() == ItemTypes.APPLE && itemName.equals(Text.of(TextColors.DARK_AQUA, "Completed Promise"))) {
                        spawnSound = SoundTypes.ENTITY_ZOMBIE_VILLAGER_CURE;
                        spawnEntity = CustomEntityTypes.TRADING_CUPID;
                    } else return;

                    if (player.gameMode().get() != GameModes.CREATIVE)
                    {
                        // use up the item
                        ItemStack item = usedItem.createStack();
                        item.setQuantity(item.getQuantity() - 1);
                        HandType hand = HandTypes.MAIN_HAND;
                        Optional<ItemStack> handItem = player.getItemInHand(hand);
                        if (handItem.isPresent() && !handItem.get().createSnapshot().equals(usedItem)) hand = HandTypes.OFF_HAND;
                        player.setItemInHand(hand, item);
                    }

                    // replace with desired mob
                    Vector3d position = target.getLocation().getPosition();
                    player.playSound(spawnSound, position, 1);
                    spawnEntity.createEntity(target.getWorld(), position).spawn(target.getWorld());
                    target.remove();
                }
            }
            else if (usedItem.getType() == ItemTypes.GOLDEN_CARROT && target instanceof Rabbit)
            {
                event.setCancelled(true);
            }
            else if (usedItem.getType() == ItemTypes.CARROT && target instanceof Rabbit)
            {
                if (pdType.get().equals(CustomEntityTypes.EASTER_BUNNY.getId()))
                {
                    Optional<RabbitType> rabbitType = target.get(Keys.RABBIT_TYPE);
                    if (rabbitType.isPresent() && rabbitType.get() == RabbitTypes.BROWN)
                    {
                        Optional<Text> itemName = usedItem.get(Keys.DISPLAY_NAME);
                        if (itemName.isPresent() && itemName.get().equals(Text.of(TextColors.DARK_BLUE, "Candy Carrot")))
                        {
                            // don't create multiple bosses from the same entity or after it has died
                            if (target.isRemoved() || ((Living) target).health().get() <= 0) return;

                            if (player.gameMode().get() != GameModes.CREATIVE)
                            {
                                // use up the item
                                ItemStack item = usedItem.createStack();
                                item.setQuantity(item.getQuantity() - 1);
                                HandType hand = HandTypes.MAIN_HAND;
                                Optional<ItemStack> handItem = player.getItemInHand(hand);
                                // check if it isn't the main hand holding this used item
                                if (handItem.isPresent())
                                {
                                    if (handItem.get().getType() != ItemTypes.CARROT)
                                    {
                                        hand = HandTypes.OFF_HAND;
                                    }
                                    else
                                    {
                                        Optional<Text> handItemName = handItem.get().get(Keys.DISPLAY_NAME);
                                        if (!handItemName.isPresent() || !handItemName.get().equals(Text.of(TextColors.DARK_BLUE, "Candy Carrot")))
                                        {
                                            hand = HandTypes.OFF_HAND;
                                        }
                                    }
                                }
                                player.setItemInHand(hand, item);
                            }

                            // replace with boss (egg golem)
                            Vector3d position = target.getLocation().getPosition();
                            player.playSound(SoundTypes.ENTITY_GENERIC_EAT, position, 1);
                            CustomEntityTypes.EGG_GOLEM.createEntity(target.getWorld(), position).spawn(target.getWorld());
                            target.remove();
                        }
                    }
                    // cancel here even if it is non-brown bunny to cancel usual carrot event
                    event.setCancelled(true);
                }
            }
        }
    }

    @Listener
    public void onDamagePDEntity(DamageEntityEvent event, @First DamageSource source)
    {
        Entity target = event.getTargetEntity();

        Optional<ResourceKey> pdType = target.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (pdType.isPresent())
        {
            if (pdType.get().equals(CustomEntityTypes.FLYING_CUPID.getId()))
            {
                if (source.getType() == DamageTypes.SUFFOCATE)
                {
                    // do not take suffocation damage
                    event.setCancelled(true);
                }
                else if (event.willCauseDeath())
                {
                    // remove ridden entity on death
                    if (target.getVehicle().isPresent()) target.getVehicle().get().remove();
                }
                else if (source instanceof EntityDamageSource)
                {
                    Entity entitySource = ((EntityDamageSource) source).getSource();
                    if (entitySource instanceof DamagingProjectile)
                    {
                        ProjectileSource shooter = ((DamagingProjectile) entitySource).getShooter();
                        if (shooter instanceof Entity && ((Entity) shooter).equals(target))
                        {
                            // don't self damage
                            event.setBaseDamage(0);
                            return;
                        }
                    }
                    else if (entitySource.equals(target))
                    {
                        // don't self damage
                        event.setBaseDamage(0);
                        return;
                    }
                }
            }
            else if (pdType.get().equals(CustomEntityTypes.CAKE_SLIME.getId()) || pdType.get().equals(CustomEntityTypes.ANGRY_BEE.getId()) ||
                    pdType.get().equals(CustomEntityTypes.ANGRY_BOMBEE.getId()) || pdType.get().equals(CustomEntityTypes.ANGRY_ENBEE.getId()) ||
                    pdType.get().equals(CustomEntityTypes.ANGRY_MAYBEE.getId()) || pdType.get().equals(CustomEntityTypes.HONEY_SLIME.getId()) ||
                    pdType.get().equals(CustomEntityTypes.MINION_BEE.getId()) || pdType.get().equals(CustomEntityTypes.MINION_HONEY_SLIME.getId())) {

                if (source.getType() == DamageTypes.FALL)
                {
                    // do not take fall damage
                    event.setCancelled(true);
                }
            }
            else if (pdType.get().equals(CustomEntityTypes.CHAOS_GHAST.getId()))
            {
                if (event.willCauseDeath())
                {
                    for (Entity passenger: target.getPassengers())
                    {
                        if (passenger instanceof EntityEnderCrystal)
                        {
                            ((EntityEnderCrystal) passenger).setDead();
                        }
                        if (!source.isExplosive())
                        {
                            Location<World> location = target.getLocation();
                            ((net.minecraft.entity.Entity) target).world.createExplosion(null, location.getX(), location.getY(), location.getZ(), 6.0F, false);
                        }
                    }
                }
            }
        }
    }

    @Listener
    public void onTeleport(MoveEntityEvent.Teleport event)
    {
        Entity target = event.getTargetEntity();

        Optional<ResourceKey> pdType = target.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (pdType.isPresent())
        {
            if(pdType.get().equals(CustomEntityTypes.PIGCHINKO_PIG.getId()) ||
               pdType.get().equals(CustomEntityTypes.LOVE_VEX.getId()))
            {
                // reset path target so pig does not keep going to score in the same pigchinko hole
                ((EntityCreature)target).getNavigator().clearPath();
            }
        }
    }
}
