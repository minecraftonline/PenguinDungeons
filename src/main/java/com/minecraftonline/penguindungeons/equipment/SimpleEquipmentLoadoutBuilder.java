package com.minecraftonline.penguindungeons.equipment;

import net.minecraft.inventory.EntityEquipmentSlot;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Tuple;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class SimpleEquipmentLoadoutBuilder implements EquipmentLoadout.Builder {
    private final Map<EntityEquipmentSlot, Tuple<ItemStack, Double>> map = new HashMap<>();
    private final Set<Tuple<ItemStack, Double>> anywhere = new HashSet<>();

    public SimpleEquipmentLoadoutBuilder() {}

    @Override
    public EquipmentLoadout.Builder slot(EntityEquipmentSlot slot, ItemStack itemStack, double dropChance) {
        this.map.put(slot, Tuple.of(itemStack, dropChance));
        return this;
    }

    @Override
    public EquipmentLoadout.Builder head(ItemStack itemStack, double dropChance) {
        return slot(EntityEquipmentSlot.HEAD, itemStack, dropChance);
    }

    @Override
    public EquipmentLoadout.Builder chest(ItemStack itemStack, double dropChance) {
        return slot(EntityEquipmentSlot.CHEST, itemStack, dropChance);
    }

    @Override
    public EquipmentLoadout.Builder leggings(ItemStack itemStack, double dropChance) {
        return slot(EntityEquipmentSlot.LEGS, itemStack, dropChance);
    }

    @Override
    public EquipmentLoadout.Builder boots(ItemStack itemStack, double dropChance) {
        return slot(EntityEquipmentSlot.FEET, itemStack, dropChance);
    }

    @Override
    public EquipmentLoadout.Builder mainHand(ItemStack itemStack, double dropChance) {
        return slot(EntityEquipmentSlot.MAINHAND, itemStack, dropChance);
    }

    @Override
    public EquipmentLoadout.Builder offHand(ItemStack itemStack, double dropChance) {
        return slot(EntityEquipmentSlot.OFFHAND, itemStack, dropChance);
    }

    @Override
    public EquipmentLoadout build() {
        if (this.map.isEmpty() && this.anywhere.isEmpty()) {
            return new EmptyEquipmentLoadout();
        }
        Map<EntityEquipmentSlot, Tuple<ItemStack, Double>> map = new HashMap<>(this.map);
        if (!this.anywhere.isEmpty()) {
            Iterator<Tuple<ItemStack, Double>> anywhereIter = this.anywhere.iterator();
            for (EntityEquipmentSlot slot : EntityEquipmentSlot.values()) {
                if (!map.containsKey(slot)) {
                    map.put(slot, anywhereIter.next());
                    if (!anywhereIter.hasNext()) {
                        break;
                    }
                }
            }
            if (anywhereIter.hasNext()) {
                throw new IllegalStateException("Unable to fit all items into an equipment loaded! " + this.map.size() + " fixed and " + this.anywhere + " to go anywhere.");
            }
        }
        return new SimpleEquipmentLoadout(map);
    }
}
