package com.minecraftonline.penguindungeons.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.ItemSpectralArrow;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(ItemSpectralArrow.class)
public abstract class ItemSpectralArrowMixin {

    @Inject(method = "createArrow", at = @At("RETURN"), cancellable = true)
    public void onCeateArrow(World worldIn, ItemStack stack, EntityLivingBase shooter, CallbackInfoReturnable<EntityArrow> cir)
    {
        EntityArrow entityarrow = cir.getReturnValue();
        // name the arrow after the item used
        if (stack.hasDisplayName()) entityarrow.setCustomNameTag(stack.getDisplayName());
        cir.setReturnValue(entityarrow);
    }

}
