package com.minecraftonline.penguindungeons.mixin;

import java.util.Optional;

import org.spongepowered.api.entity.living.animal.Wolf;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;

@Mixin(EntityWolf.class)
public abstract class EntityWolfMixin extends EntityTameable {

    public EntityWolfMixin(World worldIn) {
        super(worldIn);
    }

    private boolean isZombieDog()
    {
        Optional<ResourceKey> id = ((Wolf)(Object) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        return id.isPresent() && id.get().equals(CustomEntityTypes.ZOMBIE_DOG.getId());
    }

    @Inject(method = "getHurtSound", at = @At("HEAD"), cancellable = true)
    private void onGetHurtSound(CallbackInfoReturnable<SoundEvent> cir) {
        if (isZombieDog()) {
            cir.setReturnValue(SoundEvents.ENTITY_ZOMBIE_HURT);
        }
    }

    @Inject(method = "getDeathSound", at = @At("HEAD"), cancellable = true)
    private void onGetDeathSound(CallbackInfoReturnable<SoundEvent> cir) {
        if (isZombieDog()) {
            cir.setReturnValue(SoundEvents.ENTITY_ZOMBIE_DEATH);
        }
    }

    @Override
    public EnumCreatureAttribute getCreatureAttribute() {
        if (isZombieDog()) {
            // zombie dogs are undead
            return EnumCreatureAttribute.UNDEAD;
        } else {
            return super.getCreatureAttribute();
        }
    }

}
