package com.minecraftonline.penguindungeons.mixin;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAITarget;

@Mixin(EntityAITarget.class)
public abstract class EntityAITargetMixin {

    // there is probably a better place to put this check, but figuring out mixin injection locations is complicated
    @Inject(method = "isSuitableTarget", at = @At("HEAD"), cancellable = true)
    private static void onIsSuitableTarget(EntityLiving attacker, @Nullable EntityLivingBase target, boolean includeInvincibles, boolean checkSight, CallbackInfoReturnable<Boolean> cir) {
        // prevent mobs from targeting invulnerable mobs when they shouldn't
        // normally vanilla only uses "includeInvincibles" to check for players
        if (!includeInvincibles && target != null && target.getIsInvulnerable())
        {
            cir.setReturnValue(false);
        }
    }
}
