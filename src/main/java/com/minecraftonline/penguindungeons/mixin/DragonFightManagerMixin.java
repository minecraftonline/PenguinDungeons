package com.minecraftonline.penguindungeons.mixin;

import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.Logger;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.dragon.CrystalType;
import com.minecraftonline.penguindungeons.customentity.dragon.DragonType;

import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.boss.dragon.phase.PhaseList;
import net.minecraft.entity.item.EntityEnderCrystal;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.BossInfoServer;
import net.minecraft.world.WorldServer;
import net.minecraft.world.end.DragonFightManager;
import net.minecraft.world.gen.feature.WorldGenEndPodium;

@Mixin(DragonFightManager.class)
public abstract class DragonFightManagerMixin {

    @Shadow @Final public static Logger LOGGER;
    @Shadow @Final public BossInfoServer bossInfo;
    @Shadow @Final public WorldServer world;
    @Shadow public UUID dragonUniqueId;
    @Shadow public List<EntityEnderCrystal> crystals;

    // allow custom dragon spawning if all spawn crystals are the same kind of custom crystal
    @Inject(method = "createNewDragon", at = @At("HEAD"), cancellable = true)
    private void onCreateNewDragon(CallbackInfoReturnable<EntityDragon> cir) {
        // make sure boss bar name is reset to default
        this.bossInfo.setName(new TextComponentTranslation("entity.EnderDragon.name", new Object[0]));
        // note bossInfo.setColor is only clientside, so this end dragon fight is stuck with purple boss bar

        String crystalName = null;
        for(EntityEnderCrystal crystal : crystals) {
            // iterate through summoning crystals and summon custom dragon if needed
            // return if not a custom dragon spawn
            if (!crystal.hasCustomName()) return;
            if (crystalName == null)
            {
                LOGGER.debug("A summoning crystal is named: \"" + crystal.getCustomNameTag() + "\"");
                crystalName = crystal.getCustomNameTag();
            }
            else if (!crystalName.equals(crystal.getCustomNameTag()))
            {
                LOGGER.debug("A summoning crystal is not named the same as another: \"" + crystal.getCustomNameTag() + "\", \"" + crystalName + "\"");
                return;
            }
        }
        this.world.getChunk(new BlockPos(0, 128, 0));
        EntityDragon entitydragon;
        BlockPos podiumBlock = this.world.getTopSolidOrLiquidBlock(WorldGenEndPodium.END_PODIUM_LOCATION);
        Vector3d podiumPos = new Vector3d(podiumBlock.getX(), podiumBlock.getY(), podiumBlock.getZ());
        DragonType dragonType = null;
        CrystalType crystalType = CrystalType.fromName(crystalName);
        switch(crystalType)
        {
        case TERRAIN:
            LOGGER.debug("Terrain Crystals are being used");
            dragonType = CustomEntityTypes.TERRAIN_DRAGON;
            break;
        case METAL:
            LOGGER.debug("Metal Crystals are being used");
            dragonType = CustomEntityTypes.ROBOT_DRAGON;
            break;
        case ICE:
            LOGGER.debug("Snowflake Crystals are being used");
            // create Frost Golems
            for(EntityEnderCrystal crystal : crystals) {
                Vector3d crystalPos = new Vector3d(crystal.getPosition().getX(), crystal.getPosition().getY(), crystal.getPosition().getZ());
                // don't show boss bar, and start in phase 2 (less than half health, but still normal max health)
                EntityIronGolem frostGolem = (EntityIronGolem) CustomEntityTypes.FROST_GOLEM.makeFrostGolem((World) this.world, crystalPos, false, true);
                // randomize rotation
                frostGolem.setLocationAndAngles(crystal.getPosition().getX(), crystal.getPosition().getY(), crystal.getPosition().getZ(), this.world.rand.nextFloat() * 360.0F, 0.0F);
                this.world.spawnEntity(frostGolem);
                CustomEntityTypes.FROST_GOLEM.onLoad((Entity) frostGolem);
                // cause rain/snow to start in the main world
                this.world.getMinecraftServer().worlds[0].getWorldInfo().setRaining(true);
            }
            break;
        case CHOCOLATE:
            LOGGER.debug("Chocolate Crystals are being used");
            // create Power Paws
            for(EntityEnderCrystal crystal : crystals) {
                Vector3d crystalPos = new Vector3d(crystal.getPosition().getX(), crystal.getPosition().getY(), crystal.getPosition().getZ());
                // don't show boss bar
                EntityIronGolem eggGolem = (EntityIronGolem) CustomEntityTypes.EGG_GOLEM.makeEggGolem((World) this.world, crystalPos, false);
                // randomize rotation
                eggGolem.setLocationAndAngles(crystal.getPosition().getX(), crystal.getPosition().getY(), crystal.getPosition().getZ(), this.world.rand.nextFloat() * 360.0F, 0.0F);
                this.world.spawnEntity(eggGolem);
                CustomEntityTypes.EGG_GOLEM.onLoad((Entity) eggGolem);
            }
            break;
        case SPOOKY:
            LOGGER.debug("Spooky Crystals are being used");
            for(EntityEnderCrystal crystal : crystals) {
                Vector3d crystalPos = new Vector3d(crystal.getPosition().getX(), crystal.getPosition().getY(), crystal.getPosition().getZ());
                // don't show boss bar
                EntityWither pumpkinWither = (EntityWither) CustomEntityTypes.PUMPKIN_WITHER.makeWither((World) this.world, crystalPos, false);
                // randomize rotation
                pumpkinWither.setLocationAndAngles(crystal.getPosition().getX(), crystal.getPosition().getY(), crystal.getPosition().getZ(), this.world.rand.nextFloat() * 360.0F, 0.0F);
                this.world.spawnEntity(pumpkinWither);
                CustomEntityTypes.PUMPKIN_WITHER.onLoad((Entity) pumpkinWither);
            }
            break;
        case ASTRAL:
            LOGGER.debug("Astral Crystals are being used");
            dragonType = CustomEntityTypes.ASTRAL_DRAGON;
            break;
        default:
            LOGGER.debug("Special crystals are not being used");
            // return to let default summoning apply
            return;
        }

        this.world.getChunk(new BlockPos(0, 128, 0));
        if (dragonType != null) entitydragon = (EntityDragon) dragonType.makeDragon((World) this.world, podiumPos, (DragonFightManager)(Object)this);
        else entitydragon = new EntityDragon(this.world);

        entitydragon.getPhaseManager().setPhase(PhaseList.HOLDING_PATTERN);
        entitydragon.setLocationAndAngles(0.0D, 128.0D, 0.0D, this.world.rand.nextFloat() * 360.0F, 0.0F);

        LOGGER.info("Spawning dragon");
        this.world.spawnEntity(entitydragon);

        if (dragonType != null) dragonType.onLoad((Entity) entitydragon);

        LOGGER.info("Dragon is spawned");

        this.dragonUniqueId = entitydragon.getUniqueID();

        cir.setReturnValue(entitydragon);
     }

}
