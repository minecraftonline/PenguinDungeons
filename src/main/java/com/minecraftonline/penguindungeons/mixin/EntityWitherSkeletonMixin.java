package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.monster.AbstractSkeleton;
import net.minecraft.entity.monster.EntityWitherSkeleton;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityTippedArrow;
import net.minecraft.init.Items;
import net.minecraft.init.PotionTypes;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.PotionUtils;
import net.minecraft.world.World;

import java.util.Optional;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityWitherSkeleton.class)
public abstract class EntityWitherSkeletonMixin extends AbstractSkeleton {

    private static final ItemStack BROKEN_HEART_ARROW_ITEM = new ItemStack(Items.TIPPED_ARROW);
    private static final int BROKEN_HEART_COLOR = 10970310;
    static
    {
        PotionUtils.addPotionToItemStack(BROKEN_HEART_ARROW_ITEM, PotionTypes.HARMING);
        NBTTagCompound nbttagcompound = BROKEN_HEART_ARROW_ITEM.getTagCompound();
        if (nbttagcompound == null) {
           nbttagcompound = new NBTTagCompound();
           BROKEN_HEART_ARROW_ITEM.setTagCompound(nbttagcompound);
        }
        nbttagcompound.setInteger("CustomPotionColor", BROKEN_HEART_COLOR);
    }

    public EntityWitherSkeletonMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "getArrow", at = @At("HEAD"), cancellable = true)
    public void onGetArrow(float distanceFactor, CallbackInfoReturnable<EntityArrow> cir) {
        final Optional<ResourceKey> id = ((Entity) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (id.isPresent() && id.get().equals(CustomEntityTypes.FLYING_CUPID.getId())) {
            EntityTippedArrow entitytippedarrow = (EntityTippedArrow) super.getArrow(distanceFactor);
            entitytippedarrow.setPotionEffect(BROKEN_HEART_ARROW_ITEM);
            ((Entity) entitytippedarrow).offer(Keys.DISPLAY_NAME, Text.of(TextColors.DARK_PURPLE, "Broken Heart Arrow"));
            cir.setReturnValue(entitytippedarrow);
        }
    }

    @Override
    protected int getExperiencePoints(EntityPlayer player) {
        final Optional<ResourceKey> id = ((Entity) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (id.isPresent() && id.get().equals(CustomEntityTypes.FLYING_CUPID.getId()))
        {
            this.experienceValue = 25;
        }

        return super.getExperiencePoints(player);
    }
}
