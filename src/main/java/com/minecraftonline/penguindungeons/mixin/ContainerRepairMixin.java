package com.minecraftonline.penguindungeons.mixin;

import org.apache.commons.lang3.StringUtils;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerRepair;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatAllowedCharacters;

@Mixin(ContainerRepair.class)
public abstract class ContainerRepairMixin extends Container {

    @Shadow
    private IInventory inputSlots;
    @Shadow
    private String repairedItemName;

    private boolean updateOutput = true;

    // just replace the whole method with our own version
    @Inject(method = "updateItemName", at = @At("HEAD"), cancellable = true)
    public void onUpdateItemName(String newName, CallbackInfo ci) {
        boolean isBlank = StringUtils.isBlank(newName);
        ItemStack inputItemstack = this.inputSlots.getStackInSlot(0);
        if (!isBlank && inputItemstack.hasDisplayName()) {
            String filteredInputItemName = ChatAllowedCharacters.filterAllowedCharacters(inputItemstack.getDisplayName());
            String filteredNewName       = ChatAllowedCharacters.filterAllowedCharacters(newName);
            if (filteredInputItemName.equals(filteredNewName) && !filteredInputItemName.equals(inputItemstack.getDisplayName())) {
                // IF   filtered input item name is the same as filtered new name (i.e. it hasn't been renamed)
                // AND  filtered input item name is different to the item name (i.e. it has filtered characters)
                // THEN use the original input item name to keep formatting symbols
                newName = inputItemstack.getDisplayName();
            } else if (!filteredNewName.equals(newName)) {
                // re-filter the name here as the item could have been swapped out
                newName = filteredNewName;
            }
        }
        this.repairedItemName = newName;
        if (this.getSlot(2).getHasStack()) {
            ItemStack outputItemstack = this.getSlot(2).getStack();
            if (isBlank) {
                outputItemstack.clearCustomName();
            } else {
                outputItemstack.setStackDisplayName(this.repairedItemName);
            }
        }

        // note, the client may visually make the end slot appear with formatting codes
        // which is not overridden by the server, as the server does not think anything has changed
        if (updateOutput) ((ContainerRepair)(Object)this).updateRepairOutput();
        updateOutput = true;
        ci.cancel();
    }

    @Inject(method = "onCraftMatrixChanged", at = @At("HEAD"))
    public void onOnCraftMatrixChanged(IInventory inventoryIn, CallbackInfo ci) {
        if (inventoryIn == this.inputSlots) {
            // workaround for items being added after input name is first sent
            // and item being replaced with another item that has same name but no formatting
            // don't need to call updateRepairOutput again here as it will be by onCraftMatrixChanged
            updateOutput = false;
            ((ContainerRepair)(Object)this).updateItemName(this.repairedItemName);
        }
    }

}
