package com.minecraftonline.penguindungeons.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityTippedArrow;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

@Mixin(EntityTippedArrow.class)
public abstract class EntityTippedArrowMixin extends EntityArrow {

    @Shadow
    private boolean fixedColor;

    public EntityTippedArrowMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "getArrowStack", at = @At("RETURN"), cancellable = true)
    public void onGetArrowStack(CallbackInfoReturnable<ItemStack> cir)
    {
        ItemStack arrowitem = cir.getReturnValue();
        if (this.fixedColor && arrowitem.getItem() != Items.TIPPED_ARROW) {
            // fix arrows with custom potion color but no potion effects reverting to regular arrows
            arrowitem = new ItemStack(Items.TIPPED_ARROW);
            NBTTagCompound nbttagcompound = arrowitem.getTagCompound();
            if (nbttagcompound == null) {
               nbttagcompound = new NBTTagCompound();
               arrowitem.setTagCompound(nbttagcompound);
            }

            nbttagcompound.setInteger("CustomPotionColor", ((EntityTippedArrow)(Object) this).getColor());
        }
        // name the arrow item after entity
        if (this.hasCustomName()) arrowitem.setStackDisplayName(this.getCustomNameTag());
        cir.setReturnValue(arrowitem);
    }

}
