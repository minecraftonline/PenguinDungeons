package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityFlying;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

import org.spongepowered.api.entity.living.monster.Ghast;
import org.spongepowered.asm.mixin.Mixin;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

@Mixin(EntityGhast.class)
public abstract class EntityGhastMixin extends EntityFlying {

    public EntityGhastMixin(World worldIn) {
        super(worldIn);
    }
    
    @Override
    protected int getExperiencePoints(EntityPlayer player) {
        if (((Ghast) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent())
        {
            this.experienceValue = 50;
        }

        return super.getExperiencePoints(player);
    }

    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        boolean result = super.attackEntityFrom(source, amount);
        if (!result) return result;

        // Ghast AI is weird and seems to have trouble targeting players
        if (this.getAttackTarget() == null && ((Ghast) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent())
        {
            Entity attacker = source.getTrueSource();
            if (attacker != null && attacker instanceof EntityPlayer)
            {
                this.setAttackTarget((EntityPlayer) attacker);
            }
        }
        return result;
    }
}
