package com.minecraftonline.penguindungeons.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.world.World;

@Mixin(EntityTameable.class)
public abstract class EntityTameableMixin extends EntityAnimal {

    public EntityTameableMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "setTamedBy", at = @At("HEAD"))
    private void onSetTamedBy(CallbackInfo ci)
    {
        this.enablePersistence();
    }
}
