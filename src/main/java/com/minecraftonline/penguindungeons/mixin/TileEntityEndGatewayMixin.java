package com.minecraftonline.penguindungeons.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.Entity;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.tileentity.TileEntityEndGateway;

@Mixin(TileEntityEndGateway.class)
public abstract class TileEntityEndGatewayMixin {

    @Inject(method = "teleportEntity", at = @At("HEAD"), cancellable = true)
    public void onTeleportEntity(Entity entityIn, CallbackInfo ci) {
        // don't teleport ender dragons
        if (entityIn instanceof EntityDragon) ci.cancel();
    }
}
