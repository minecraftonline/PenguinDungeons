package com.minecraftonline.penguindungeons.mixin;

import java.util.Optional;

import javax.annotation.Nullable;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Slime;
import org.spongepowered.api.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.CustomEntityType;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.other.SlimeType;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.monster.EntitySlime;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DifficultyInstance;

@Mixin(EntitySlime.class)
public abstract class EntitySlimeMixin extends EntityLiving {

    public EntitySlimeMixin(net.minecraft.world.World worldIn) {
        super(worldIn);
    }

    private boolean isPenguinDungeonMob()
    {
        Slime slime = (Slime) this;
        final Optional<ResourceKey> id = slime.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        return id.isPresent();
    }

    @Inject(method = "canDamagePlayer", at = @At("HEAD"), cancellable = true)
    protected void onCanDamagePlayer(CallbackInfoReturnable<Boolean> cir) {
        if (this.isPenguinDungeonMob())
        {
            // dungeon slimes can damage players
            cir.setReturnValue(true);
        }
    }

    @Inject(method = "getAttackStrength", at = @At("HEAD"), cancellable = true)
    protected void onGetAttackStrength(CallbackInfoReturnable<Integer> cir) {
        if (this.isPenguinDungeonMob())
        {
            // dungeon slimes do more damage
            cir.setReturnValue(((EntitySlime)(Object)this).getSlimeSize() + 3);
        }
    }

    @Inject(method = "getCanSpawnHere", at = @At("HEAD"), cancellable = true)
    private void onGetCanSpawnHere(CallbackInfoReturnable<Boolean> cir) {
        if (this.isPenguinDungeonMob())
        {
            // bypass slime chunk spawn restriction
            IBlockState iblockstate = this.world.getBlockState((new BlockPos(this)).down());
            cir.setReturnValue(iblockstate.canEntitySpawn(this));
        }
    }

    @Inject(method = "setDead", at = @At("HEAD"), cancellable = true)
    private void onSetDead(CallbackInfo ci) {
        Slime slime = (Slime) this;
        Optional<Integer> optSize = slime.get(Keys.SLIME_SIZE);
        if (!optSize.isPresent() || optSize.get() < 1)
        {
            // no more slimes
            super.setDead();
            ci.cancel();
            return;
        }
        int size = optSize.get();

        final Optional<ResourceKey> id = slime.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (id.isPresent())
        {
            Optional<CustomEntityType> pdEntityType = CustomEntityTypes.getById(id.get());
            if (pdEntityType.isPresent() && pdEntityType.get() instanceof SlimeType)
            {
                int newSize;
                if (size <= 1) newSize = 0;
                else newSize = size / 2;
                SlimeType slimeType = ((SlimeType)pdEntityType.get());
                BlockPos blockPos = this.getPosition();
                Vector3d pos = new Vector3d(blockPos.getX(), blockPos.getY(), blockPos.getZ());
                int j = 2 + this.rand.nextInt(3);

                for(int k = 0; k < j; ++k) {
                    float f = ((float)(k % 2) - 0.5F) * (float)size / 4.0F;
                    float f1 = ((float)(k / 2) - 0.5F) * (float)size / 4.0F;
                    // spawn a new version of this dungeon mob with half the size
                    EntitySlime newSlime = (EntitySlime) slimeType.makeSlime((World) this.world, pos, newSize);
                    newSlime.setLocationAndAngles(this.posX + (double)f, this.posY + 0.5D, this.posZ + (double)f1, this.rand.nextFloat() * 360.0F, 0.0F);
                    this.world.spawnEntity(newSlime);
                    slimeType.onLoad(slime);
                }
                super.setDead();
                ci.cancel();
            }
        }
    }

    @Inject(method="onInitialSpawn", at = @At("HEAD"), cancellable = true)
    public void onOnInitialSpawn(DifficultyInstance difficulty, @Nullable IEntityLivingData livingdata,
            CallbackInfoReturnable<IEntityLivingData> cir) {
        if (this.isPenguinDungeonMob())
        {
            // don't set a new random slime size
            cir.setReturnValue(super.onInitialSpawn(difficulty, livingdata));
        }
    }

    @Invoker("setSlimeSize")
    public abstract void setSlimeSize(int size, boolean resetHealth);
}
