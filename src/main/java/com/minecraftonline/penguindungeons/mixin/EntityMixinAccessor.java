package com.minecraftonline.penguindungeons.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(net.minecraft.entity.Entity.class)
public interface EntityMixinAccessor {

    @Accessor("isImmuneToFire")
    public void setIsImmuneToFire(boolean value);

}
