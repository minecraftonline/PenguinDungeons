package com.minecraftonline.penguindungeons.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntitySpectralArrow;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(EntitySpectralArrow.class)
public abstract class EntitySpectralArrowMixin extends EntityArrow {

    public EntitySpectralArrowMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "getArrowStack", at = @At("RETURN"), cancellable = true)
    public void onGetArrowStack(CallbackInfoReturnable<ItemStack> cir)
    {
        ItemStack arrowitem = cir.getReturnValue();
        // name the arrow item after entity
        if (this.hasCustomName()) arrowitem.setStackDisplayName(this.getCustomNameTag());
        cir.setReturnValue(arrowitem);
    }

}
