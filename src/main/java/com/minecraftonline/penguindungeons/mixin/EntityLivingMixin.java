package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.BossInfo;
import net.minecraft.world.BossInfoServer;
import net.minecraft.world.World;

import org.spongepowered.api.boss.BossBarColor;
import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.google.common.collect.ImmutableMap;
import com.minecraftonline.penguindungeons.customentity.golem.HasBossBar;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

@Mixin(EntityLiving.class)
public abstract class EntityLivingMixin extends EntityLivingBase implements HasBossBar {

    private BossInfoServer bossInfo;
    private boolean usesBossBar = false;
    private static ImmutableMap<BossBarColor, BossInfo.Color> colorMap;

    @Shadow
    private EntityLivingBase attackTarget;

    public EntityLivingMixin(World worldIn) {
        super(worldIn);
    }

    private BossInfoServer checkBossInfo()
    {
        // this has to be done here instead of in constructor because mixins constructors are weird
        if (EntityLivingMixin.colorMap == null)
        {
            EntityLivingMixin.colorMap = ImmutableMap.<BossBarColor, BossInfo.Color>builder()
                .put(BossBarColors.BLUE,   BossInfo.Color.BLUE)
                .put(BossBarColors.GREEN,  BossInfo.Color.GREEN)
                .put(BossBarColors.PINK,   BossInfo.Color.PINK)
                .put(BossBarColors.PURPLE, BossInfo.Color.PURPLE)
                .put(BossBarColors.RED,    BossInfo.Color.RED)
                .put(BossBarColors.WHITE,  BossInfo.Color.WHITE)
                .put(BossBarColors.YELLOW, BossInfo.Color.YELLOW)
                .build();
        }
        if (this.bossInfo == null)
        {
            this.usesBossBar = ((Entity) this).get(PenguinDungeonKeys.PD_BOSS_BAR).orElse(false);
            if (this.usesBossBar)
            {
                BossBarColor color = ((Entity) this).get(PenguinDungeonKeys.PD_BOSS_BAR_COLOR).orElse(BossBarColors.RED);
                this.bossInfo = new BossInfoServer(this.getDisplayName(), EntityLivingMixin.colorMap.get(color), BossInfo.Overlay.PROGRESS);
                this.bossInfo.setDarkenSky(false);
                this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
            }
        }
        return this.bossInfo;
    }

    @Override
    public void addTrackingPlayer(EntityPlayerMP player) {
        this.checkBossInfo();
        if (this.usesBossBar) this.bossInfo.addPlayer(player);
        super.addTrackingPlayer(player);
    }

    @Override
    public void removeTrackingPlayer(EntityPlayerMP player) {
        this.checkBossInfo();
        if (this.usesBossBar) this.bossInfo.removePlayer(player);
        super.removeTrackingPlayer(player);
    }

    @Override
    public void setCustomNameTag(String name) {
        super.setCustomNameTag(name);
        if (this.usesBossBar) this.bossInfo.setName(this.getDisplayName());
    }

    @Inject(method = "onLivingUpdate", at = @At("HEAD"))
    public void onOnLivingUpdate(CallbackInfo ci) {
        this.checkBossInfo();
        if (this.usesBossBar) this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
    }

    @Inject(method = "getAttackTarget", at = @At("HEAD"), cancellable = true)
    private void onGetAttackTarget(CallbackInfoReturnable<EntityLivingBase> cir) {
        // fix mobs continuing to attack targets which are already dead
        if (this.attackTarget != null && !this.attackTarget.isEntityAlive()) cir.setReturnValue(null);
    }
}
